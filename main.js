(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\keminWorkspace\angular\carparcking\src\main.ts */"zUnb");


/***/ }),

/***/ "0bGh":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/parking-area/parking-area.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-6\">\n                <form>\n\n                    <fieldset>\n                        <label for=\"firstName\">Id</label>\n                        <input type=\"text\" disabled readonly name=\"name\" [(ngModel)]=\"parkingAreaDetails.id\" id=\"firstName\" placeholder=\"Id\">\n                    </fieldset>\n                    \n                    <fieldset>\n                        <label for=\"textarea\">Address</label>\n                        <input name=\"textarea\" [(ngModel)]=\"parkingAreaDetails.address\" type=\"text\" id=\"textarea\" placeholder=\"Image\">\n                    </fieldset>\n                    <fieldset>\n                        <label for=\"textarea\">Image</label>\n                        <input name=\"textarea\" [(ngModel)]=\"parkingAreaDetails.imageTemp\" (change)=\"onFileChange($event)\" type=\"file\" id=\"textarea\" placeholder=\"Image\">\n                    </fieldset>\n\n                    <!-- <button>Submit</button> -->\n                </form>\n            </div>\n\n            <div class=\"col-md-6\">\n                <div class=\"rightBox\">\n                    <app-resizable-draggable *ngIf=\"markingadding\" style=\"position: absolute;\" [width]=\"width\" [height]=\"height\" [left]=\"left\" [top]=\"top\"></app-resizable-draggable>\n                    \n                    <canvas id=\"myCanvas\" [class.hidden]=\"!imageSrc\" width=\"500\" height=\"300\">\n\n                    </canvas>\n                    <img [src]=\"imageSrc\" class=\"hidden\" id=\"imagePlacer\" style=\"display: none;\" style=\"height: 300px; width:500px\">\n                </div>\n                \n        <div class=\"row margin-top\">\n            \n            <div class=\"col-md-3\"  *ngIf=\"!markingadding && imageSrc\">\n                <button (click)=\"markingadding = true\">Add New Marking</button>\n            </div>\n            <div class=\"col-md-3\" *ngIf=\"markingadding\">\n                <button (click)=\"saveMarkings()\">Save marking</button>\n            </div>\n        </div>\n            </div>\n        </div>\n        <div class=\"row margin-top\">\n            \n            <div class=\"col-md-3\">\n                <button (click)=\"addCamera()\">Save</button>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <table>\n                    <thead>\n                      <tr>\n                        <th scope=\"col\">Id</th>\n                        <th scope=\"col\">Address</th>\n                        <th scope=\"col\">Image</th>\n                      </tr>\n                    </thead>\n                    <tbody>\n                      <tr *ngFor=\"let par of parkings\">\n                        <td data-label=\"Account\">{{par.id}}</td>\n                        <td data-label=\"Due Date\">{{par.address}}</td>\n                        <td data-label=\"Amount\">{{par.image}}</td>\n                      </tr>\n                    \n                    </tbody>\n                  </table>\n            </div>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "0rzt":
/*!*****************************************************!*\
  !*** ./src/app/my-profile/my-profile.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@import url(https://fonts.googleapis.com/css?family=Roboto:400,300);\r\n* {\r\n  box-sizing: border-box;\r\n}\r\nhtml {\r\n  height: 100%;\r\n}\r\nbody {\r\n  height: 100%;\r\n  font-family: Roboto, san-serif;\r\n  font-weight: 300;\r\n  background-color: #f5f6f7;\r\n}\r\n/* Form element setup */\r\nform {\r\n    \r\n  /* position: absolute; */\r\n  top: 0;\r\n  /* left: 50%; */\r\n  width: 300px;\r\n  /* transform: translateX(-50%); */\r\n  margin: 2rem 0;\r\n  z-index: 1;\r\n}\r\nfieldset {\r\n  margin: 0 0 1rem 0;\r\n  padding: 0;\r\n  border: none;\r\n}\r\nlegend {\r\n  font-weight: 400;\r\n}\r\nlegend,\r\nlabel {\r\n  display: inline-block;\r\n  margin-bottom: 0.5rem;\r\n}\r\ninput[type=text],\r\ntextarea,\r\nselect {\r\n  display: block;\r\n  padding: 0.5rem;\r\n  width: 100%;\r\n  background-color: white;\r\n  border-radius: 0.25rem;\r\n  border: 1px solid #e5e5e5;\r\n  outline: none;\r\n  /* List some properties that might change */\r\n  transition-property: none;\r\n  transition-duration: none;\r\n}\r\ninput[type=text]:focus,\r\ntextarea:focus,\r\nselect:focus {\r\n  border-color: #ef7ead;\r\n}\r\ntextarea {\r\n  max-width: 300px;\r\n  height: 100px;\r\n}\r\ninput[type=text],\r\nselect {\r\n  height: 34px;\r\n}\r\nselect {\r\n  font-size: 0.875rem;\r\n}\r\ninput[type=checkbox],\r\ninput[type=radio] {\r\n  position: relative;\r\n  top: 5px;\r\n  width: 22px;\r\n  height: 22px;\r\n  margin: 0 0.5rem;\r\n  background-color: white;\r\n  border: 1px solid #e5e5e5;\r\n  outline: none;\r\n  -moz-appearance: none;\r\n  -webkit-appearance: none;\r\n  /* List some properties that might change */\r\n  transition-property: none;\r\n  transition-duration: none;\r\n}\r\ninput[type=checkbox] {\r\n  border-radius: 5px;\r\n}\r\ninput[type=checkbox]:checked {\r\n  background-color: #ef7ead;\r\n  border: none;\r\n}\r\ninput[type=checkbox]:checked:after {\r\n  display: block;\r\n  content: \"\";\r\n  height: 4px;\r\n  width: 10px;\r\n  border-bottom: 3px solid #fff;\r\n  border-left: 3px solid #fff;\r\n  transform: translate(5px, 6px) rotate(-45deg) scale(1);\r\n}\r\ninput[type=radio] {\r\n  border-radius: 50%;\r\n}\r\ninput[type=radio]:checked {\r\n  border-width: 5px;\r\n  border-color: white;\r\n  background-color: #ef7ead;\r\n}\r\nbutton {\r\n  display: block;\r\n  margin: 3em auto;\r\n  padding: 0.5rem 2rem;\r\n  font-size: 125%;\r\n  color: white;\r\n  border: none;\r\n  border-radius: 0.25rem;\r\n  background-color: #ef7ead;\r\n  outline: none;\r\n  box-shadow: 0 0.4rem 0.1rem -0.3rem rgba(0, 0, 0, 0.1);\r\n  /* We'll talk about this next */\r\n  transform: perspective(300px) scale(0.95) translateZ(0);\r\n  transform-style: preserve-3d;\r\n  /* List the properties that you're looking to transition.\r\n     Try not to use 'all' */\r\n  transition-property: none;\r\n  /* This applies to all of the above properties */\r\n  transition-duration: none;\r\n}\r\nbutton:hover {\r\n  cursor: pointer;\r\n  background-color: #ff96c8;\r\n  box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);\r\n  transform: scale(1.1) rotateX(0);\r\n}\r\nbutton:active {\r\n  background-color: #ef7ead;\r\n  transform: scale(1.05) rotateX(-10deg);\r\n}\r\n.rightBox {\r\n    border: 1px solid;\r\n    width: 100%;\r\n    height: 40vh;\r\n}\r\n.mt {margin-top: 25px; padding-top: 25px;}\r\n.bt {\r\n    border-top: 1px solid;\r\n}\r\ntable { border: 1px solid #ccc; border-collapse: collapse; margin: 0; padding: 0; width: 100%; table-layout: fixed;}\r\ntable caption { font-size: 1.5em; margin: .5em 0 .75em;}\r\ntable tr { border: 1px solid #ddd; padding: .35em;}\r\ntable th,\r\n      table td { padding: .625em; text-align: center;}\r\ntable th { font-size: .85em; letter-spacing: .1em; text-transform: uppercase;}\r\n@media screen and (max-width: 600px) {\r\n        table { border: 0; }\r\n        table caption { font-size: 1.3em; }\r\n        table thead { border: none; clip: rect(0 0 0 0); height: 1px; margin: -1px; overflow: hidden; padding: 0; position: absolute; width: 1px;}\r\n        table tr { border-bottom: 3px solid #ddd; display: block; margin-bottom: .625em; }\r\n        table td { border-bottom: 1px solid #ddd; display: block; font-size: .8em; text-align: right;}\r\n        table td::before { content: attr(data-label); float: left; font-weight: bold; text-transform: uppercase; }\r\n        table td:last-child { border-bottom: 0; }\r\n      }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm15LXByb2ZpbGUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxtRUFBbUU7QUFDbkU7RUFDRSxzQkFBc0I7QUFDeEI7QUFFQTtFQUNFLFlBQVk7QUFDZDtBQUVBO0VBQ0UsWUFBWTtFQUNaLDhCQUE4QjtFQUM5QixnQkFBZ0I7RUFDaEIseUJBQXlCO0FBQzNCO0FBRUEsdUJBQXVCO0FBQ3ZCOztFQUVFLHdCQUF3QjtFQUN4QixNQUFNO0VBQ04sZUFBZTtFQUNmLFlBQVk7RUFDWixpQ0FBaUM7RUFDakMsY0FBYztFQUNkLFVBQVU7QUFDWjtBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixZQUFZO0FBQ2Q7QUFFQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUVBOztFQUVFLHFCQUFxQjtFQUNyQixxQkFBcUI7QUFDdkI7QUFFQTs7O0VBR0UsY0FBYztFQUNkLGVBQWU7RUFDZixXQUFXO0VBQ1gsdUJBQXVCO0VBQ3ZCLHNCQUFzQjtFQUN0Qix5QkFBeUI7RUFDekIsYUFBYTtFQUNiLDJDQUEyQztFQUMzQyx5QkFBeUI7RUFDekIseUJBQXlCO0FBQzNCO0FBQ0E7OztFQUdFLHFCQUFxQjtBQUN2QjtBQUVBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGFBQWE7QUFDZjtBQUVBOztFQUVFLFlBQVk7QUFDZDtBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCO0FBRUE7O0VBRUUsa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixXQUFXO0VBQ1gsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQix1QkFBdUI7RUFDdkIseUJBQXlCO0VBQ3pCLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsd0JBQXdCO0VBQ3hCLDJDQUEyQztFQUMzQyx5QkFBeUI7RUFDekIseUJBQXlCO0FBQzNCO0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLHlCQUF5QjtFQUN6QixZQUFZO0FBQ2Q7QUFDQTtFQUNFLGNBQWM7RUFDZCxXQUFXO0VBQ1gsV0FBVztFQUNYLFdBQVc7RUFDWCw2QkFBNkI7RUFDN0IsMkJBQTJCO0VBQzNCLHNEQUFzRDtBQUN4RDtBQUVBO0VBQ0Usa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLHlCQUF5QjtBQUMzQjtBQUVBO0VBQ0UsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUNmLFlBQVk7RUFDWixZQUFZO0VBQ1osc0JBQXNCO0VBQ3RCLHlCQUF5QjtFQUN6QixhQUFhO0VBQ2Isc0RBQXNEO0VBQ3RELCtCQUErQjtFQUMvQix1REFBdUQ7RUFDdkQsNEJBQTRCO0VBQzVCOzJCQUN5QjtFQUN6Qix5QkFBeUI7RUFDekIsZ0RBQWdEO0VBQ2hELHlCQUF5QjtBQUMzQjtBQUNBO0VBQ0UsZUFBZTtFQUNmLHlCQUF5QjtFQUN6QixvQ0FBb0M7RUFDcEMsZ0NBQWdDO0FBQ2xDO0FBQ0E7RUFDRSx5QkFBeUI7RUFDekIsc0NBQXNDO0FBQ3hDO0FBR0E7SUFDSSxpQkFBaUI7SUFDakIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQSxLQUFLLGdCQUFnQixFQUFFLGlCQUFpQixDQUFDO0FBQ3pDO0lBQ0kscUJBQXFCO0FBQ3pCO0FBQ0EsUUFBUSxzQkFBc0IsRUFBRSx5QkFBeUIsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxtQkFBbUIsQ0FBQztBQUM3RyxnQkFBZ0IsZ0JBQWdCLEVBQUUsb0JBQW9CLENBQUM7QUFDdkQsV0FBVyxzQkFBc0IsRUFBRSxjQUFjLENBQUM7QUFDbEQ7aUJBQ1csZUFBZSxFQUFFLGtCQUFrQixDQUFDO0FBQy9DLFdBQVcsZ0JBQWdCLEVBQUUsb0JBQW9CLEVBQUUseUJBQXlCLENBQUM7QUFDN0U7UUFDRSxRQUFRLFNBQVMsRUFBRTtRQUNuQixnQkFBZ0IsZ0JBQWdCLEVBQUU7UUFDbEMsY0FBYyxZQUFZLEVBQUUsbUJBQW1CLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxnQkFBZ0IsRUFBRSxVQUFVLEVBQUUsa0JBQWtCLEVBQUUsVUFBVSxDQUFDO1FBQ3pJLFdBQVcsNkJBQTZCLEVBQUUsY0FBYyxFQUFFLHFCQUFxQixFQUFFO1FBQ2pGLFdBQVcsNkJBQTZCLEVBQUUsY0FBYyxFQUFFLGVBQWUsRUFBRSxpQkFBaUIsQ0FBQztRQUM3RixtQkFBbUIseUJBQXlCLEVBQUUsV0FBVyxFQUFFLGlCQUFpQixFQUFFLHlCQUF5QixFQUFFO1FBQ3pHLHNCQUFzQixnQkFBZ0IsRUFBRTtNQUMxQyIsImZpbGUiOiJteS1wcm9maWxlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IHVybChodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9Um9ib3RvOjQwMCwzMDApO1xyXG4qIHtcclxuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG59XHJcblxyXG5odG1sIHtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuXHJcbmJvZHkge1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBmb250LWZhbWlseTogUm9ib3RvLCBzYW4tc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDMwMDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjVmNmY3O1xyXG59XHJcblxyXG4vKiBGb3JtIGVsZW1lbnQgc2V0dXAgKi9cclxuZm9ybSB7XHJcbiAgICBcclxuICAvKiBwb3NpdGlvbjogYWJzb2x1dGU7ICovXHJcbiAgdG9wOiAwO1xyXG4gIC8qIGxlZnQ6IDUwJTsgKi9cclxuICB3aWR0aDogMzAwcHg7XHJcbiAgLyogdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpOyAqL1xyXG4gIG1hcmdpbjogMnJlbSAwO1xyXG4gIHotaW5kZXg6IDE7XHJcbn1cclxuXHJcbmZpZWxkc2V0IHtcclxuICBtYXJnaW46IDAgMCAxcmVtIDA7XHJcbiAgcGFkZGluZzogMDtcclxuICBib3JkZXI6IG5vbmU7XHJcbn1cclxuXHJcbmxlZ2VuZCB7XHJcbiAgZm9udC13ZWlnaHQ6IDQwMDtcclxufVxyXG5cclxubGVnZW5kLFxyXG5sYWJlbCB7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcclxufVxyXG5cclxuaW5wdXRbdHlwZT10ZXh0XSxcclxudGV4dGFyZWEsXHJcbnNlbGVjdCB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgcGFkZGluZzogMC41cmVtO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDAuMjVyZW07XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2U1ZTVlNTtcclxuICBvdXRsaW5lOiBub25lO1xyXG4gIC8qIExpc3Qgc29tZSBwcm9wZXJ0aWVzIHRoYXQgbWlnaHQgY2hhbmdlICovXHJcbiAgdHJhbnNpdGlvbi1wcm9wZXJ0eTogbm9uZTtcclxuICB0cmFuc2l0aW9uLWR1cmF0aW9uOiBub25lO1xyXG59XHJcbmlucHV0W3R5cGU9dGV4dF06Zm9jdXMsXHJcbnRleHRhcmVhOmZvY3VzLFxyXG5zZWxlY3Q6Zm9jdXMge1xyXG4gIGJvcmRlci1jb2xvcjogI2VmN2VhZDtcclxufVxyXG5cclxudGV4dGFyZWEge1xyXG4gIG1heC13aWR0aDogMzAwcHg7XHJcbiAgaGVpZ2h0OiAxMDBweDtcclxufVxyXG5cclxuaW5wdXRbdHlwZT10ZXh0XSxcclxuc2VsZWN0IHtcclxuICBoZWlnaHQ6IDM0cHg7XHJcbn1cclxuXHJcbnNlbGVjdCB7XHJcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcclxufVxyXG5cclxuaW5wdXRbdHlwZT1jaGVja2JveF0sXHJcbmlucHV0W3R5cGU9cmFkaW9dIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgdG9wOiA1cHg7XHJcbiAgd2lkdGg6IDIycHg7XHJcbiAgaGVpZ2h0OiAyMnB4O1xyXG4gIG1hcmdpbjogMCAwLjVyZW07XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2U1ZTVlNTtcclxuICBvdXRsaW5lOiBub25lO1xyXG4gIC1tb3otYXBwZWFyYW5jZTogbm9uZTtcclxuICAtd2Via2l0LWFwcGVhcmFuY2U6IG5vbmU7XHJcbiAgLyogTGlzdCBzb21lIHByb3BlcnRpZXMgdGhhdCBtaWdodCBjaGFuZ2UgKi9cclxuICB0cmFuc2l0aW9uLXByb3BlcnR5OiBub25lO1xyXG4gIHRyYW5zaXRpb24tZHVyYXRpb246IG5vbmU7XHJcbn1cclxuXHJcbmlucHV0W3R5cGU9Y2hlY2tib3hdIHtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbn1cclxuaW5wdXRbdHlwZT1jaGVja2JveF06Y2hlY2tlZCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VmN2VhZDtcclxuICBib3JkZXI6IG5vbmU7XHJcbn1cclxuaW5wdXRbdHlwZT1jaGVja2JveF06Y2hlY2tlZDphZnRlciB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgY29udGVudDogXCJcIjtcclxuICBoZWlnaHQ6IDRweDtcclxuICB3aWR0aDogMTBweDtcclxuICBib3JkZXItYm90dG9tOiAzcHggc29saWQgI2ZmZjtcclxuICBib3JkZXItbGVmdDogM3B4IHNvbGlkICNmZmY7XHJcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoNXB4LCA2cHgpIHJvdGF0ZSgtNDVkZWcpIHNjYWxlKDEpO1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPXJhZGlvXSB7XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG59XHJcbmlucHV0W3R5cGU9cmFkaW9dOmNoZWNrZWQge1xyXG4gIGJvcmRlci13aWR0aDogNXB4O1xyXG4gIGJvcmRlci1jb2xvcjogd2hpdGU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VmN2VhZDtcclxufVxyXG5cclxuYnV0dG9uIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBtYXJnaW46IDNlbSBhdXRvO1xyXG4gIHBhZGRpbmc6IDAuNXJlbSAycmVtO1xyXG4gIGZvbnQtc2l6ZTogMTI1JTtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIGJvcmRlci1yYWRpdXM6IDAuMjVyZW07XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VmN2VhZDtcclxuICBvdXRsaW5lOiBub25lO1xyXG4gIGJveC1zaGFkb3c6IDAgMC40cmVtIDAuMXJlbSAtMC4zcmVtIHJnYmEoMCwgMCwgMCwgMC4xKTtcclxuICAvKiBXZSdsbCB0YWxrIGFib3V0IHRoaXMgbmV4dCAqL1xyXG4gIHRyYW5zZm9ybTogcGVyc3BlY3RpdmUoMzAwcHgpIHNjYWxlKDAuOTUpIHRyYW5zbGF0ZVooMCk7XHJcbiAgdHJhbnNmb3JtLXN0eWxlOiBwcmVzZXJ2ZS0zZDtcclxuICAvKiBMaXN0IHRoZSBwcm9wZXJ0aWVzIHRoYXQgeW91J3JlIGxvb2tpbmcgdG8gdHJhbnNpdGlvbi5cclxuICAgICBUcnkgbm90IHRvIHVzZSAnYWxsJyAqL1xyXG4gIHRyYW5zaXRpb24tcHJvcGVydHk6IG5vbmU7XHJcbiAgLyogVGhpcyBhcHBsaWVzIHRvIGFsbCBvZiB0aGUgYWJvdmUgcHJvcGVydGllcyAqL1xyXG4gIHRyYW5zaXRpb24tZHVyYXRpb246IG5vbmU7XHJcbn1cclxuYnV0dG9uOmhvdmVyIHtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmOTZjODtcclxuICBib3gtc2hhZG93OiAwIDAgMCAwIHJnYmEoMCwgMCwgMCwgMCk7XHJcbiAgdHJhbnNmb3JtOiBzY2FsZSgxLjEpIHJvdGF0ZVgoMCk7XHJcbn1cclxuYnV0dG9uOmFjdGl2ZSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VmN2VhZDtcclxuICB0cmFuc2Zvcm06IHNjYWxlKDEuMDUpIHJvdGF0ZVgoLTEwZGVnKTtcclxufVxyXG5cclxuXHJcbi5yaWdodEJveCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiA0MHZoO1xyXG59XHJcbi5tdCB7bWFyZ2luLXRvcDogMjVweDsgcGFkZGluZy10b3A6IDI1cHg7fVxyXG4uYnQge1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkO1xyXG59XHJcbnRhYmxlIHsgYm9yZGVyOiAxcHggc29saWQgI2NjYzsgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTsgbWFyZ2luOiAwOyBwYWRkaW5nOiAwOyB3aWR0aDogMTAwJTsgdGFibGUtbGF5b3V0OiBmaXhlZDt9XHJcbiAgICAgIHRhYmxlIGNhcHRpb24geyBmb250LXNpemU6IDEuNWVtOyBtYXJnaW46IC41ZW0gMCAuNzVlbTt9XHJcbiAgICAgIHRhYmxlIHRyIHsgYm9yZGVyOiAxcHggc29saWQgI2RkZDsgcGFkZGluZzogLjM1ZW07fVxyXG4gICAgICB0YWJsZSB0aCxcclxuICAgICAgdGFibGUgdGQgeyBwYWRkaW5nOiAuNjI1ZW07IHRleHQtYWxpZ246IGNlbnRlcjt9XHJcbiAgICAgIHRhYmxlIHRoIHsgZm9udC1zaXplOiAuODVlbTsgbGV0dGVyLXNwYWNpbmc6IC4xZW07IHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7fVxyXG4gICAgICBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA2MDBweCkge1xyXG4gICAgICAgIHRhYmxlIHsgYm9yZGVyOiAwOyB9XHJcbiAgICAgICAgdGFibGUgY2FwdGlvbiB7IGZvbnQtc2l6ZTogMS4zZW07IH1cclxuICAgICAgICB0YWJsZSB0aGVhZCB7IGJvcmRlcjogbm9uZTsgY2xpcDogcmVjdCgwIDAgMCAwKTsgaGVpZ2h0OiAxcHg7IG1hcmdpbjogLTFweDsgb3ZlcmZsb3c6IGhpZGRlbjsgcGFkZGluZzogMDsgcG9zaXRpb246IGFic29sdXRlOyB3aWR0aDogMXB4O31cclxuICAgICAgICB0YWJsZSB0ciB7IGJvcmRlci1ib3R0b206IDNweCBzb2xpZCAjZGRkOyBkaXNwbGF5OiBibG9jazsgbWFyZ2luLWJvdHRvbTogLjYyNWVtOyB9XHJcbiAgICAgICAgdGFibGUgdGQgeyBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RkZDsgZGlzcGxheTogYmxvY2s7IGZvbnQtc2l6ZTogLjhlbTsgdGV4dC1hbGlnbjogcmlnaHQ7fVxyXG4gICAgICAgIHRhYmxlIHRkOjpiZWZvcmUgeyBjb250ZW50OiBhdHRyKGRhdGEtbGFiZWwpOyBmbG9hdDogbGVmdDsgZm9udC13ZWlnaHQ6IGJvbGQ7IHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7IH1cclxuICAgICAgICB0YWJsZSB0ZDpsYXN0LWNoaWxkIHsgYm9yZGVyLWJvdHRvbTogMDsgfVxyXG4gICAgICB9Il19 */");

/***/ }),

/***/ "47Jg":
/*!**********************************************!*\
  !*** ./src/app/sidebar/sidebar.component.ts ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SidebarComponent = exports.ROUTESUSER = exports.ROUTES = void 0;
var http_1 = __webpack_require__(/*! @angular/common/http */ "tk/3");
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var router_1 = __webpack_require__(/*! @angular/router */ "tyNb");
var environment_1 = __webpack_require__(/*! environments/environment */ "AytR");
exports.ROUTES = [
    // { path: '/dashboard', title: 'Add Map',  icon: 'pe-7s-graph', class: '' },
    // { path: '/table', title: 'Map List',  icon:'pe-7s-note2', class: '' },
    // { path: '/user', title: 'Map View',  icon:'pe-7s-user', class: '' },
    { path: '/my-profile', title: 'My Profile', icon: 'pe-7s-user', class: '' },
    { path: '/cameras', title: 'Cameras', icon: 'pe-7s-user', class: '' },
    { path: '/parking-area', title: 'Parking Area', icon: 'pe-7s-user', class: '' },
    { path: '/users', title: 'Users', icon: 'pe-7s-user', class: '' },
    { path: '/complaints', title: 'Complaints', icon: 'pe-7s-user', class: '' },
    { path: '/test', title: 'Test', icon: 'pe-7s-user', class: '' },
];
exports.ROUTESUSER = [
    { path: '/dashboard', title: 'Parkings', icon: 'pe-7s-graph', class: '' },
    { path: '/profile', title: 'Profile', icon: 'pe-7s-note2', class: '' },
];
var SidebarComponent = /** @class */ (function () {
    function SidebarComponent(http, router) {
        this.http = http;
        this.router = router;
        this.isAdmin = false;
    }
    SidebarComponent.prototype.ngOnInit = function () {
        this.menuItems = exports.ROUTES.filter(function (menuItem) { return menuItem; });
        this.menuItemsUser = exports.ROUTESUSER.filter(function (menuItem) { return menuItem; });
        var userStr = localStorage.getItem('user');
        var user = userStr ? JSON.parse(userStr) : {};
        if (user && user.user_id == 13) {
            this.isAdmin = true;
        }
    };
    SidebarComponent.prototype.isMobileMenu = function () {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
    SidebarComponent.prototype.clearAndLogout = function () {
        var _this = this;
        var headers = new http_1.HttpHeaders();
        headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
        this.http.delete(environment_1.apiEndpoint + 'delete_data', { headers: headers }).subscribe(function (res) {
            localStorage.removeItem('user');
            localStorage.removeItem('token');
            _this.router.navigate(['/login']);
        });
    };
    SidebarComponent.ctorParameters = function () { return [
        { type: http_1.HttpClient },
        { type: router_1.Router }
    ]; };
    SidebarComponent = __decorate([
        core_1.Component({
            selector: 'app-sidebar',
            template: __webpack_require__(/*! raw-loader!./sidebar.component.html */ "zvoc").default
        }),
        __metadata("design:paramtypes", [http_1.HttpClient, router_1.Router])
    ], SidebarComponent);
    return SidebarComponent;
}());
exports.SidebarComponent = SidebarComponent;


/***/ }),

/***/ "4zmU":
/*!****************************************************!*\
  !*** ./src/app/complaints/complaints.component.ts ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ComplaintsComponent = void 0;
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var ComplaintsComponent = /** @class */ (function () {
    function ComplaintsComponent() {
    }
    ComplaintsComponent.prototype.ngOnInit = function () {
    };
    ComplaintsComponent.ctorParameters = function () { return []; };
    ComplaintsComponent = __decorate([
        core_1.Component({
            selector: 'app-complaints',
            template: __webpack_require__(/*! raw-loader!./complaints.component.html */ "SpKp").default,
            styles: [__webpack_require__(/*! ./complaints.component.css */ "wYun").default]
        }),
        __metadata("design:paramtypes", [])
    ], ComplaintsComponent);
    return ComplaintsComponent;
}());
exports.ComplaintsComponent = ComplaintsComponent;


/***/ }),

/***/ "7/Ie":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/my-profile/my-profile.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-6\">\n                <fieldset>\n                    <label for=\"textarea2\">Camera feeds:</label>\n                    <textarea name=\"textarea\" rows=\"10\" id=\"textarea2\" placeholder=\"\"></textarea>\n                </fieldset>\n            </div>\n\n            <div class=\"col-md-6\">\n                <div class=\"rightBox\">\n                    \n                </div>\n            </div>\n        </div>\n        <div class=\"row mt\">\n            <div class=\"col-md-4\">\n                <fieldset>\n                    <label for=\"status\">City</label>\n                    <select name=\"select-choice\" id=\"status\">\n                        <option value=\"Choice 1\">- - select one - -</option>\n                        <option value=\"Choice 2\">Yes!</option>\n                        <option value=\"Choice 3\">They're kinda cool, yeah.</option>\n                        <option value=\"Choice 4\">Meh... not really.</option>\n                        <option value=\"Choice 5\">What's Transformers?</option>\n                    </select>\n                </fieldset>\n            </div>\n\n            <div class=\"col-md-8\">\n                <div class=\"rightBox\">\n                    \n                </div>\n            </div>\n        </div>\n\n\n\n\n        <div class=\"row mt bt\">\n            <div class=\"col-md-6\">\n                <fieldset>\n                    <label for=\"name2\">Name</label>\n                    <input type=\"text\" name=\"name\" id=\"name2\" placeholder=\"Date of issued\">\n                </fieldset>\n            </div>\n\n            <div class=\"col-md-6\">\n                <fieldset>\n                    <label for=\"status\">Complaint Type</label>\n                    <select name=\"select-choice\" id=\"status\">\n                        <option value=\"Choice 1\">- - select one - -</option>\n                        <option value=\"Choice 2\">Yes!</option>\n                        <option value=\"Choice 3\">They're kinda cool, yeah.</option>\n                        <option value=\"Choice 4\">Meh... not really.</option>\n                        <option value=\"Choice 5\">What's Transformers?</option>\n                    </select>\n                </fieldset>\n            </div>\n\n\n            <div class=\"col-md-6\">\n                <fieldset>\n                    <label for=\"textarea2\">Details</label>\n                    <textarea name=\"textarea\" rows=\"10\" id=\"textarea2\" placeholder=\"Fee\"></textarea>\n                </fieldset>\n\n            </div>\n            \n            <div class=\"col-md-6\">\n                <div class=\"row\">\n                    <div class=\"col-md-6\">\n                        <button>Submit</button>\n                    </div>\n                    \n                    <div class=\"col-md-6\">\n                        <button>Clear</button>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n\n\n    </div>\n</div>");

/***/ }),

/***/ "A3xY":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "AK6u":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/layouts/admin-layout/admin-layout.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<div class=\"wrapper\">\n  <div class=\"sidebar\" data-color=\"red\" data-image=\"\">\n      <app-sidebar></app-sidebar>\n      <div class=\"sidebar-background\" style=\"background-image: url(assets/img/sidebar-5.jpg)\"></div>\n  </div>\n\n  <div class=\"main-panel\">\n      <navbar-cmp></navbar-cmp>\n      <router-outlet></router-outlet>\n      <div *ngIf=\"isMap('maps')\">\n          <footer-cmp></footer-cmp>\n      </div>\n  </div>\n\n</div>\n");

/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
Object.defineProperty(exports, "__esModule", { value: true });
exports.apiEndpoint = exports.environment = void 0;
exports.environment = {
    production: false,
};
exports.apiEndpoint = 'http://18.118.226.189/apis/';


/***/ }),

/***/ "CpO+":
/*!************************************************!*\
  !*** ./src/app/shared/navbar/navbar.module.ts ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NavbarModule = void 0;
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var common_1 = __webpack_require__(/*! @angular/common */ "ofXK");
var router_1 = __webpack_require__(/*! @angular/router */ "tyNb");
var navbar_component_1 = __webpack_require__(/*! ./navbar.component */ "EtQq");
var NavbarModule = /** @class */ (function () {
    function NavbarModule() {
    }
    NavbarModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule, common_1.CommonModule],
            declarations: [navbar_component_1.NavbarComponent],
            exports: [navbar_component_1.NavbarComponent]
        })
    ], NavbarModule);
    return NavbarModule;
}());
exports.NavbarModule = NavbarModule;


/***/ }),

/***/ "EtQq":
/*!***************************************************!*\
  !*** ./src/app/shared/navbar/navbar.component.ts ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NavbarComponent = void 0;
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var sidebar_component_1 = __webpack_require__(/*! ../../sidebar/sidebar.component */ "47Jg");
var common_1 = __webpack_require__(/*! @angular/common */ "ofXK");
var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(location, element) {
        this.element = element;
        this.location = location;
        this.sidebarVisible = false;
    }
    NavbarComponent.prototype.ngOnInit = function () {
        this.listTitles = sidebar_component_1.ROUTES.filter(function (listTitle) { return listTitle; });
        var navbar = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
    };
    NavbarComponent.prototype.sidebarOpen = function () {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        body.classList.add('nav-open');
        this.sidebarVisible = true;
    };
    ;
    NavbarComponent.prototype.sidebarClose = function () {
        var body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    };
    ;
    NavbarComponent.prototype.sidebarToggle = function () {
        // const toggleButton = this.toggleButton;
        // const body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        }
        else {
            this.sidebarClose();
        }
    };
    ;
    NavbarComponent.prototype.getTitle = function () {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(1);
        }
        for (var item = 0; item < this.listTitles.length; item++) {
            if (this.listTitles[item].path === titlee) {
                return this.listTitles[item].title;
            }
        }
        return 'Dashboard';
    };
    NavbarComponent.ctorParameters = function () { return [
        { type: common_1.Location },
        { type: core_1.ElementRef }
    ]; };
    NavbarComponent = __decorate([
        core_1.Component({
            // moduleId: module.id,
            selector: 'navbar-cmp',
            template: __webpack_require__(/*! raw-loader!./navbar.component.html */ "zRkE").default
        }),
        __metadata("design:paramtypes", [common_1.Location, core_1.ElementRef])
    ], NavbarComponent);
    return NavbarComponent;
}());
exports.NavbarComponent = NavbarComponent;


/***/ }),

/***/ "K9yR":
/*!****************************************!*\
  !*** ./src/app/test/test.component.ts ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TestComponent = void 0;
var http_1 = __webpack_require__(/*! @angular/common/http */ "tk/3");
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var platform_browser_1 = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
var environment_1 = __webpack_require__(/*! environments/environment */ "AytR");
var TestComponent = /** @class */ (function () {
    function TestComponent(http, ds) {
        this.http = http;
        this.ds = ds;
        this.cameras = [];
        this.cameraSelected = 0;
        this.parkings = [];
        this.date = 123456;
        this.entries = [];
        this.occupied = {};
        this.counter = 0;
        this.getParkings();
        this.getCameras();
    }
    TestComponent.prototype.getUrl = function () {
        // return this.ds.bypassSecurityTrustUrl();
    };
    TestComponent.prototype.ngOnInit = function () {
    };
    TestComponent.prototype.getParkings = function () {
        var _this = this;
        var headers = new http_1.HttpHeaders();
        headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
        this.http.get(environment_1.apiEndpoint + 'car/parking_map', { headers: headers }).subscribe(function (res) {
            _this.parkings = res.data;
        });
    };
    TestComponent.prototype.startTest = function () {
        var _this = this;
        var video = document.getElementById('video');
        var url = 'http://18.118.226.189/media/Camera_' + this.cameraSelected + '.mp4';
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'blob';
        xhr.onload = function (e) {
            if (this.status == 200) {
                var myBlob = this.response;
                video.src = window.URL.createObjectURL(myBlob);
                // myBlob is now the blob that the object URL pointed to.
            }
        };
        xhr.send();
        this.cameras.map(function (cam) {
            if (_this.cameraSelected == cam.id) {
                //     this.cameraSelected = 0;
                //     setTimeout(() => {
                // this.cameraSelected = temp;
                //     }, 2000);
                _this.parkings.map(function (par) {
                    if (par.id == cam.parking_area) {
                        var payload = new FormData();
                        _this.date = new Date();
                        payload.append('camera_url', 'http://18.118.226.189/media/Camera_' + cam.id + '.mp4');
                        payload.append('placement', par.placement);
                        _this.http.post('http://52.14.22.200:8080/start_process', payload).subscribe(function (res) {
                            // this.getDetectings();             
                        });
                        _this.getDetectings(video);
                    }
                });
            }
        });
    };
    TestComponent.prototype.getDetectings = function (video) {
        var _this = this;
        this.http.get('http://52.14.22.200:8080/').subscribe(function (res) {
            console.log(video);
            if (video && res && res.data && res.data[0] && res.data[0].frame_number) {
                _this.counter++;
                if (_this.counter > 10) {
                    video.currentTime = res.data[0].frame_number / 24;
                    _this.counter = 0;
                }
                _this.drawPoints(res);
            }
            setTimeout(function () {
                _this.getDetectings(video);
            }, 500);
        });
    };
    TestComponent.prototype.drawPoints = function (data) {
        if (data && data.data && data.data[0] && data.data[0].values) {
            var c = document.getElementById("myCanvas");
            var ctx = c.getContext("2d");
            ctx.clearRect(0, 0, 500, 400);
            var points = data.data[0].values;
            for (var box2 in points) {
                var box = points[box2].coordinates;
                if (this.entries && this.entries.length < 3) {
                    this.entries.push({
                        time: new Date(),
                        status: false,
                        plat: '',
                        index: box2
                    });
                }
                if (!box.occupiedStatus && this.occupied[box2]) {
                    this.entries.push({
                        time: new Date(),
                        status: false,
                        plat: box.Number_plate,
                        index: box2
                    });
                }
                if (box.occupiedStatus && !this.occupied[box2]) {
                    this.entries.push({
                        time: new Date(),
                        status: true,
                        plat: box.Number_plate,
                        index: box2
                    });
                }
                this.occupied[box2] = box.occupiedStatus;
                ctx.fillStyle = box.occupiedStatus ? '#ff0000' : '#00ff00';
                ctx.beginPath();
                ctx.moveTo.apply(ctx, box.x1); // es5 friendly
                ctx.lineTo.apply(// es5 friendly
                ctx, box.x2); // es6+
                ctx.lineTo.apply(// es6+
                ctx, box.y1);
                ctx.lineTo.apply(ctx, box.y2);
                ctx.lineTo.apply(ctx, box.x1);
                ctx.fillStyle = box.occupiedStatus ? '#ff0000' : '#00ff00';
                ctx.stroke();
            }
        }
        // let c: any = document.getElementById("myCanvas");
        //     let ctx = c.getContext("2d");
        //     let img = document.getElementById("imagePlacer");
        //     ctx.drawImage(img, 10, 10);
    };
    TestComponent.prototype.getCameras = function () {
        var _this = this;
        var headers = new http_1.HttpHeaders();
        headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
        this.http.get(environment_1.apiEndpoint + 'camera/get_cameras', { headers: headers }).subscribe(function (res) {
            _this.cameras = res.data;
        });
    };
    TestComponent.ctorParameters = function () { return [
        { type: http_1.HttpClient },
        { type: platform_browser_1.DomSanitizer }
    ]; };
    TestComponent = __decorate([
        core_1.Component({
            selector: 'app-test',
            template: __webpack_require__(/*! raw-loader!./test.component.html */ "M50W").default,
            styles: [__webpack_require__(/*! ./test.component.css */ "eouj").default]
        }),
        __metadata("design:paramtypes", [http_1.HttpClient, platform_browser_1.DomSanitizer])
    ], TestComponent);
    return TestComponent;
}());
exports.TestComponent = TestComponent;


/***/ }),

/***/ "M50W":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/test/test.component.html ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"main-content\">\n    <div class=\"container-fluid\">\n\n        <div class=\"row mt\">\n            <div class=\"col-md-4\">\n                <fieldset>\n                    <label for=\"status\">Camera</label>\n                    <select name=\"select-choice\" id=\"status\" [(ngModel)]=\"cameraSelected\">\n                        <option value=\"\">- - select one - -</option>\n                        <option [value]=\"cam.id\" *ngFor=\"let cam of cameras\">{{cam.camera_url}}</option>\n                    </select>\n                </fieldset>\n            </div>\n\n            <div class=\"col-md-8\">\n                        <button (click)=\"startTest()\">Start</button>\n            </div>\n        </div>\n        \n        <div class=\"row mt\">\n            <div class=\"col-md-6\"> \n                 \n            <video width=\"320\" height=\"240\" id=\"video\" autoplay *ngIf=\"cameraSelected\">\n                <source src=\"\" type=\"video/mp4\">\n            </video>\n           </div>\n\n            <div class=\"col-md-6\">\n                <canvas id=\"myCanvas\" width=\"500\" height=\"400\">\n\n                </canvas>\n            </div>\n        </div>\n\n\n\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <table>\n                    <thead>\n                      <tr>\n                        <th scope=\"col\">Time</th>\n                        <th scope=\"col\" *ngFor=\"let o of occupied| keyvalue;let i = index\">P{{i+1}}</th>\n                      </tr>\n                    </thead>\n                    <tbody>\n                      <tr *ngFor=\"let entr of entries\">\n                        <td data-label=\"Account\">{{entr.time | date}}</td>\n                        <td data-label=\"Due Date\"  *ngFor=\"let o of occupied| keyvalue;let i = index\"><span *ngIf=\"entr.index == i\">\n{{entr.status ? 'Occupied' : 'Available'}} {{entr.plat ? '('+entr.plat+')' : '(No Plat detected)'}}\n                        </span></td>\n                      </tr>\n                    \n                    </tbody>\n                  </table>\n            </div>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "P6kD":
/*!****************************************************************!*\
  !*** ./src/app/layouts/admin-layout/admin-layout.component.ts ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminLayoutComponent = void 0;
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var common_1 = __webpack_require__(/*! @angular/common */ "ofXK");
__webpack_require__(/*! rxjs/add/operator/filter */ "fjAU");
var router_1 = __webpack_require__(/*! @angular/router */ "tyNb");
var perfect_scrollbar_1 = __webpack_require__(/*! perfect-scrollbar */ "t/UT");
var AdminLayoutComponent = /** @class */ (function () {
    function AdminLayoutComponent(location, router) {
        this.location = location;
        this.router = router;
        this.yScrollStack = [];
    }
    AdminLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log(this.router);
        var isWindows = navigator.platform.indexOf('Win') > -1 ? true : false;
        if (isWindows) {
            // if we are on windows OS we activate the perfectScrollbar function
            document.getElementsByTagName('body')[0].classList.add('perfect-scrollbar-on');
        }
        else {
            document.getElementsByTagName('body')[0].classList.remove('perfect-scrollbar-off');
        }
        var elemMainPanel = document.querySelector('.main-panel');
        var elemSidebar = document.querySelector('.sidebar .sidebar-wrapper');
        this.location.subscribe(function (ev) {
            _this.lastPoppedUrl = ev.url;
        });
        this.router.events.subscribe(function (event) {
            if (event instanceof router_1.NavigationStart) {
                if (event.url != _this.lastPoppedUrl)
                    _this.yScrollStack.push(window.scrollY);
            }
            else if (event instanceof router_1.NavigationEnd) {
                if (event.url == _this.lastPoppedUrl) {
                    _this.lastPoppedUrl = undefined;
                    window.scrollTo(0, _this.yScrollStack.pop());
                }
                else
                    window.scrollTo(0, 0);
            }
        });
        this._router = this.router.events.filter(function (event) { return event instanceof router_1.NavigationEnd; }).subscribe(function (event) {
            elemMainPanel.scrollTop = 0;
            elemSidebar.scrollTop = 0;
        });
        if (window.matchMedia("(min-width: 960px)").matches && !this.isMac()) {
            var ps = new perfect_scrollbar_1.default(elemMainPanel);
            ps = new perfect_scrollbar_1.default(elemSidebar);
        }
    };
    AdminLayoutComponent.prototype.ngAfterViewInit = function () {
        this.runOnRouteChange();
    };
    AdminLayoutComponent.prototype.isMap = function (path) {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        titlee = titlee.slice(1);
        if (path == titlee) {
            return false;
        }
        else {
            return true;
        }
    };
    AdminLayoutComponent.prototype.runOnRouteChange = function () {
        if (window.matchMedia("(min-width: 960px)").matches && !this.isMac()) {
            var elemMainPanel = document.querySelector('.main-panel');
            var ps = new perfect_scrollbar_1.default(elemMainPanel);
            ps.update();
        }
    };
    AdminLayoutComponent.prototype.isMac = function () {
        var bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    };
    AdminLayoutComponent.ctorParameters = function () { return [
        { type: common_1.Location },
        { type: router_1.Router }
    ]; };
    AdminLayoutComponent = __decorate([
        core_1.Component({
            selector: 'app-admin-layout',
            template: __webpack_require__(/*! raw-loader!./admin-layout.component.html */ "AK6u").default,
            styles: [__webpack_require__(/*! ./admin-layout.component.scss */ "vtrx").default]
        }),
        __metadata("design:paramtypes", [common_1.Location, router_1.Router])
    ], AdminLayoutComponent);
    return AdminLayoutComponent;
}());
exports.AdminLayoutComponent = AdminLayoutComponent;


/***/ }),

/***/ "SpKp":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/complaints/complaints.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-6\">\n                <form>\n\n                    <fieldset>\n                        <label for=\"firstName\">User Id</label>\n                        <input type=\"text\" name=\"name\" id=\"firstName\" placeholder=\"User Id\">\n                    </fieldset>\n                    \n\n                    <fieldset>\n                        <label for=\"name\">Name</label>\n                        <input type=\"text\" name=\"name\" id=\"name\" placeholder=\"Name\">\n                    </fieldset>\n                    \n                    <fieldset>\n                        <label for=\"name2\">Date Issued</label>\n                        <input type=\"text\" name=\"name\" id=\"name2\" placeholder=\"Date of issued\">\n                    </fieldset>\n                    <fieldset>\n                        <label for=\"status\">Status</label>\n                        <select name=\"select-choice\" id=\"status\">\n                            <option value=\"Choice 1\">- - select one - -</option>\n                            <option value=\"Choice 2\">Yes!</option>\n                            <option value=\"Choice 3\">They're kinda cool, yeah.</option>\n                            <option value=\"Choice 4\">Meh... not really.</option>\n                            <option value=\"Choice 5\">What's Transformers?</option>\n                        </select>\n                    </fieldset>\n                    <fieldset>\n                        <label for=\"select-choice\">Complainet Type</label>\n                        <select name=\"select-choice\" id=\"select-choice\">\n                            <option value=\"Choice 1\">- - select one - -</option>\n                            <option value=\"Choice 2\">Yes!</option>\n                            <option value=\"Choice 3\">They're kinda cool, yeah.</option>\n                            <option value=\"Choice 4\">Meh... not really.</option>\n                            <option value=\"Choice 5\">What's Transformers?</option>\n                        </select>\n                    </fieldset>\n                    <fieldset>\n                        <label for=\"textarea\">Fee</label>\n                        <input name=\"textarea\" type=\"text\" min=\"0\" id=\"textarea\" placeholder=\"Fee\">\n                    </fieldset>\n\n                </form>\n            </div>\n\n            <div class=\"col-md-6\">\n                <div class=\"rightBox\">\n                    <fieldset>\n                        <label for=\"textarea2\">Details</label>\n                        <textarea name=\"textarea\" type=\"number\" min=\"0\" id=\"textarea2\" placeholder=\"Fee\"></textarea>\n                    </fieldset>\n                </div>\n                <div class=\"rightBox\">\n            \n                    <fieldset>\n                        <legend>Images</legend>\n                      \n                      </fieldset>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-3\">\n                <button>Add Complaint</button>\n            </div>\n\n            <div class=\"col-md-3\">\n                <button>Save</button>\n            </div>\n            <div class=\"col-md-3\">\n                <button>Modify</button>\n            </div>\n            <div class=\"col-md-3\">\n                <button>Delete</button>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <table>\n                    <thead>\n                      <tr>\n                        <th scope=\"col\">Name</th>\n                        <th scope=\"col\">Date issued</th>\n                        <th scope=\"col\">Status</th>\n                        <th scope=\"col\">Complaint Type</th>\n                        <th scope=\"col\">Fee</th>\n                        <th scope=\"col\">Details</th>\n                        <th scope=\"col\">Images</th>\n                      </tr>\n                    </thead>\n                    <tbody>\n                      <tr>\n                        <td data-label=\"Account\">KK</td>\n                        <td data-label=\"Due Date\">21/08/2021</td>\n                        <td data-label=\"Amount\">Active</td>\n                        <td data-label=\"Period\">T1</td>\n                        <td data-label=\"Period\">20$</td>\n                        <td data-label=\"Period\">All</td>\n                        <td data-label=\"Period\">I1, I2, I3</td>\n                      </tr>\n                    \n                    </tbody>\n                  </table>\n            </div>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppComponent = void 0;
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var common_1 = __webpack_require__(/*! @angular/common */ "ofXK");
var router_1 = __webpack_require__(/*! @angular/router */ "tyNb");
var AppComponent = /** @class */ (function () {
    function AppComponent(location, router) {
        this.location = location;
        this.router = router;
    }
    AppComponent.prototype.ngOnInit = function () {
        var token = localStorage.getItem('token');
        if (token && token.length > 0) {
            this.router.navigate(['/cameras']);
        }
        else {
            this.router.navigate(['/login']);
        }
    };
    AppComponent.prototype.isMap = function (path) {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        titlee = titlee.slice(1);
        if (path == titlee) {
            return false;
        }
        else {
            return true;
        }
    };
    AppComponent.ctorParameters = function () { return [
        { type: common_1.Location },
        { type: router_1.Router }
    ]; };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "VzVu").default,
            styles: [__webpack_require__(/*! ./app.component.css */ "A3xY").default]
        }),
        __metadata("design:paramtypes", [common_1.Location, router_1.Router])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;


/***/ }),

/***/ "Sz8D":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/resize/resize.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div #box class=\"resizable-draggable\" \n    [style.width.px]=\"width\" \n    [style.height.px]=\"height\" \n    [style.transform]=\"'translate3d('+ left + 'px,' + top + 'px,' + '0px)'\"\n    [class.active]=\"status === 1 || status === 2\"\n    (mousedown)=\"setStatus($event, 2)\"\n    (window:mouseup)=\"setStatus($event, 0)\"\n>\n    <div class=\"resize-action\" (mousedown)=\"setStatus($event, 1)\"></div>\n    <span>{{width | number:'1.0-0'}}px</span>\n    <span>{{height | number:'1.0-0'}}px</span>\n    <span>({{left}}, {{top}})</span>\n</div>");

/***/ }),

/***/ "Tu9x":
/*!*******************************************!*\
  !*** ./src/app/users/users.component.css ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@import url(https://fonts.googleapis.com/css?family=Roboto:400,300);\r\n* {\r\n  box-sizing: border-box;\r\n}\r\nhtml {\r\n  height: 100%;\r\n}\r\nbody {\r\n  height: 100%;\r\n  font-family: Roboto, san-serif;\r\n  font-weight: 300;\r\n  background-color: #f5f6f7;\r\n}\r\n/* Form element setup */\r\nform {\r\n    \r\n  /* position: absolute; */\r\n  top: 0;\r\n  /* left: 50%; */\r\n  width: 300px;\r\n  /* transform: translateX(-50%); */\r\n  margin: 2rem 0;\r\n  z-index: 1;\r\n}\r\nfieldset {\r\n  margin: 0 0 1rem 0;\r\n  padding: 0;\r\n  border: none;\r\n}\r\nlegend {\r\n  font-weight: 400;\r\n}\r\nlegend,\r\nlabel {\r\n  display: inline-block;\r\n  margin-bottom: 0.5rem;\r\n}\r\ninput[type=text],\r\ntextarea,\r\nselect {\r\n  display: block;\r\n  padding: 0.5rem;\r\n  width: 100%;\r\n  background-color: white;\r\n  border-radius: 0.25rem;\r\n  border: 1px solid #e5e5e5;\r\n  outline: none;\r\n  /* List some properties that might change */\r\n  transition-property: none;\r\n  transition-duration: none;\r\n}\r\ninput[type=text]:focus,\r\ntextarea:focus,\r\nselect:focus {\r\n  border-color: #ef7ead;\r\n}\r\ntextarea {\r\n  max-width: 300px;\r\n  height: 100px;\r\n}\r\ninput[type=text],\r\nselect {\r\n  height: 34px;\r\n}\r\nselect {\r\n  font-size: 0.875rem;\r\n}\r\ninput[type=checkbox],\r\ninput[type=radio] {\r\n  position: relative;\r\n  top: 5px;\r\n  width: 22px;\r\n  height: 22px;\r\n  margin: 0 0.5rem;\r\n  background-color: white;\r\n  border: 1px solid #e5e5e5;\r\n  outline: none;\r\n  -moz-appearance: none;\r\n  -webkit-appearance: none;\r\n  /* List some properties that might change */\r\n  transition-property: none;\r\n  transition-duration: none;\r\n}\r\ninput[type=checkbox] {\r\n  border-radius: 5px;\r\n}\r\ninput[type=checkbox]:checked {\r\n  background-color: #ef7ead;\r\n  border: none;\r\n}\r\ninput[type=checkbox]:checked:after {\r\n  display: block;\r\n  content: \"\";\r\n  height: 4px;\r\n  width: 10px;\r\n  border-bottom: 3px solid #fff;\r\n  border-left: 3px solid #fff;\r\n  transform: translate(5px, 6px) rotate(-45deg) scale(1);\r\n}\r\ninput[type=radio] {\r\n  border-radius: 50%;\r\n}\r\ninput[type=radio]:checked {\r\n  border-width: 5px;\r\n  border-color: white;\r\n  background-color: #ef7ead;\r\n}\r\nbutton {\r\n  display: block;\r\n  margin: 3em auto;\r\n  padding: 0.5rem 2rem;\r\n  font-size: 125%;\r\n  color: white;\r\n  border: none;\r\n  border-radius: 0.25rem;\r\n  background-color: #ef7ead;\r\n  outline: none;\r\n  box-shadow: 0 0.4rem 0.1rem -0.3rem rgba(0, 0, 0, 0.1);\r\n  /* We'll talk about this next */\r\n  transform: perspective(300px) scale(0.95) translateZ(0);\r\n  transform-style: preserve-3d;\r\n  /* List the properties that you're looking to transition.\r\n     Try not to use 'all' */\r\n  transition-property: none;\r\n  /* This applies to all of the above properties */\r\n  transition-duration: none;\r\n}\r\nbutton:hover {\r\n  cursor: pointer;\r\n  background-color: #ff96c8;\r\n  box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);\r\n  transform: scale(1.1) rotateX(0);\r\n}\r\nbutton:active {\r\n  background-color: #ef7ead;\r\n  transform: scale(1.05) rotateX(-10deg);\r\n}\r\n.rightBox {\r\n    border: 1px solid;\r\n    width: 100%;\r\n    height: 30vh;\r\n}\r\ntable { border: 1px solid #ccc; border-collapse: collapse; margin: 0; padding: 0; width: 100%; table-layout: fixed;}\r\ntable caption { font-size: 1.5em; margin: .5em 0 .75em;}\r\ntable tr { border: 1px solid #ddd; padding: .35em;}\r\ntable th,\r\n      table td { padding: .625em; text-align: center;}\r\ntable th { font-size: .85em; letter-spacing: .1em; text-transform: uppercase;}\r\n@media screen and (max-width: 600px) {\r\n        table { border: 0; }\r\n        table caption { font-size: 1.3em; }\r\n        table thead { border: none; clip: rect(0 0 0 0); height: 1px; margin: -1px; overflow: hidden; padding: 0; position: absolute; width: 1px;}\r\n        table tr { border-bottom: 3px solid #ddd; display: block; margin-bottom: .625em; }\r\n        table td { border-bottom: 1px solid #ddd; display: block; font-size: .8em; text-align: right;}\r\n        table td::before { content: attr(data-label); float: left; font-weight: bold; text-transform: uppercase; }\r\n        table td:last-child { border-bottom: 0; }\r\n      }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVzZXJzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsbUVBQW1FO0FBQ25FO0VBQ0Usc0JBQXNCO0FBQ3hCO0FBRUE7RUFDRSxZQUFZO0FBQ2Q7QUFFQTtFQUNFLFlBQVk7RUFDWiw4QkFBOEI7RUFDOUIsZ0JBQWdCO0VBQ2hCLHlCQUF5QjtBQUMzQjtBQUVBLHVCQUF1QjtBQUN2Qjs7RUFFRSx3QkFBd0I7RUFDeEIsTUFBTTtFQUNOLGVBQWU7RUFDZixZQUFZO0VBQ1osaUNBQWlDO0VBQ2pDLGNBQWM7RUFDZCxVQUFVO0FBQ1o7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsWUFBWTtBQUNkO0FBRUE7RUFDRSxnQkFBZ0I7QUFDbEI7QUFFQTs7RUFFRSxxQkFBcUI7RUFDckIscUJBQXFCO0FBQ3ZCO0FBRUE7OztFQUdFLGNBQWM7RUFDZCxlQUFlO0VBQ2YsV0FBVztFQUNYLHVCQUF1QjtFQUN2QixzQkFBc0I7RUFDdEIseUJBQXlCO0VBQ3pCLGFBQWE7RUFDYiwyQ0FBMkM7RUFDM0MseUJBQXlCO0VBQ3pCLHlCQUF5QjtBQUMzQjtBQUNBOzs7RUFHRSxxQkFBcUI7QUFDdkI7QUFFQTtFQUNFLGdCQUFnQjtFQUNoQixhQUFhO0FBQ2Y7QUFFQTs7RUFFRSxZQUFZO0FBQ2Q7QUFFQTtFQUNFLG1CQUFtQjtBQUNyQjtBQUVBOztFQUVFLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsV0FBVztFQUNYLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsdUJBQXVCO0VBQ3ZCLHlCQUF5QjtFQUN6QixhQUFhO0VBQ2IscUJBQXFCO0VBQ3JCLHdCQUF3QjtFQUN4QiwyQ0FBMkM7RUFDM0MseUJBQXlCO0VBQ3pCLHlCQUF5QjtBQUMzQjtBQUVBO0VBQ0Usa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSx5QkFBeUI7RUFDekIsWUFBWTtBQUNkO0FBQ0E7RUFDRSxjQUFjO0VBQ2QsV0FBVztFQUNYLFdBQVc7RUFDWCxXQUFXO0VBQ1gsNkJBQTZCO0VBQzdCLDJCQUEyQjtFQUMzQixzREFBc0Q7QUFDeEQ7QUFFQTtFQUNFLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQix5QkFBeUI7QUFDM0I7QUFFQTtFQUNFLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFDZixZQUFZO0VBQ1osWUFBWTtFQUNaLHNCQUFzQjtFQUN0Qix5QkFBeUI7RUFDekIsYUFBYTtFQUNiLHNEQUFzRDtFQUN0RCwrQkFBK0I7RUFDL0IsdURBQXVEO0VBQ3ZELDRCQUE0QjtFQUM1QjsyQkFDeUI7RUFDekIseUJBQXlCO0VBQ3pCLGdEQUFnRDtFQUNoRCx5QkFBeUI7QUFDM0I7QUFDQTtFQUNFLGVBQWU7RUFDZix5QkFBeUI7RUFDekIsb0NBQW9DO0VBQ3BDLGdDQUFnQztBQUNsQztBQUNBO0VBQ0UseUJBQXlCO0VBQ3pCLHNDQUFzQztBQUN4QztBQUdBO0lBQ0ksaUJBQWlCO0lBQ2pCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBR0EsUUFBUSxzQkFBc0IsRUFBRSx5QkFBeUIsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxtQkFBbUIsQ0FBQztBQUM3RyxnQkFBZ0IsZ0JBQWdCLEVBQUUsb0JBQW9CLENBQUM7QUFDdkQsV0FBVyxzQkFBc0IsRUFBRSxjQUFjLENBQUM7QUFDbEQ7aUJBQ1csZUFBZSxFQUFFLGtCQUFrQixDQUFDO0FBQy9DLFdBQVcsZ0JBQWdCLEVBQUUsb0JBQW9CLEVBQUUseUJBQXlCLENBQUM7QUFDN0U7UUFDRSxRQUFRLFNBQVMsRUFBRTtRQUNuQixnQkFBZ0IsZ0JBQWdCLEVBQUU7UUFDbEMsY0FBYyxZQUFZLEVBQUUsbUJBQW1CLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxnQkFBZ0IsRUFBRSxVQUFVLEVBQUUsa0JBQWtCLEVBQUUsVUFBVSxDQUFDO1FBQ3pJLFdBQVcsNkJBQTZCLEVBQUUsY0FBYyxFQUFFLHFCQUFxQixFQUFFO1FBQ2pGLFdBQVcsNkJBQTZCLEVBQUUsY0FBYyxFQUFFLGVBQWUsRUFBRSxpQkFBaUIsQ0FBQztRQUM3RixtQkFBbUIseUJBQXlCLEVBQUUsV0FBVyxFQUFFLGlCQUFpQixFQUFFLHlCQUF5QixFQUFFO1FBQ3pHLHNCQUFzQixnQkFBZ0IsRUFBRTtNQUMxQyIsImZpbGUiOiJ1c2Vycy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCB1cmwoaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PVJvYm90bzo0MDAsMzAwKTtcclxuKiB7XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxufVxyXG5cclxuaHRtbCB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG5ib2R5IHtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgZm9udC1mYW1pbHk6IFJvYm90bywgc2FuLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiAzMDA7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y1ZjZmNztcclxufVxyXG5cclxuLyogRm9ybSBlbGVtZW50IHNldHVwICovXHJcbmZvcm0ge1xyXG4gICAgXHJcbiAgLyogcG9zaXRpb246IGFic29sdXRlOyAqL1xyXG4gIHRvcDogMDtcclxuICAvKiBsZWZ0OiA1MCU7ICovXHJcbiAgd2lkdGg6IDMwMHB4O1xyXG4gIC8qIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKTsgKi9cclxuICBtYXJnaW46IDJyZW0gMDtcclxuICB6LWluZGV4OiAxO1xyXG59XHJcblxyXG5maWVsZHNldCB7XHJcbiAgbWFyZ2luOiAwIDAgMXJlbSAwO1xyXG4gIHBhZGRpbmc6IDA7XHJcbiAgYm9yZGVyOiBub25lO1xyXG59XHJcblxyXG5sZWdlbmQge1xyXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbn1cclxuXHJcbmxlZ2VuZCxcclxubGFiZWwge1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XHJcbn1cclxuXHJcbmlucHV0W3R5cGU9dGV4dF0sXHJcbnRleHRhcmVhLFxyXG5zZWxlY3Qge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHBhZGRpbmc6IDAuNXJlbTtcclxuICB3aWR0aDogMTAwJTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICBib3JkZXItcmFkaXVzOiAwLjI1cmVtO1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNlNWU1ZTU7XHJcbiAgb3V0bGluZTogbm9uZTtcclxuICAvKiBMaXN0IHNvbWUgcHJvcGVydGllcyB0aGF0IG1pZ2h0IGNoYW5nZSAqL1xyXG4gIHRyYW5zaXRpb24tcHJvcGVydHk6IG5vbmU7XHJcbiAgdHJhbnNpdGlvbi1kdXJhdGlvbjogbm9uZTtcclxufVxyXG5pbnB1dFt0eXBlPXRleHRdOmZvY3VzLFxyXG50ZXh0YXJlYTpmb2N1cyxcclxuc2VsZWN0OmZvY3VzIHtcclxuICBib3JkZXItY29sb3I6ICNlZjdlYWQ7XHJcbn1cclxuXHJcbnRleHRhcmVhIHtcclxuICBtYXgtd2lkdGg6IDMwMHB4O1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbn1cclxuXHJcbmlucHV0W3R5cGU9dGV4dF0sXHJcbnNlbGVjdCB7XHJcbiAgaGVpZ2h0OiAzNHB4O1xyXG59XHJcblxyXG5zZWxlY3Qge1xyXG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XHJcbn1cclxuXHJcbmlucHV0W3R5cGU9Y2hlY2tib3hdLFxyXG5pbnB1dFt0eXBlPXJhZGlvXSB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHRvcDogNXB4O1xyXG4gIHdpZHRoOiAyMnB4O1xyXG4gIGhlaWdodDogMjJweDtcclxuICBtYXJnaW46IDAgMC41cmVtO1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNlNWU1ZTU7XHJcbiAgb3V0bGluZTogbm9uZTtcclxuICAtbW96LWFwcGVhcmFuY2U6IG5vbmU7XHJcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lO1xyXG4gIC8qIExpc3Qgc29tZSBwcm9wZXJ0aWVzIHRoYXQgbWlnaHQgY2hhbmdlICovXHJcbiAgdHJhbnNpdGlvbi1wcm9wZXJ0eTogbm9uZTtcclxuICB0cmFuc2l0aW9uLWR1cmF0aW9uOiBub25lO1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPWNoZWNrYm94XSB7XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG59XHJcbmlucHV0W3R5cGU9Y2hlY2tib3hdOmNoZWNrZWQge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlZjdlYWQ7XHJcbiAgYm9yZGVyOiBub25lO1xyXG59XHJcbmlucHV0W3R5cGU9Y2hlY2tib3hdOmNoZWNrZWQ6YWZ0ZXIge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIGNvbnRlbnQ6IFwiXCI7XHJcbiAgaGVpZ2h0OiA0cHg7XHJcbiAgd2lkdGg6IDEwcHg7XHJcbiAgYm9yZGVyLWJvdHRvbTogM3B4IHNvbGlkICNmZmY7XHJcbiAgYm9yZGVyLWxlZnQ6IDNweCBzb2xpZCAjZmZmO1xyXG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKDVweCwgNnB4KSByb3RhdGUoLTQ1ZGVnKSBzY2FsZSgxKTtcclxufVxyXG5cclxuaW5wdXRbdHlwZT1yYWRpb10ge1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxufVxyXG5pbnB1dFt0eXBlPXJhZGlvXTpjaGVja2VkIHtcclxuICBib3JkZXItd2lkdGg6IDVweDtcclxuICBib3JkZXItY29sb3I6IHdoaXRlO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlZjdlYWQ7XHJcbn1cclxuXHJcbmJ1dHRvbiB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgbWFyZ2luOiAzZW0gYXV0bztcclxuICBwYWRkaW5nOiAwLjVyZW0gMnJlbTtcclxuICBmb250LXNpemU6IDEyNSU7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGJvcmRlcjogbm9uZTtcclxuICBib3JkZXItcmFkaXVzOiAwLjI1cmVtO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlZjdlYWQ7XHJcbiAgb3V0bGluZTogbm9uZTtcclxuICBib3gtc2hhZG93OiAwIDAuNHJlbSAwLjFyZW0gLTAuM3JlbSByZ2JhKDAsIDAsIDAsIDAuMSk7XHJcbiAgLyogV2UnbGwgdGFsayBhYm91dCB0aGlzIG5leHQgKi9cclxuICB0cmFuc2Zvcm06IHBlcnNwZWN0aXZlKDMwMHB4KSBzY2FsZSgwLjk1KSB0cmFuc2xhdGVaKDApO1xyXG4gIHRyYW5zZm9ybS1zdHlsZTogcHJlc2VydmUtM2Q7XHJcbiAgLyogTGlzdCB0aGUgcHJvcGVydGllcyB0aGF0IHlvdSdyZSBsb29raW5nIHRvIHRyYW5zaXRpb24uXHJcbiAgICAgVHJ5IG5vdCB0byB1c2UgJ2FsbCcgKi9cclxuICB0cmFuc2l0aW9uLXByb3BlcnR5OiBub25lO1xyXG4gIC8qIFRoaXMgYXBwbGllcyB0byBhbGwgb2YgdGhlIGFib3ZlIHByb3BlcnRpZXMgKi9cclxuICB0cmFuc2l0aW9uLWR1cmF0aW9uOiBub25lO1xyXG59XHJcbmJ1dHRvbjpob3ZlciB7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZjk2Yzg7XHJcbiAgYm94LXNoYWRvdzogMCAwIDAgMCByZ2JhKDAsIDAsIDAsIDApO1xyXG4gIHRyYW5zZm9ybTogc2NhbGUoMS4xKSByb3RhdGVYKDApO1xyXG59XHJcbmJ1dHRvbjphY3RpdmUge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlZjdlYWQ7XHJcbiAgdHJhbnNmb3JtOiBzY2FsZSgxLjA1KSByb3RhdGVYKC0xMGRlZyk7XHJcbn1cclxuXHJcblxyXG4ucmlnaHRCb3gge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMzB2aDtcclxufVxyXG5cclxuXHJcbnRhYmxlIHsgYm9yZGVyOiAxcHggc29saWQgI2NjYzsgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTsgbWFyZ2luOiAwOyBwYWRkaW5nOiAwOyB3aWR0aDogMTAwJTsgdGFibGUtbGF5b3V0OiBmaXhlZDt9XHJcbiAgICAgIHRhYmxlIGNhcHRpb24geyBmb250LXNpemU6IDEuNWVtOyBtYXJnaW46IC41ZW0gMCAuNzVlbTt9XHJcbiAgICAgIHRhYmxlIHRyIHsgYm9yZGVyOiAxcHggc29saWQgI2RkZDsgcGFkZGluZzogLjM1ZW07fVxyXG4gICAgICB0YWJsZSB0aCxcclxuICAgICAgdGFibGUgdGQgeyBwYWRkaW5nOiAuNjI1ZW07IHRleHQtYWxpZ246IGNlbnRlcjt9XHJcbiAgICAgIHRhYmxlIHRoIHsgZm9udC1zaXplOiAuODVlbTsgbGV0dGVyLXNwYWNpbmc6IC4xZW07IHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7fVxyXG4gICAgICBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA2MDBweCkge1xyXG4gICAgICAgIHRhYmxlIHsgYm9yZGVyOiAwOyB9XHJcbiAgICAgICAgdGFibGUgY2FwdGlvbiB7IGZvbnQtc2l6ZTogMS4zZW07IH1cclxuICAgICAgICB0YWJsZSB0aGVhZCB7IGJvcmRlcjogbm9uZTsgY2xpcDogcmVjdCgwIDAgMCAwKTsgaGVpZ2h0OiAxcHg7IG1hcmdpbjogLTFweDsgb3ZlcmZsb3c6IGhpZGRlbjsgcGFkZGluZzogMDsgcG9zaXRpb246IGFic29sdXRlOyB3aWR0aDogMXB4O31cclxuICAgICAgICB0YWJsZSB0ciB7IGJvcmRlci1ib3R0b206IDNweCBzb2xpZCAjZGRkOyBkaXNwbGF5OiBibG9jazsgbWFyZ2luLWJvdHRvbTogLjYyNWVtOyB9XHJcbiAgICAgICAgdGFibGUgdGQgeyBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RkZDsgZGlzcGxheTogYmxvY2s7IGZvbnQtc2l6ZTogLjhlbTsgdGV4dC1hbGlnbjogcmlnaHQ7fVxyXG4gICAgICAgIHRhYmxlIHRkOjpiZWZvcmUgeyBjb250ZW50OiBhdHRyKGRhdGEtbGFiZWwpOyBmbG9hdDogbGVmdDsgZm9udC13ZWlnaHQ6IGJvbGQ7IHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7IH1cclxuICAgICAgICB0YWJsZSB0ZDpsYXN0LWNoaWxkIHsgYm9yZGVyLWJvdHRvbTogMDsgfVxyXG4gICAgICB9Il19 */");

/***/ }),

/***/ "VzVu":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<router-outlet></router-outlet>\n");

/***/ }),

/***/ "W6KJ":
/*!**********************************************!*\
  !*** ./src/app/profile/profile.component.ts ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileComponent = void 0;
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var ProfileComponent = /** @class */ (function () {
    function ProfileComponent() {
    }
    ProfileComponent.prototype.ngOnInit = function () {
    };
    ProfileComponent.ctorParameters = function () { return []; };
    ProfileComponent = __decorate([
        core_1.Component({
            selector: 'app-profile',
            template: __webpack_require__(/*! raw-loader!./profile.component.html */ "xwfu").default,
            styles: [__webpack_require__(/*! ./profile.component.css */ "fMGI").default]
        }),
        __metadata("design:paramtypes", [])
    ], ProfileComponent);
    return ProfileComponent;
}());
exports.ProfileComponent = ProfileComponent;


/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
var animations_1 = __webpack_require__(/*! @angular/platform-browser/animations */ "R1ws");
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var forms_1 = __webpack_require__(/*! @angular/forms */ "3Pt+");
var http_1 = __webpack_require__(/*! @angular/common/http */ "tk/3");
var router_1 = __webpack_require__(/*! @angular/router */ "tyNb");
var app_routing_1 = __webpack_require__(/*! ./app.routing */ "beVS");
var navbar_module_1 = __webpack_require__(/*! ./shared/navbar/navbar.module */ "CpO+");
var footer_module_1 = __webpack_require__(/*! ./shared/footer/footer.module */ "cNqA");
var sidebar_module_1 = __webpack_require__(/*! ./sidebar/sidebar.module */ "wCP4");
var app_component_1 = __webpack_require__(/*! ./app.component */ "Sy1n");
var admin_layout_component_1 = __webpack_require__(/*! ./layouts/admin-layout/admin-layout.component */ "P6kD");
var login_component_1 = __webpack_require__(/*! ./login/login.component */ "vtpD");
var profile_component_1 = __webpack_require__(/*! ./profile/profile.component */ "W6KJ");
var parking_area_component_1 = __webpack_require__(/*! ./parking-area/parking-area.component */ "owvD");
var users_component_1 = __webpack_require__(/*! ./users/users.component */ "oYre");
var cameras_component_1 = __webpack_require__(/*! ./cameras/cameras.component */ "dE4j");
var complaints_component_1 = __webpack_require__(/*! ./complaints/complaints.component */ "4zmU");
var my_profile_component_1 = __webpack_require__(/*! ./my-profile/my-profile.component */ "kMBp");
var test_component_1 = __webpack_require__(/*! ./test/test.component */ "K9yR");
var ngx_toastr_1 = __webpack_require__(/*! ngx-toastr */ "EApP");
var resize_component_1 = __webpack_require__(/*! ./resize/resize.component */ "dRrA");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                animations_1.BrowserAnimationsModule,
                forms_1.FormsModule,
                router_1.RouterModule,
                http_1.HttpClientModule,
                navbar_module_1.NavbarModule,
                forms_1.ReactiveFormsModule,
                footer_module_1.FooterModule,
                sidebar_module_1.SidebarModule,
                app_routing_1.AppRoutingModule,
                ngx_toastr_1.ToastrModule.forRoot()
            ],
            declarations: [
                app_component_1.AppComponent,
                admin_layout_component_1.AdminLayoutComponent,
                login_component_1.LoginComponent,
                profile_component_1.ProfileComponent,
                parking_area_component_1.ParkingAreaComponent,
                users_component_1.UsersComponent,
                cameras_component_1.CamerasComponent,
                complaints_component_1.ComplaintsComponent,
                my_profile_component_1.MyProfileComponent,
                test_component_1.TestComponent,
                resize_component_1.ResizableDraggableComponent
            ],
            providers: [],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;


/***/ }),

/***/ "aO28":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cameras/cameras.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-6\">\n                <form>\n\n                    <fieldset>\n                        <label for=\"firstName\">Id</label>\n                        <input type=\"text\" name=\"namejk\" readonly ngDefaultControl disabled id=\"firstName\" [(ngModel)]=\"cameraDetails.id\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Id\">\n                    </fieldset>\n                    \n                    <input type=\"file\" #input\n                    id=\"file\" style=\"display: none\"\n                    (change)=\"handleFileInput($event.target.files)\">\n                    <fieldset>\n                        <label for=\"name\" class=\"w60\">Link</label>\n                        <input type=\"text\" class=\"ib w60\" ngDefaultControl  [(ngModel)]=\"cameraDetails.camera_url\" [ngModelOptions]=\"{standalone: true}\" name=\"name\" id=\"name\" placeholder=\"Link\">\n                        <button class=\"ib w40 no-margin\" (click)=\"input.click()\">Add Video</button>\n                    </fieldset>\n\n                    <fieldset>\n                        <label for=\"select-choice\" >Parking Area</label>\n                        <select name=\"select-choice\" ngDefaultControl  [(ngModel)]=\"cameraDetails.parking_area\" [ngModelOptions]=\"{standalone: true}\" id=\"select-choice\">\n                            <option value=\"0\">- - select one - -</option>\n                            <option *ngFor=\"let par of parkings\" [value]=\"par.id\">{{par.address}}</option>\n                        </select>\n                    </fieldset>\n                    <fieldset>\n                        <label for=\"textarea\">Coordinated</label>\n                        <input name=\"textarea\" type=\"text\" id=\"textarea\" ngDefaultControl  placeholder=\"Coordinates\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"cameraDetails.co_ordinates\">\n                    </fieldset>\n                    \n                </form>\n            </div>\n\n            <div class=\"col-md-6\">\n                <div class=\"rightBox\">\n            \n                    <div id=\"googleMap\" style=\"width:100%;height:400px;\"></div>\n\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-3\">\n                <button (click)=\"addCamera()\">{{cameraDetails.id ? 'Update' : 'Save'}}</button>\n            </div>\n            <div class=\"col-md-3\">\n                <button (click)=\"delete()\" *ngIf=\"cameraDetails.id\">Delete</button>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <table>\n                    <thead>\n                      <tr>\n                        <th scope=\"col\">Id</th>\n                        <th scope=\"col\">Link</th>\n                        <th scope=\"col\">Coordinates</th>\n                        <th scope=\"col\">Parking area</th>\n                      </tr>\n                    </thead>\n                    <tbody>\n                      <tr *ngFor=\"let cam of cameras\" (click)=\"cameraDetails = cam\">\n                        <td data-label=\"Account\">{{cam.id}}</td>\n                        <td data-label=\"Due Date\">{{cam.camera_url}}</td>\n                        <td data-label=\"Amount\">{{cam.co_ordinates}}</td>\n                        <td data-label=\"Period\">PA-{{cam.parking_area}}</td>\n                      </tr>\n                    \n                    </tbody>\n                  </table>\n            </div>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "beVS":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppRoutingModule = void 0;
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var common_1 = __webpack_require__(/*! @angular/common */ "ofXK");
var platform_browser_1 = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
var router_1 = __webpack_require__(/*! @angular/router */ "tyNb");
var admin_layout_component_1 = __webpack_require__(/*! ./layouts/admin-layout/admin-layout.component */ "P6kD");
var login_component_1 = __webpack_require__(/*! ./login/login.component */ "vtpD");
var routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    { path: 'login', component: login_component_1.LoginComponent },
    {
        path: '',
        component: admin_layout_component_1.AdminLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
            }
        ]
    },
    {
        path: '**',
        redirectTo: 'dashboard'
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                platform_browser_1.BrowserModule,
                router_1.RouterModule.forRoot(routes, {
                    useHash: true
                })
            ],
            exports: [],
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;


/***/ }),

/***/ "cNqA":
/*!************************************************!*\
  !*** ./src/app/shared/footer/footer.module.ts ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FooterModule = void 0;
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var common_1 = __webpack_require__(/*! @angular/common */ "ofXK");
var router_1 = __webpack_require__(/*! @angular/router */ "tyNb");
var footer_component_1 = __webpack_require__(/*! ./footer.component */ "jQpT");
var FooterModule = /** @class */ (function () {
    function FooterModule() {
    }
    FooterModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule, common_1.CommonModule],
            declarations: [footer_component_1.FooterComponent],
            exports: [footer_component_1.FooterComponent]
        })
    ], FooterModule);
    return FooterModule;
}());
exports.FooterModule = FooterModule;


/***/ }),

/***/ "crnd":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./layouts/admin-layout/admin-layout.module": [
		"IqXj",
		"layouts-admin-layout-admin-layout-module"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__.t(id, 7);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "crnd";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "dDHc":
/*!***********************************************!*\
  !*** ./src/app/cameras/cameras.component.css ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@import url(https://fonts.googleapis.com/css?family=Roboto:400,300);\r\n* {\r\n  box-sizing: border-box;\r\n}\r\nhtml {\r\n  height: 100%;\r\n}\r\nbody {\r\n  height: 100%;\r\n  font-family: Roboto, san-serif;\r\n  font-weight: 300;\r\n  background-color: #f5f6f7;\r\n}\r\n/* Form element setup */\r\nform {\r\n    \r\n  /* position: absolute; */\r\n  top: 0;\r\n  /* left: 50%; */\r\n  width: 300px;\r\n  /* transform: translateX(-50%); */\r\n  margin: 2rem 0;\r\n  z-index: 1;\r\n}\r\nfieldset {\r\n  margin: 0 0 1rem 0;\r\n  padding: 0;\r\n  border: none;\r\n}\r\nlegend {\r\n  font-weight: 400;\r\n}\r\nlegend,\r\nlabel {\r\n  display: inline-block;\r\n  margin-bottom: 0.5rem;\r\n}\r\ninput[type=text],\r\ntextarea,\r\nselect {\r\n  display: block;\r\n  padding: 0.5rem;\r\n  width: 100%;\r\n  background-color: white;\r\n  border-radius: 0.25rem;\r\n  border: 1px solid #e5e5e5;\r\n  outline: none;\r\n  /* List some properties that might change */\r\n  transition-property: none;\r\n  transition-duration: none;\r\n}\r\ninput[type=text]:focus,\r\ntextarea:focus,\r\nselect:focus {\r\n  border-color: #ef7ead;\r\n}\r\ntextarea {\r\n  max-width: 300px;\r\n  height: 100px;\r\n}\r\ninput[type=text],\r\nselect {\r\n  height: 34px;\r\n}\r\nselect {\r\n  font-size: 0.875rem;\r\n}\r\ninput[type=checkbox],\r\ninput[type=radio] {\r\n  position: relative;\r\n  top: 5px;\r\n  width: 22px;\r\n  height: 22px;\r\n  margin: 0 0.5rem;\r\n  background-color: white;\r\n  border: 1px solid #e5e5e5;\r\n  outline: none;\r\n  -moz-appearance: none;\r\n  -webkit-appearance: none;\r\n  /* List some properties that might change */\r\n  transition-property: none;\r\n  transition-duration: none;\r\n}\r\ninput[type=checkbox] {\r\n  border-radius: 5px;\r\n}\r\ninput[type=checkbox]:checked {\r\n  background-color: #ef7ead;\r\n  border: none;\r\n}\r\ninput[type=checkbox]:checked:after {\r\n  display: block;\r\n  content: \"\";\r\n  height: 4px;\r\n  width: 10px;\r\n  border-bottom: 3px solid #fff;\r\n  border-left: 3px solid #fff;\r\n  transform: translate(5px, 6px) rotate(-45deg) scale(1);\r\n}\r\ninput[type=radio] {\r\n  border-radius: 50%;\r\n}\r\ninput[type=radio]:checked {\r\n  border-width: 5px;\r\n  border-color: white;\r\n  background-color: #ef7ead;\r\n}\r\nbutton {\r\n  display: block;\r\n  margin: 3em auto;\r\n  padding: 0.5rem 2rem;\r\n  font-size: 125%;\r\n  color: white;\r\n  border: none;\r\n  border-radius: 0.25rem;\r\n  background-color: #ef7ead;\r\n  outline: none;\r\n  box-shadow: 0 0.4rem 0.1rem -0.3rem rgba(0, 0, 0, 0.1);\r\n  /* We'll talk about this next */\r\n  transform: perspective(300px) scale(0.95) translateZ(0);\r\n  transform-style: preserve-3d;\r\n  /* List the properties that you're looking to transition.\r\n     Try not to use 'all' */\r\n  transition-property: none;\r\n  /* This applies to all of the above properties */\r\n  transition-duration: none;\r\n}\r\nbutton:hover {\r\n  cursor: pointer;\r\n  background-color: #ff96c8;\r\n  box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);\r\n  transform: scale(1.1) rotateX(0);\r\n}\r\nbutton:active {\r\n  background-color: #ef7ead;\r\n  transform: scale(1.05) rotateX(-10deg);\r\n}\r\n.rightBox {\r\n    border: 1px solid;\r\n    width: 100%;\r\n    height: 30vh;\r\n}\r\ntable { border: 1px solid #ccc; border-collapse: collapse; margin: 0; padding: 0; width: 100%; table-layout: fixed;}\r\ntable caption { font-size: 1.5em; margin: .5em 0 .75em;}\r\ntable tr { border: 1px solid #ddd; padding: .35em;}\r\ntable th,\r\n      table td { padding: .625em; text-align: center;}\r\ntable th { font-size: .85em; letter-spacing: .1em; text-transform: uppercase;}\r\n@media screen and (max-width: 600px) {\r\n        table { border: 0; }\r\n        table caption { font-size: 1.3em; }\r\n        table thead { border: none; clip: rect(0 0 0 0); height: 1px; margin: -1px; overflow: hidden; padding: 0; position: absolute; width: 1px;}\r\n        table tr { border-bottom: 3px solid #ddd; display: block; margin-bottom: .625em; }\r\n        table td { border-bottom: 1px solid #ddd; display: block; font-size: .8em; text-align: right;}\r\n        table td::before { content: attr(data-label); float: left; font-weight: bold; text-transform: uppercase; }\r\n        table td:last-child { border-bottom: 0; }\r\n      }\r\nbody{ margin:0; padding:0;}\r\n.play-bt{width:50px; height:50px; border-radius:50%; padding:5px; background:#000 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA9dJREFUeNrMmmtIFFEUx8dVLHsSlqVhqRioJFL4ocjIyEdi9iALAoMgCulFEESBVEQfCvtoUPRAksCgsKgkiqRAyzJ6oCGKabgklYb5SDO16X/YM7KKj507d+7OgR/rLs7d+587995z/ncDdF3XJMZiEA3CwDJ+nQGGwW/gBs2gHTSAbllfHGTxehdIABvBWpAEFoFpU1zXy6JqwHPwDLRa6UiA4IgEg61gN8jg91aCRqgMlIBKoRZIiEk2gyrdnugHt0CS2X6Z+edYUKKriU5wCoTIFpINmnX1UQHiZQk5CgZ0/4UbpFoVck53RvwEOaJCCnRnxS+QPlF/J1p+aVkt5n3CSfEVZIFaX/aRFbxBzdOcGdUgc2xWMPaO0458SUDEP4VCVoGC8VIM7zgEVgs0TvnTEfBFkZjDLGjcnT0CtAlOxHpuIwoUg78KJn+592T3HpGDIFzwDgWCEB6RPSCbE0I7I4PnyqhHayGvVLLiKUgDJ8EPm4TQzdtvaDCEpINIyV9Eq8p5sAHctUlMGtc/I0LybHwE6kAu2AHqJbc9B2yjVxISAZYrWGnugFRQyNWirKARjyEhiSxGRdB8OQ7Wg8eS2owHa1xcWwco3p1pRcsB+aDFYlu0UCW6FD1W48UQuALWgav8XiSm00JFQmL9nDu5eRlNE67XebKHOSQZfMEb3Anwzaz14JLggMiMPnABpIDroN+ML+XE+MzZwbAZIX0OE7ESlINSMMuMkDaHCKAa6Ayo4CrQtOXZ4AAR29k6PQ3mmp3o5FQGcS7kr4jlzudZXCDcJKQJDPhgPMuMEK5/jrHpbSXI/H5Dj9ZH3pRURTqvSIUSRGjsqNS4uG54pUAAnZ0UgYeU5Els9wGNirGP3LRRgFHJVfLjJHMDJp/rCU0NQ8hL8M4GEcl8xyg5jLKh/TKjlHZ5zfzLEr8gFJzVPEZflk0j3T2qz16WCp1FvBe0Zhq92skFnxTYQUWTmdhbwJBAo3UgAZQqPGqInEwIecE3BBruAR0Knfm9vrjxoZwu+KtynCqugX1jP5zoWCGZM9AFDhNRpXlczC5f65G3msf67HKQiFrOybrMFlblfGG7A0R8ADu1ydx+Hw5DU0CTH4/cHoFwWcfT0eC+YgGDfBjr01m7mR8MBIJ80KpARDXItOuXDwZLwEXw3QYBlBEcALPN9kv0RzUaJ4G7wCbNc1wnarv2sqd1G9wDPSKNWBFixEwQx047mdNLwXw2E4K9BNKB6R/QwTSwkf2aq9RhK52QIWTEtgQxmufgJY7/JjsniA2CQdDJAhrZvG7hzy3HfwEGAFdwoJHzsBGDAAAAAElFTkSuQmCC) no-repeat 5px;position: absolute;bottom:25px;right: 175px; margin-left:-25px; margin-top:-40px;z-index: 99; cursor:pointer;}\r\n.pause-bt{width:50px; height:50px; border-radius:50%; padding:5px; background:#000 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAw1JREFUeNrcmttLVEEcx2cP2pVSowtKFBQRSpoYW1QEQUVRIXSBHpIIumBPIQU+9Bd0gSKhrJ6jiOhCgT4EZQ9dKShJKboRhEWBaCm5atP3187AYZhd98yc457ZL3xwPc6657NzzplrgnPOQsoikBRUgnJQBiaDBEiBPvANfAQvwTPQCf7YfnjCUmQe2Al2gDowyeB/vAVt4IoQMwuJGFAJWkEvDy+joB1sMjmnoG8oBSfAAI82t0B1VCLrQBcfv/SDprBFjoIUz0+uiivBWqSF5z9PQEW28xzrqXURHGDxyGuwGXzV/dHL8sZTMZKg1IAboCSICAkcYfHLcnBJNLBjNohLwSMwhcU3TeBMNpFi0AFWsnhnQHSFujNdWvsdkKBMBScz1Uip6MDNZe5kA7hHL4p8B/daSAyC75pvbbZy7Df4oRybBmYafm6zFJENygTL7sddUKywS1PusqbcQcuOZh05yHtkjRhDmGYUDGswLZdr6Pwb/Df7dttxTY5tVK7lgmQLmOiJR+5a5m5oZLqERBaIX1wN1fIKEqkSteJylnmO14bMQk9MILieOSQyowBEpnuGUzhxy//HLy8AEe6JLrHrGfQ0nT0X00sinwtApIdE3hWASDeJvAG/HBd5TiI9LD1n5GqGwFP5+G13WOQF+CTHAjfBiOXASvdNqRnJsVyQXKfKkGN2uk/ui8G8SWaBVcqxGk25ck25KguJfnBNnUXZCu44dlm1gkOqCF1mD8FqRySoR1IL3qvj5b/gmEN9rxYpodaIzHnQGHMJWkBN+ts/nQhN2z9mdtNDUYamj9aL24D57ws1tBbeEOPWvlmVyCRCocX8PZZtSxQ5C07rRyTZ1w93g2Eej1ywXQzdBvryLHE8rOXpJOjMgwCtte8Le8NAGTg3jhIPQG1UWzjkDoiOCAU+gEZQFOVeFD/1oA0MhSTwChwWNR/4fGy3OVGqQT3YKHq8JTm+LyWG2bT4elv8TJmeRBgi/lSIbvliMJ+mMll6CY7aK9pc9hN8EX2kLpbegBZKW/VPgAEAGYXv2/tP1L0AAAAASUVORK5CYII=) no-repeat 5px;position: absolute;bottom:25px;right: 175px; margin-left:-25px; margin-top:-40px;z-index: 99; cursor:pointer;}\r\n.stop-bt{width:50px; height:50px; border-radius:50%; padding:5px; background:#000 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAxxJREFUeNrcmktoFDEYxzPDamuxKopdaFVQEV+giFVaFbRWRER8IbRFKipV0KMHbx68eVVQRPSyF8WLFPGBVMRnS2tPolgoimgVWl91i7Dt1vj/2AyM407nsUk32T/8LrvzyH+SfEm+xOKcM0maBhaADWAxqAFzQSWwQQZ8B5/AEOgEfeCLjJcnJNxfD5rAZrAElEW4/yd4Ae6B2+BD7JJQjcSgAhwFnVyevoGLYGWcMsUxsR/0cnVKC0MLVRmpASk+efoIWmUbaQD9vDi6JppywUboq4zw4qoDJAsxchyMcT3UBar9ympNMI60ghTTS8/ALjDs/cPPyEYR2yuZfroFmsGo+0c7z4U0Gl/V1ARpHzgVpkaugGNMb42ARtDtZ4T+vAumBjyIqpUrLOgUn9bi1iOwHWS9Rqjwj0FdwAPGxQP6FJpoB6tCXNsCbngnjbtDmHBEM9YBhTUyFvK606LzZ2xXp2+L+NVUiVqGFfLaNWCbO2otB1uZmTrsNrJX8VdWqQaQtEU/aWTmag61JjJSBdYys1VLRhaB6YYbqScj60IMPrqrigzMZOZrNhmZVQJGKsjIjBIwkrBZiYiM/CoBH1k737LRQP22RT7WdP0gIz3gj+FGPpORdyBtuJEnZIRS/C8NN/LKFkvXhwab+Erld8aR9gjLS91ElTDkGHkLOgw1knKvEClqXY5wc0ZhwUYjRNEepwLcWRRKkT5nuXRp0GxghXhhQrIJLpIPYddH50Q5/kvQ0T7gAxacoBtXOPZYIT8Q9Y0dLE+CztElcELzfpEWSYdedzPx6gx4o7mRs24TfjVCqhN9RsdF101w0GlSE9UIqQucZJ49CA1Eies2rwkWkHS4znLbC7qYoYja5DcvDFoh0mBzRIPF132wR8wLfQJ3uO3pTeB1kTZB6fBAmcwDA0mx552dJAPvQYvKIxw7QbdCA8PgPJiv+iwKUQ4OgacS9+EHwQWwLE6ZLAnntWrBAbAFrAblEeZVgyLU05h1h+XOcsWb10g8eEbzs3lgPVjKcun+avbvwTPK2AyI6EPhtF+YKVh/BRgAE4h+1tvQXHoAAAAASUVORK5CYII=) no-repeat 5px;position: absolute;bottom:25px;right: 100px; margin-left:-25px; margin-top:-40px;z-index: 99; cursor:pointer;}\r\n.mute-bt{width:50px; height:50px; border-radius:50%; padding:5px; background:#000 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA61JREFUeNrsmUlIVVEYxzM1S0sbJMvMMBQ0XKgN0IANBoVWFAVBrlpYJLqwVbWLaGVJRkW0MRpWNkohRFSYoEHDIqkoUlAbzAw1UXLg9j9wDv378j29vXevr7gf/PA70/X835nu+W6YZVmT/gcL84R4QjwhnhBPyCi2G2SDC6DZZtuNIBbcBiM+aykhDlNm/bJ7INxG221gWLc95q+ukwImg1PW79YJ4mw8o1K03+K2kOngpvWnfbIpJAd8p/atYI5bQhaCRmt0G4+QKJEuEs8474aQbNBi+baxhOwBzeCOqFdDzxgEWU4KKQDdln/zJ0RtAm1U9yKVpYkpds0pIQfAiDW2+ROijoJbov4mKq+g/B9aXFCFHLfGb2NNrSTQRfWfgkhdlgL6qawiWELUorxs2TMpZIleV/zcYtFmF5Xx/2vhjYFP9ihQDBaN88RdBlbbPKU/g3TQA7aC6yASlIIzus4U0ATSdPoByNP+ZlBLz1sL6uTJfthy3nhEzlF+D0igvhykMrX2Mul8+kBl5abNZFKX7fJ7XjX56l2qhNJX9KgpU33M136fHiFjK8EMU8nYsMtCHoIaSu8F0dr/AhqobB35nK+mX4oUMhFWSf4CsJzSdeRnmV9erx9j8SA1FIQ0go+UziX/CfnzQLL228EQTbvkUBDSD15QmkdE3VsGzb1Ji1H2DfRSvYRQEKLsFflzye/TGJup/w7QRmA2ipAQwp2aRv4QTSFzzplNaZDyI0NFiOy8sXCN3FVVnyMofyRUhKSR30W+2opjKG3WxVTawcwUnHAh6pfNofRL8pPEaHXQWomj/M5QEJIJFlP6kXiXM9YNWrU/X4+KsbZQELKP+tAjTu01YqTMtEsXApulkAiXRWTo1xJjN6iz0eJwrCd/Bfkt4L0U0uCykEKaImrnKaey7XoKGaulrTaP8p+Br3IUTuuhGs99RJ20BWBpAEKek6/uIq8pXSKmVaP2c8TUuhuMSGOsiG78zQ1xg74BhlFevmhTSmUnfAX7Ar2vR4CzQbyzR4M3VL+N6s8GHVR21YkoyqEgCTkp6hf5iCErW+VUXEsF1wYCFPKO6j7WI67yZ4F2Kqt3OtKYK4bfrpD9oFdPr1TKl8HwAjdiv+mgKYDYbyKIofR6METPqHUzGh8P7gchGp8oYsl9IMPt7yMqeFYV4PeRI6J92UR86DEcFYvUzherHdS22l9dt74hFuo33Srw1mbbnSARXBK3Se+rrifEE+IJ+YftpwADAJs4uzLWopW5AAAAAElFTkSuQmCC) no-repeat 5px;position: absolute; bottom:25px;right: 25px; margin-left:-25px; margin-top:-25px;z-index: 99; cursor:pointer;}\r\n.mute-bt.stop{background:#000 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAqpJREFUeNrsmU1IVUEUx59SZoiKiBV9WGAaqYuKsKKCQltEtI5cJEIf60IMIYPapAuh0CiKFtLmSQTRuhSEPqAWrWoXhKJPjT4oAym9/QfOxcPhznv39t5478Qc+PG4M2fuvD9z5szHLfI8L/U/WJET4oQ4IU6IExJg7WAvuA8+GOlBCTHMdW/Z3oLVJvoxOSIl4B7oYGVzoB58tyW01lMYnRTl02CnLUKawGOwI6DOGiGHQBps0tRbIeQ0hVNZFh9jQgqVNXq8cDYFKk1krXxfoEb0jhfepJCDoBc0Rez3LLgEKgohpAqkvWjGhdSBn1Seoecw/fax9930y1eJSOsDR9TcCRGV68C2PKK6gs0nla6fgFYwm6XNZcK3jbo5Ytr4iKiwvCXqx0CpZiROgEXm+xXs0YXWSgpRlIBR4XM7QMRu8IX5/CZhqaQIUVTTHozbVVa/FXwS9edzZa04hPh/dlr4ttOIvRDl/WHSb1xCFK1ggfl+pjnDLR12HYlTSIpG4Y+m7WtQbosQRVeAmHdgQ7Z2xQk8tb5Xq4IomwKZKHutuEekGcxo2t6NsteKU8gW8JH5qvCaFe27ky5kDXgpfG+ABtqH+bYETiVZyAPh95RdUrTRSu7bPNiXRCEDwmcclAmfDuEzIXfLcQvpFPWToFYzD64EpOQanZA3NMkW6Tcb+QrZDn6xuh/gQI41ZkS8b1B3HjkMakFxQC6X1gIGQeU/rhflYK24jXyVo00nqALH2JmoIGf2/QGbvSjnkYvgOTgT8WT6kBJCYyFvGhvBCGgO4Zv466AaMAyO2y7Ev+sdAudsvtfiXLPxXkvHBXbVsyJCTH5WOAoegWpWlqHQ+mbbF6tdlNEa6HkG1IF5G79YbQbPaH/Ubaof9zHUCXFCnBAnJBb7K8AA5jNpuFcQhagAAAAASUVORK5CYII=) no-repeat 5px;}\r\n.video{width: 100%; height: 100%; position:relative; background:url(data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAZAAA/+4ADkFkb2JlAGTAAAAAAf/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgCigQAAwERAAIRAQMRAf/EAL4AAAICAwEBAQEAAAAAAAAAAAYHBAUDCAkCAQoAAQACAwEBAQEAAAAAAAAAAAADBAABAgUGBwgQAAECBQIEAwQIBAMFBgQCCwECAwARIQQFMRJBUWEGcSITgTIUB/CRobHB0UII4VIjFfFicrIzJDQWgqLCQ3M1klM2CdJjsyUX4oN0k0R1JjcRAAICAQQCAQMDAwQBBAIDAQABEQIDITESBEEFE1FhInEyBoGRFKGxQiNS8MHRFTMW4XIkB//aAAwDAQACEQMRAD8A/OfbOhCwCZfxjyfGT5IFNklTxAAmJaj6/wAIohbJPnCACVAgUlzHWKIFlpj3XGVFSFJBQSCeRiItbkO1bFtcCav/ADDr4mJsHWwQ31wl62UhJBJAFPAxXIsAVf0XZKoeR+nSN1aJ5M3qFVQJwaUzFz+9bZPcJeMWB1JdjcjcrSC0BXCBklxCimol/CGqAzwFgLAjF3BCpyakpKiVAGolCmS6DVPmG2l4K3CX+P5xz8l1yNkjNkluQEwOPtjSuoILa9I9VHGShP65/jGuaIOHsHIttMqQpSQTSvHzCkXyTIHOcfS7bOEEEFOvs+qKsaqLe2U36gSVicBGahOq9atbdO1aVKBlLjwgdg0oJ8EWbVD12pxILzK6GfFBA6QKH/QnJCtyCRdXt4qcgHFEHgZAQWqgnJA8t5TDm1I3TMiYqyFsrkZfa53sesqhBFPb0gYs2kMrBOG/yjdoB7/EeIHjxg1ANtWPe3wi8bbJuFBW3+Y6UH8Y6GIJXYz3AJabUkbpkaeIhyu0A7LUs7cLt0tqCSdwB8I2tzDRUdz2yrxCVgElCRp/plB1sCS1BKyt1nHPskEKUZAcTVUWXdfiAuTtLi3QpHpLIHGX8ekYs9BJ7i0yynlpLQaUSDwlP74WuTUiWOJfdKVLbWgTBrACPcP8fbrbSlMjICU4nggTIbIRXWWnWICuEeCxqrsiU5zoB4wagJFtc4a8t8hJLDikCc1Spr49YboGoWLWP2LDi1bDSYM+FeUM121B23GBkLlvKYVnH7kp9FtUjzpPhWJbYy9jWjOYG5bubj0mluBSzUVEiOsJW/cB8gvb4K9SpRLDgqToPzjVv2/cKtiYMU8haVKQsS4fVHOzeTdS0YxjrjgXtVLn7YQe4dIYuGa+FSgnUS16QOxYcW142EjzDTT2e2OZ2lJupTZoovUgIUCUcB4xxb1YYTfc7TjB2hBMx+H3wKIGsIJYRLwvG1qbUAFGshz416R6D17ShjtS77ku0tvLdpuG6SefGPWdZ/j/AECMhYu2cuAi6UkpSrjw5/jAc37gFidmcii2tA2khSpKG3jM04+MXjakpA72XjLzJ5R51xhxtCXtwUQACkBJnQmOxgtoHrsbdYWxt0WiXHLhKFMNJkCT+mQ5Q7ka+MsTvfzxyHcFtbsjelQKdydKBI8Y8539asgYYwDB4UoWZFO3XWiTzjidRf8Ac/1C4twQ7hyYyFh6bciSTQdZCGfcOcX9B1bDo+Tj1parleXCLcLaKQVkgTUgp4AnUx8s9jRtuAGUNu8sFaNJN2zdIdD4K/KVEanmByjndWjQhcUtvYF8PlvzhBUCRwkqPSdXRAjNatuJPwYQT6hIn7Y6CZNi0tMC9Z5BD+xUk8eGoMX5IPrD48qx7dzXceFeQPhxhnGm2QxXF96aw3ITSoffKOxgTgp7DDwF9K3XMCravuh6iA+Red12q7twlCSak08DDeNOQy2+wGWmLeQhW5tYMzL64Nm/ZobruCGcsHRcq8iuP3xyP+TYdbAldNPNJMm1GXhF22MWUk3CILrxDnk01/hOAsUy1YfIsEFKClYNBzg2K6L66ZbWuNCpDdBrWkLlowssO0EXQD6nSkJ/1fgI52elm9AKxWa0PeUw7do2sIdCpciTp4wt8VjkdrDeT+7aUg3SWioBVPtnBKpoXpR1cFte4ub63AZhR68JwerR1sC0BvN2fpsa12Gf2xs6+DcRmcJauBIT85/GLOxj/aW+CyrdojctSU7TMz4VirJs1bYJ7nvCzU2Ues1M8I116/mCW4o+4Mk1duubFJM9Je2Ohm/bJu2x8wl47j1+slsqBlyPDqY4HbU6F4hnYzut5YKQxUDkmenjCWKrD3K3P3zuTaUVNFG1MtB4cDD1U0K3FSpv0lqB/mP3w9iEMpZWjg3AR2MLXH+gAN7ApKEifWOd2P3AL7lDlH0NOKBIAnL6zC6MM+Wlot9JcbSVSG6nKGsW4G25lYv1276GSiRUvbWX5x18T0MhUtai40ZU2pOnMCGk1BAxtblJximgQVkjy8TQxm1kQD8jZOuBSlIUlJ48IDyJ4FxfY4tOrXWR+nKB3soCUKq1yPwTqkoG8qO0ilJgA6yhW9i7lx8asqQ6EaeaVIHIIOMUTl7FwLHpmW0DiZGXCcdHrkIqMT8G6EzJA8efWUdCrAXJ4oqXKX4xvkjn5dz3aEeufZ9kTkhO24VocDjKkgiaUK+wTjS1ZmJKW0sTePOIE6rIp4w1jNJayUfdfbK7QhfmI2BXHimcHyWXAvI1x+4IYhHpOpCqDSZ4dDHE7TTRzbV1CG6bbfmlKweUj0pHnOz5D4FqXdrjCm2Cq+6efAThCurOnxYqu5L4NOrbMhJW2vWkaumbooA5Fwht5C5gzUDAYgKGTF6xd7Gy4kKISJcTIDSKKIWYaFm2pSTuABM/ERT2D1YAPOXF1P02VKCuKQJH7escvsbmye24+2ylC2VJkDqP4xzLfuIiI+gKQ4omRCVGXgIax7B67AQxlHU3oQGiZOlIMhwnzMN1NrcKslcXD/poLCgC2msh/KOsWHWwWduY5sY/etwJIIoZ8ZnhPSDYdyywQ823denuG0fq9p9sP3/aYsVl46n1VlBCp8oXqmgPkhs2wdKys7JgynT6VgPYX0IUOTxiSSd0/r/jHMtVkBhZVZvJSBuBNT9vGEsrVdyBLbbbtmZUEky+72xrHdMrQMbXuVGAxqUJ2rWj9JlMyA5y5R18F0XIRYTuzctN2ECbkpJp1lxlxh95a/G19iDKx9k/3OlXrtqt20pKgsiQIA3T8s44GVzm0ICPcqbLtu2uGEXSHVvoUnbMkgmsqgco6PWqyCQxdq7fPOyQqS3nCCOq1ER3uuo1ZGNTF9puotw44FoSJTJnyJ4Q1nc0SRi2xd2VmEXItwZpEvNX+EcmtGrf1As5gu45aVlUiJdTHpOB0gsw6iwlII0GpE+HWA2qQ9Jvmm70FSwJuonOX8wBnGNUQdbN3YqxiFIcSVehMyA1lFF13F+66w+6qTlQtUpeJ5RmzQdGVtfpkBRp7SJacfCAuxZTZZIUVOtmajOgpxi63RD7jGVOhPqCU/xhmljFi1ucQl1A2z50nB0wJ/WeIAUQZznLjrB8dWwdwrtsY4xauySTNM6+MNVowQJXT67VwlYlInWF8yZANyWTXdXRQgzSZ6eMcvM2g1C1xTzjZBIlpWcc67s2bLy9uWnGJKUN0jTWsjxjVXYgu1tKuLopSCQXJRtciDO7fwy2EpdG4AAKNSB1++GKJkDt8tuWq21qkSAOtAYM6uDVQD/t6/jB6QJTwMzzEBdWHT0M7tksK/qlSQDxJlSB3roR2gn3mTcZtG2UGg8swZUJA8YzWpXMolOEVTUu+97aawRqCcwhwXaqcqr1HAaKnxpWUVAG9gzvcWzgbRaWjUAGR8J8TGHSdhezLP5c3y3c1avrH8utf1A8oJSrRhG3eWv2rrCNsI2+oCqgABqlMqgdIex/QNXYHrBXq7WlfpI/CG6ltoNjjmhbeo6dsm9yaUPH74JVagW1IIOu+opxuQKZlNQNJy4wZaIxKKj022btDQltUa08OHtiuSM31RW90WaRburaQlRE5AJHWM2coTtXUThxTz1wXHGaHpTWF7mS0+DuEoCEMCSeMgPrpOAkJdsyQoAprMA04xRAjRjvWSCBUCIBuGnats8xdso9PyFVSRpUeMGoZSk2Dbwlhdsb3NgcPDaJ1HOGqBaqELLuHAlp5z0Ukp4SpxMMVZi1QdUwqyQhQ3TUQkgk6GQi7WUA3sW7OHtLpr1CElRSVKBSJgwm/wBwHyYG+37JZUjamZJHuD6tI1b9oVbH8/8AL4XDanGmiacEkeEc7MbruBN9g7zFOlg28kieo5eyEHuHWxDXvbQCRI8qdIHYshqfukAqQlRn1+6Of2FJuh6x79wXFhafeMjMz4DnHLtQMZ8lhbXIbVvK2kcJdJcPGFbVGsUFA7hLOxSpTZExUUl1jr9JxA9QEhgv75l0MrCvRXOZE+Ylp4x6jrXipvwH2b7dt8H2+kWnneQV0kZ0SnnOMZcikBYSlvauZO/LVzuSkLTxPOekVS65FIfuGxbOIs21sISStrzHaAeI1lHXwX0DV2It3nVtFbTaiN5KSAZfSsO2yfhBoxY3CXN++nKFpSwyZlRmRXxnyjidt8kQm9xqcumHGUCSjPyinAjhLjHJwLhkb+4XHuBVhji2sJuNyU01JI+2J7O6tj/oOrYOG2yy02pta0BBCgUKKZ7SDqCOUeC7OB3bgBmLa570vblhuyVMpaT6YmZmUzU/XCePqum4hcK+zFWyra8+IWErcCikGsyVg8Ydx/gCgKbPBpW6LpCZlBJFKaw7jtJBj2+GRd40vuoCXKUCa1BOsFRC3xd01btJsnFBKE8TLjIfhD+CsshTv4pV1eKWwCtJWCCJylOO3gpoU9g8s7FOPtgXyWypuk51nMcfCHa1AveDDYWLF86veQRuMqTmJ68oZogy2La67WbSJtIBEpmnMRM37DdRa5ztoG4VNEteEcj/AJMOtQWe7QbcHu69D+Maak0qSVp7LdaVvYaUSeUx9KQtdNAcuJhFie3Lgki4bUgJ90mfIRMcvYJ1uu34Cq1wbSFAEmYNBWG6Usxy3VswnaT8K0WECZUKQevVdw+LpWa2BLNBxJUFgg11irdRpHL7fThvQXmPyNzZZpThTJobfNOnvGOdlx8WcPJi42D9zuS39KZcTukeWsoFsxnEoAu/z6rsrQZbRMDSCKyOpg3QAZO0buVFRNZk/fG62TZ2Mf7ShdxK1tqbb3zVQSJH3QVxBdgXuu08ogl5Lb2waneuX3xeBrloDruDLrLrFwW3NwUNZmcOZn+P9DdtgvsrgFlCKTFNPxjh9jUvEF+ERJZmPeP3gD6hC+Koe4SZC1Slkk0mifLWGlX6C1xU5hlDZWtPCfSG8SOfmKKxvBvSN1fGOjjtoACq2ybzagAKCUJZ3qBsVmWFxeAKbSSrcCZf6geHSF1ZSDewRYPKrx7Sm30hO5BT5gDqJcRDWNgLbmB0Jubtt5EpBzfTTjy8Y6OO8IoYFpbKuUJUBPakCnQS4Qwr6ELfFWe7KMsuTDZnOpl7yRppxirWIwr7xsrWywq3bchTg30lL9IgLuQSeVaK7FLm0b1AzAA5QO9zdRSptrtF08pTZ2lc0znpIQtaxqxetPvJR5kyAH5RKAti1xHcF8xct29s0FpWqun5R1Ou0iDUKX37Q3Vw36axKniDPQdIa5ALgs9ehKiAoT5fSkXzOfl3M9g4VulXPr0jSbE7bhPY+vNY20UCPrEoYxlLcIcHbm3uUlQlucmZ11h1WSqEnQIO8bJu5tiUgE+kmoA12DlC2XLpAK5r3e2S7dKvTSdwJ/GOZntyQtEso2Mk/av/ANQEJA1J/OOJ2KjWCmoy2M4ybBPnTuKVAiQ/ll+MJYq6nR46CW7lt0377i2zMhe4geMGvQsCHmXGkq3ggpHlqeELXqQprfJXlvcJdSFf01H9VDWXGkD4kGNb3TmdsFKeElKkPrEZstA1dj4y/jsSgNuOpC06hQBNPGOV2DZEvL1h5CnGikgikgAOmgpHNspsRC3v8tctuuNoTNJO3hoZCcNY1oHrsRscxuuG3nBLzhRpp1hlbG1uHGRyFqltGxSCUtpGg/lAMXIdbEOxzF48kMW6NyFGkqaQxg3LCEWb6WBcvpUg8ankD0joxNQdnJ7asUr/AKkyZifGB8QRRXl8606Gm00CwnhpP64Xy0bIW4sl3NuXFJMwicKvGT/YELqwS4shVFAkAeFOko4PdpadCHi2adtnUtgHbPn1H5xOviuyF9cdtovbX4hxSgFa1VLTkDKOnStq7kLXtrHWwuU29w4UMtlJCq1+3pGbZmvxJI673vNnA4xtjGlt1ZZLaqJCvNNOsjMgGAVTtkTIIPKLve5L31rsLQkO7hJRlKvAS5x3etjcIr7BfhrKzxWxZUny1MxOp11js0rFSwsu+82vhlWjJbO6UpJSDQS4DrGmnGpm2xR2mVfS6HCmlK0gNqoB5OeTmQYcd9NKp/SUd2UdIK2LRkWSX0+8Z/cIDaGQA/hLm7yLiAmaUuJl9YPKA2qyDFcL2PsEg0mzLUwO2xa3A6xvHlPLUeDivtMAb1CqyLq4yhCgCqsoHbY39z43dJdoo0gddybFmb5q3ZSW1eafhSXiYao/BixbY3IPvnaaiVKnlDVE2BL9hi7W80UJmCtM9dI6fXrtIK43cdiUv2u1xPmWhIlLnLnDeiBi5717VDDLziG5SBOkoUywybiVx2JUu7QFJ+zqI5ebE3sGqGz+I+HtwtCanp4fdCVsDk2VpxrjqTNPA/dOKWByQj4vEn4halI9xc/qkYLXE0QPG8gLRsNEgUCZeEMVoQo77KuJX5VeQ61gzpoWmSLHIj0w4lXn+hgTxhVclNJv8i+UqTNoyI1rMnpA70M2sYshiXkIG5FE1+qp+6MVoB5lCl+3DiUbqoUArSkou1Ccw6xvcrWLb2MuSUoDiBWh/CMKjYK9yc/lV5llS7gzEuc6QauIHyQQ9mKsrO7ZCFSUJcBwI69IIsTLQ6z3Aw2AC5IdT/GC1ow9dgwwbjDsn905jcDTgJw1WsA7bl5e5Z+4QGWjuSgbTM8PZ4wSUkYe0gPkMoMe8lLitu8zPtE+lDGXkW3kD5PDV0i9eQ+yrcBx+hgXPUI9gpRaIvGJPjynXjE5C1/9QMyqMbZLU2DLb0SPxgdrICUqMlYKIQ0uajJJFONKSJrAuRAjscLbXG1xKZqXU05/xiuSZAma7ffaKA23Qy4cPqMTkDsg1xeCuW0B5LdUSPH8ukGpY1Wpaf3HJMXIZkPrPMDlDVbIJxJGUeX8EHnffO6f1A6xvmZaFPlckklSSqW0xh5dAFqyf2DzilOKaUryk7deEhAndSA4uRs4bDfHbX2U7gmSlGXP6+JiWzV4wGVdBj2Nv8OgIcEkgV9kIZciZqtYKXuLt7H5Bhx9KdzpnLyg6gnWcJO2oUR2T7cKHFNluQHTr/CMWtJG4K4YB7aQluYkfppCmVSapYgf2O5Y9ZfpyCQVcRoIRvQOnIv8pmnmLpDBMvPtIn0P5QtbHIzicGZ1wXDJUqtB9PthzBV1HqWR/YRxFvdtlJqJffHf69vx/oa5KAn7ivGHMeStX9Q7pjhoJQvmu+QKzFbY2LYvS8RRSgZy6xvFaWZ5ajUVdJasAmcpMkJ06/VHWxXioZW0Ff6N9d329pG5tLxJMzpUcoLbP+MGuQ9u1rhTFgbBVC6Egjnz++EcuRMnIrs9ZMWbq1UCxOVPpxhGzSchcdlIG3CUvI8vvGcI9qzuh1WUFwzaXRt2kqT5FSSNeJl+MciuGXqBytMuEdnqS18SpqW5JWDL6cYFmwpbIRv/AKEjD2b4cIbTRtRB1pI/xjmZatAhz4C6Q00GrgyJkJfSUGwNrcg18Ulm5twyirapfdDtVLIC2Uxqmr5xLafKNPrP4R1evQy7LYMe0mEF4h8USkypyBju4aaFOygz9zrXc+m0zUNEDwkSeHjDOyB7sg4UuMOoBpNVYysyTgYrXQbaHGfgVKJ8wSJfVFZsydIN1qwAvmEXDhWoTJ/H+MctWm39RmlQMukvpuVNMig04cZcoPKGa1+pZ4x9KVlFyQNo8ZfXAMmpq1Uy+ZVaXCtqTMzloOPtjWCo31aVW5nuMQoJ9RtGgCvZHWxYuR1Fiq/AP3FvfJX6qUiSPHh7I7fW66jVD2LHWq2Ku5t3L1W64FTOfHWNZusocI5Pd66vMIG73t5gzUE18B+c48v3MUWj7nlO103zBW8xLLEysEAT+n1Qh8FnqZp1LAPf3tjaKI3yM5AU8OcV8NkPYcDT1K0XYuKtHcB93siVpZM6daxU9tXbzTyeAB6/lxg1tKmbPwF7matzi1trX/VMpCnJU+MCwuLg09RUf9P3eayyiy3vSuUqHiT0h7NblXT6G25MuT7evMO2Fra2cOPD2COZbC7MJirB7w2QeQs+qZJTXXhKKr12gti2yvczbjXptOAqCduo1FOfOCrHx3FrgSU3eRSttKdylkyEzWvhDGNCORSY2u17yzT8S+1tSnUyNJ14iGUoUAHVwYTk7Jh0trXIjw/OF8lGwNqsnM9wYtr3nZf/AA/nC/xvloCZhuMlY3qh6bkwCDw/Aw5jxuBe25dYt623JG6kxPT85w1SrKGri720ZYKUr8xAIFPzg6WhCKrKuM5FJbMpTlWXERi/0KklZfL3F5aqafP9MznUnhXWAFcwUcaXcN+mBNA04wO7N0sUd3ZstpJWJHhQawFsu9gUuQ4pRbaruO0D/CLrdIFyGD2T2236Cr2+RtU0rcDKdCo85cIfw5EXyRfdwZdCVKtrZU2jPppTQUhlZJAXsLsbl3J3+5z9vCgi1fURyOQ/w2NLmxSRMEj+MNU1E7b6DcxWBt1NAlPmCZmnEQ3RaSZImQthZLCkiW0zGkbtb8SFRc5RVwytC1cNorPSn4QlkbMOwuciGvVKSfMZ8BzhW+piqlg9ke3jcWpfQ3Oc6y6QjnpK0OhgrDFhkMrd45SrcHaEcJnwhTHjhj1tgU/6kfbe866LUBrzpBb0BFtdOJvLYupqQjcfH2eML2xyQHsayi5WpDlZrUNOSiIH8REEr1z/AGZktW5lL2aA1gOTE4YagIPW1zmHy88nchepBJ1P2xx+xRybCy3xjbVqhEtARp05dISeOWRaMqXsTZOKWZTUK6D84JWsB67ApmPibAStxIEU1EG2RupUYlN9kXQm4TNBUQdTSctCIEnqHQ5sBi7GyKAqhodB+cOYHqUwqyxZetFMtGZM/wAo6lXKBNMijE3jNml7ZJCgQDX8oKsclQCjmEu33luJbntVu+qs9IxfEXxJa7x6yCbZzy+oAiU9ekK3xfQnFmJzHNbS+4JH3h7axyux1XdlNRoVlxaOraVcMJmE0npwPH2Q11ek+OxRORkLhOLDKtRwn/lEEy9VpMgHqzjjD6khUlChrHCzYbLJvoQzqzaJBT65T0r/ABh3r4iF1jMobqYaM5R38CSRC3eZvn5NoTPeOZ/KOhWygm5NY7b+FtlXt2jYpHGXOZPLlEbRi2xXoyCnn/hbUz5CfMyGkBakCaQN4QJb+JCa85e2OpJ1eJjezwt0C03kFJ0nzkPwiGXpuFGEtUOA3UqrQpU/+yYyyiHk7i6uXAyFTQlW2XSAXJGsGW1xaWmySnzKE/aamFnuEqiuvLOSiSNOn0pGLBNiRY2LjwG0fZA1ozPLwQsihy3UUK1B/hDONyZs5DbtdCVlveJzkPwjoY6wDNmOz+17e9aU840FBCd4oOAn9sdDE4MWUmbuG7ZxNw0zbnZok15J/MRu9wfABe4cj8VauIcVMqTT6oVvcviLbGWDarlKgnl98D5KAiUBZcW7XpBDiaeH0…);}\r\n.tv_video{height: 30vh;width: 100%; -o-object-fit: cover; object-fit: cover;}\r\n.ib {\r\n    display: inline-block!important;\r\n\r\n}\r\n.w60{\r\n    width: 60%!important;\r\n}\r\n.w40{\r\n    width: 40%!important;\r\n}\r\n.no-margin {\r\n    margin: auto!important;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNhbWVyYXMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxtRUFBbUU7QUFDbkU7RUFDRSxzQkFBc0I7QUFDeEI7QUFFQTtFQUNFLFlBQVk7QUFDZDtBQUVBO0VBQ0UsWUFBWTtFQUNaLDhCQUE4QjtFQUM5QixnQkFBZ0I7RUFDaEIseUJBQXlCO0FBQzNCO0FBRUEsdUJBQXVCO0FBQ3ZCOztFQUVFLHdCQUF3QjtFQUN4QixNQUFNO0VBQ04sZUFBZTtFQUNmLFlBQVk7RUFDWixpQ0FBaUM7RUFDakMsY0FBYztFQUNkLFVBQVU7QUFDWjtBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixZQUFZO0FBQ2Q7QUFFQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUVBOztFQUVFLHFCQUFxQjtFQUNyQixxQkFBcUI7QUFDdkI7QUFFQTs7O0VBR0UsY0FBYztFQUNkLGVBQWU7RUFDZixXQUFXO0VBQ1gsdUJBQXVCO0VBQ3ZCLHNCQUFzQjtFQUN0Qix5QkFBeUI7RUFDekIsYUFBYTtFQUNiLDJDQUEyQztFQUMzQyx5QkFBeUI7RUFDekIseUJBQXlCO0FBQzNCO0FBQ0E7OztFQUdFLHFCQUFxQjtBQUN2QjtBQUVBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGFBQWE7QUFDZjtBQUVBOztFQUVFLFlBQVk7QUFDZDtBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCO0FBRUE7O0VBRUUsa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixXQUFXO0VBQ1gsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQix1QkFBdUI7RUFDdkIseUJBQXlCO0VBQ3pCLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsd0JBQXdCO0VBQ3hCLDJDQUEyQztFQUMzQyx5QkFBeUI7RUFDekIseUJBQXlCO0FBQzNCO0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLHlCQUF5QjtFQUN6QixZQUFZO0FBQ2Q7QUFDQTtFQUNFLGNBQWM7RUFDZCxXQUFXO0VBQ1gsV0FBVztFQUNYLFdBQVc7RUFDWCw2QkFBNkI7RUFDN0IsMkJBQTJCO0VBQzNCLHNEQUFzRDtBQUN4RDtBQUVBO0VBQ0Usa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLHlCQUF5QjtBQUMzQjtBQUVBO0VBQ0UsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUNmLFlBQVk7RUFDWixZQUFZO0VBQ1osc0JBQXNCO0VBQ3RCLHlCQUF5QjtFQUN6QixhQUFhO0VBQ2Isc0RBQXNEO0VBQ3RELCtCQUErQjtFQUMvQix1REFBdUQ7RUFDdkQsNEJBQTRCO0VBQzVCOzJCQUN5QjtFQUN6Qix5QkFBeUI7RUFDekIsZ0RBQWdEO0VBQ2hELHlCQUF5QjtBQUMzQjtBQUNBO0VBQ0UsZUFBZTtFQUNmLHlCQUF5QjtFQUN6QixvQ0FBb0M7RUFDcEMsZ0NBQWdDO0FBQ2xDO0FBQ0E7RUFDRSx5QkFBeUI7RUFDekIsc0NBQXNDO0FBQ3hDO0FBR0E7SUFDSSxpQkFBaUI7SUFDakIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFHQSxRQUFRLHNCQUFzQixFQUFFLHlCQUF5QixFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixDQUFDO0FBQzdHLGdCQUFnQixnQkFBZ0IsRUFBRSxvQkFBb0IsQ0FBQztBQUN2RCxXQUFXLHNCQUFzQixFQUFFLGNBQWMsQ0FBQztBQUNsRDtpQkFDVyxlQUFlLEVBQUUsa0JBQWtCLENBQUM7QUFDL0MsV0FBVyxnQkFBZ0IsRUFBRSxvQkFBb0IsRUFBRSx5QkFBeUIsQ0FBQztBQUM3RTtRQUNFLFFBQVEsU0FBUyxFQUFFO1FBQ25CLGdCQUFnQixnQkFBZ0IsRUFBRTtRQUNsQyxjQUFjLFlBQVksRUFBRSxtQkFBbUIsRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUFFLGdCQUFnQixFQUFFLFVBQVUsRUFBRSxrQkFBa0IsRUFBRSxVQUFVLENBQUM7UUFDekksV0FBVyw2QkFBNkIsRUFBRSxjQUFjLEVBQUUscUJBQXFCLEVBQUU7UUFDakYsV0FBVyw2QkFBNkIsRUFBRSxjQUFjLEVBQUUsZUFBZSxFQUFFLGlCQUFpQixDQUFDO1FBQzdGLG1CQUFtQix5QkFBeUIsRUFBRSxXQUFXLEVBQUUsaUJBQWlCLEVBQUUseUJBQXlCLEVBQUU7UUFDekcsc0JBQXNCLGdCQUFnQixFQUFFO01BQzFDO0FBSUEsTUFBTSxRQUFRLEVBQUUsU0FBUyxDQUFDO0FBQ2hDLFNBQVMsVUFBVSxFQUFFLFdBQVcsRUFBRSxpQkFBaUIsRUFBRSxXQUFXLEVBQUUscTlDQUFxOUMsQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLGlCQUFpQixFQUFFLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxjQUFjLENBQUM7QUFFcm9ELFVBQVUsVUFBVSxFQUFFLFdBQVcsRUFBRSxpQkFBaUIsRUFBRSxXQUFXLEVBQUUseXNDQUF5c0MsQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLGlCQUFpQixFQUFFLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxjQUFjLENBQUM7QUFHMTNDLFNBQVMsVUFBVSxFQUFFLFdBQVcsRUFBRSxpQkFBaUIsRUFBRSxXQUFXLEVBQUUsNnRDQUE2dEMsQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLGlCQUFpQixFQUFFLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxjQUFjLENBQUM7QUFFNzRDLFNBQVMsVUFBVSxFQUFFLFdBQVcsRUFBRSxpQkFBaUIsRUFBRSxXQUFXLEVBQUUsNjVDQUE2NUMsQ0FBQyxrQkFBa0IsRUFBRSxXQUFXLENBQUMsV0FBVyxFQUFFLGlCQUFpQixFQUFFLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxjQUFjLENBQUM7QUFFN2tELGNBQWMscWtDQUFxa0MsQ0FBQztBQUNwbEMsT0FBTyxXQUFXLEVBQUUsWUFBWSxFQUFFLGlCQUFpQixFQUFFLGl5VEFBaXlULENBQUM7QUFDdjFULFVBQVUsWUFBWSxDQUFDLFdBQVcsRUFBRSxvQkFBaUIsRUFBakIsaUJBQWlCLENBQUM7QUFFdEQ7SUFDSSwrQkFBK0I7O0FBRW5DO0FBQ0E7SUFDSSxvQkFBb0I7QUFDeEI7QUFDQTtJQUNJLG9CQUFvQjtBQUN4QjtBQUNBO0lBQ0ksc0JBQXNCO0FBQzFCIiwiZmlsZSI6ImNhbWVyYXMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgdXJsKGh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1Sb2JvdG86NDAwLDMwMCk7XHJcbioge1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbn1cclxuXHJcbmh0bWwge1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG5cclxuYm9keSB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIGZvbnQtZmFtaWx5OiBSb2JvdG8sIHNhbi1zZXJpZjtcclxuICBmb250LXdlaWdodDogMzAwO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmNWY2Zjc7XHJcbn1cclxuXHJcbi8qIEZvcm0gZWxlbWVudCBzZXR1cCAqL1xyXG5mb3JtIHtcclxuICAgIFxyXG4gIC8qIHBvc2l0aW9uOiBhYnNvbHV0ZTsgKi9cclxuICB0b3A6IDA7XHJcbiAgLyogbGVmdDogNTAlOyAqL1xyXG4gIHdpZHRoOiAzMDBweDtcclxuICAvKiB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSk7ICovXHJcbiAgbWFyZ2luOiAycmVtIDA7XHJcbiAgei1pbmRleDogMTtcclxufVxyXG5cclxuZmllbGRzZXQge1xyXG4gIG1hcmdpbjogMCAwIDFyZW0gMDtcclxuICBwYWRkaW5nOiAwO1xyXG4gIGJvcmRlcjogbm9uZTtcclxufVxyXG5cclxubGVnZW5kIHtcclxuICBmb250LXdlaWdodDogNDAwO1xyXG59XHJcblxyXG5sZWdlbmQsXHJcbmxhYmVsIHtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPXRleHRdLFxyXG50ZXh0YXJlYSxcclxuc2VsZWN0IHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwYWRkaW5nOiAwLjVyZW07XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgYm9yZGVyLXJhZGl1czogMC4yNXJlbTtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZTVlNWU1O1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbiAgLyogTGlzdCBzb21lIHByb3BlcnRpZXMgdGhhdCBtaWdodCBjaGFuZ2UgKi9cclxuICB0cmFuc2l0aW9uLXByb3BlcnR5OiBub25lO1xyXG4gIHRyYW5zaXRpb24tZHVyYXRpb246IG5vbmU7XHJcbn1cclxuaW5wdXRbdHlwZT10ZXh0XTpmb2N1cyxcclxudGV4dGFyZWE6Zm9jdXMsXHJcbnNlbGVjdDpmb2N1cyB7XHJcbiAgYm9yZGVyLWNvbG9yOiAjZWY3ZWFkO1xyXG59XHJcblxyXG50ZXh0YXJlYSB7XHJcbiAgbWF4LXdpZHRoOiAzMDBweDtcclxuICBoZWlnaHQ6IDEwMHB4O1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPXRleHRdLFxyXG5zZWxlY3Qge1xyXG4gIGhlaWdodDogMzRweDtcclxufVxyXG5cclxuc2VsZWN0IHtcclxuICBmb250LXNpemU6IDAuODc1cmVtO1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPWNoZWNrYm94XSxcclxuaW5wdXRbdHlwZT1yYWRpb10ge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB0b3A6IDVweDtcclxuICB3aWR0aDogMjJweDtcclxuICBoZWlnaHQ6IDIycHg7XHJcbiAgbWFyZ2luOiAwIDAuNXJlbTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZTVlNWU1O1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbiAgLW1vei1hcHBlYXJhbmNlOiBub25lO1xyXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcclxuICAvKiBMaXN0IHNvbWUgcHJvcGVydGllcyB0aGF0IG1pZ2h0IGNoYW5nZSAqL1xyXG4gIHRyYW5zaXRpb24tcHJvcGVydHk6IG5vbmU7XHJcbiAgdHJhbnNpdGlvbi1kdXJhdGlvbjogbm9uZTtcclxufVxyXG5cclxuaW5wdXRbdHlwZT1jaGVja2JveF0ge1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxufVxyXG5pbnB1dFt0eXBlPWNoZWNrYm94XTpjaGVja2VkIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWY3ZWFkO1xyXG4gIGJvcmRlcjogbm9uZTtcclxufVxyXG5pbnB1dFt0eXBlPWNoZWNrYm94XTpjaGVja2VkOmFmdGVyIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBjb250ZW50OiBcIlwiO1xyXG4gIGhlaWdodDogNHB4O1xyXG4gIHdpZHRoOiAxMHB4O1xyXG4gIGJvcmRlci1ib3R0b206IDNweCBzb2xpZCAjZmZmO1xyXG4gIGJvcmRlci1sZWZ0OiAzcHggc29saWQgI2ZmZjtcclxuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSg1cHgsIDZweCkgcm90YXRlKC00NWRlZykgc2NhbGUoMSk7XHJcbn1cclxuXHJcbmlucHV0W3R5cGU9cmFkaW9dIHtcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbn1cclxuaW5wdXRbdHlwZT1yYWRpb106Y2hlY2tlZCB7XHJcbiAgYm9yZGVyLXdpZHRoOiA1cHg7XHJcbiAgYm9yZGVyLWNvbG9yOiB3aGl0ZTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWY3ZWFkO1xyXG59XHJcblxyXG5idXR0b24ge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbjogM2VtIGF1dG87XHJcbiAgcGFkZGluZzogMC41cmVtIDJyZW07XHJcbiAgZm9udC1zaXplOiAxMjUlO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgYm9yZGVyLXJhZGl1czogMC4yNXJlbTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWY3ZWFkO1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbiAgYm94LXNoYWRvdzogMCAwLjRyZW0gMC4xcmVtIC0wLjNyZW0gcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG4gIC8qIFdlJ2xsIHRhbGsgYWJvdXQgdGhpcyBuZXh0ICovXHJcbiAgdHJhbnNmb3JtOiBwZXJzcGVjdGl2ZSgzMDBweCkgc2NhbGUoMC45NSkgdHJhbnNsYXRlWigwKTtcclxuICB0cmFuc2Zvcm0tc3R5bGU6IHByZXNlcnZlLTNkO1xyXG4gIC8qIExpc3QgdGhlIHByb3BlcnRpZXMgdGhhdCB5b3UncmUgbG9va2luZyB0byB0cmFuc2l0aW9uLlxyXG4gICAgIFRyeSBub3QgdG8gdXNlICdhbGwnICovXHJcbiAgdHJhbnNpdGlvbi1wcm9wZXJ0eTogbm9uZTtcclxuICAvKiBUaGlzIGFwcGxpZXMgdG8gYWxsIG9mIHRoZSBhYm92ZSBwcm9wZXJ0aWVzICovXHJcbiAgdHJhbnNpdGlvbi1kdXJhdGlvbjogbm9uZTtcclxufVxyXG5idXR0b246aG92ZXIge1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmY5NmM4O1xyXG4gIGJveC1zaGFkb3c6IDAgMCAwIDAgcmdiYSgwLCAwLCAwLCAwKTtcclxuICB0cmFuc2Zvcm06IHNjYWxlKDEuMSkgcm90YXRlWCgwKTtcclxufVxyXG5idXR0b246YWN0aXZlIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWY3ZWFkO1xyXG4gIHRyYW5zZm9ybTogc2NhbGUoMS4wNSkgcm90YXRlWCgtMTBkZWcpO1xyXG59XHJcblxyXG5cclxuLnJpZ2h0Qm94IHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDMwdmg7XHJcbn1cclxuXHJcblxyXG50YWJsZSB7IGJvcmRlcjogMXB4IHNvbGlkICNjY2M7IGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7IG1hcmdpbjogMDsgcGFkZGluZzogMDsgd2lkdGg6IDEwMCU7IHRhYmxlLWxheW91dDogZml4ZWQ7fVxyXG4gICAgICB0YWJsZSBjYXB0aW9uIHsgZm9udC1zaXplOiAxLjVlbTsgbWFyZ2luOiAuNWVtIDAgLjc1ZW07fVxyXG4gICAgICB0YWJsZSB0ciB7IGJvcmRlcjogMXB4IHNvbGlkICNkZGQ7IHBhZGRpbmc6IC4zNWVtO31cclxuICAgICAgdGFibGUgdGgsXHJcbiAgICAgIHRhYmxlIHRkIHsgcGFkZGluZzogLjYyNWVtOyB0ZXh0LWFsaWduOiBjZW50ZXI7fVxyXG4gICAgICB0YWJsZSB0aCB7IGZvbnQtc2l6ZTogLjg1ZW07IGxldHRlci1zcGFjaW5nOiAuMWVtOyB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO31cclxuICAgICAgQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNjAwcHgpIHtcclxuICAgICAgICB0YWJsZSB7IGJvcmRlcjogMDsgfVxyXG4gICAgICAgIHRhYmxlIGNhcHRpb24geyBmb250LXNpemU6IDEuM2VtOyB9XHJcbiAgICAgICAgdGFibGUgdGhlYWQgeyBib3JkZXI6IG5vbmU7IGNsaXA6IHJlY3QoMCAwIDAgMCk7IGhlaWdodDogMXB4OyBtYXJnaW46IC0xcHg7IG92ZXJmbG93OiBoaWRkZW47IHBhZGRpbmc6IDA7IHBvc2l0aW9uOiBhYnNvbHV0ZTsgd2lkdGg6IDFweDt9XHJcbiAgICAgICAgdGFibGUgdHIgeyBib3JkZXItYm90dG9tOiAzcHggc29saWQgI2RkZDsgZGlzcGxheTogYmxvY2s7IG1hcmdpbi1ib3R0b206IC42MjVlbTsgfVxyXG4gICAgICAgIHRhYmxlIHRkIHsgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkZGQ7IGRpc3BsYXk6IGJsb2NrOyBmb250LXNpemU6IC44ZW07IHRleHQtYWxpZ246IHJpZ2h0O31cclxuICAgICAgICB0YWJsZSB0ZDo6YmVmb3JlIHsgY29udGVudDogYXR0cihkYXRhLWxhYmVsKTsgZmxvYXQ6IGxlZnQ7IGZvbnQtd2VpZ2h0OiBib2xkOyB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlOyB9XHJcbiAgICAgICAgdGFibGUgdGQ6bGFzdC1jaGlsZCB7IGJvcmRlci1ib3R0b206IDA7IH1cclxuICAgICAgfVxyXG5cclxuXHJcblxyXG4gICAgICBib2R5eyBtYXJnaW46MDsgcGFkZGluZzowO31cclxuLnBsYXktYnR7d2lkdGg6NTBweDsgaGVpZ2h0OjUwcHg7IGJvcmRlci1yYWRpdXM6NTAlOyBwYWRkaW5nOjVweDsgYmFja2dyb3VuZDojMDAwIHVybChkYXRhOmltYWdlL3BuZztiYXNlNjQsaVZCT1J3MEtHZ29BQUFBTlNVaEVVZ0FBQURJQUFBQXlDQVlBQUFBZVA0aXhBQUFBR1hSRldIUlRiMlowZDJGeVpRQkJaRzlpWlNCSmJXRm5aVkpsWVdSNWNjbGxQQUFBQTlkSlJFRlVlTnJNbW10SUZGRVV4OGRWTEhzU2xxVmhxUmlvSkZMNG9jakl5RWRpOWlBTEFvTWdDdWxGRUVTQlZFUWZDdnRvVVBSQWtzQ2dzS2draXFSQXl6SjZvQ0dLYWJna2xZYjVTRE8xNlgvWU03S0tqNTA3ZCs3T2dSL3JMczdkKzU4Nzk5NXovbmNEZEYzWEpNWmlFQTNDd0RKK25RR0d3Vy9nQnMyZ0hUU0FibGxmSEdUeGVoZElBQnZCV3BBRUZvRnBVMXpYeTZKcXdIUHdETFJhNlVpQTRJZ0VnNjFnTjhqZzkxYUNScWdNbElCS29SWklpRWsyZ3lyZG51Z0h0MENTMlg2WitlZFlVS0tyaVU1d0NvVElGcElObW5YMVVRSGlaUWs1Q2daMC80VWJwRm9WY2s1M1J2d0VPYUpDQ25SbnhTK1FQbEYvSjFwK2FWa3Q1bjNDU2ZFVlpJRmFYL2FSRmJ4QnpkT2NHZFVnYzJ4V01QYU8wNDU4U1VERVA0VkNWb0dDOFZJTTd6Z0VWZ3MwVHZuVEVmQkZrWmpETEdqY25UMEN0QWxPeEhwdUl3b1VnNzhLSm4rNTkyVDNIcEdESUZ6d0RnV0NFQjZSUFNDYkUwSTdJNFBueXFoSGF5R3ZWTExpS1VnREo4RVBtNFRRemR0dmFEQ0VwSU5JeVY5RXE4cDVzQUhjdFVsTUd0Yy9JMEx5Ykh3RTZrQXUyQUhxSmJjOUIyeWpWeElTQVpZcldHbnVnRlJReU5XaXJLQVJqeUVoaVN4R1JkQjhPUTdXZzhlUzJvd0hhMXhjV3djbzNwMXBSY3NCK2FERllsdTBVQ1c2RkQxVzQ4VVF1QUxXZ2F2OFhpU20wMEpGUW1MOW5EdTVlUmxORTY3WGViS0hPU1FaZk1FYjNBbnd6YXoxNEpMZ2dNaU1QbkFCcElEcm9OK01MK1hFK016WndiQVpJWDBPRTdFU2xJTlNNTXVNa0RhSENLQWE2QXlvNENyUXRPWFo0QUFSMjlrNlBRM21tcDNvNUZRR2NTN2tyNGpsenVkWlhDRGNKS1FKRFBoZ1BNdU1FSzUvanJIcGJTWEkvSDVEajlaSDNwUlVSVHF2U0lVU1JHanNxTlM0dUc1NHBVQUFuWjBVZ1llVTVFbHM5d0dOaXJHUDNMUlJnRkhKVmZMakpITURKcC9yQ1UwTlE4aEw4TTRHRWNsOHh5ZzVqTEtoL1RLamxIWjV6ZnpMRXI4Z0ZKelZQRVpmbGswajNUMnF6MTZXQ3AxRnZCZTBaaHE5MnNrRm54VFlRVVdUbWRoYndKQkFvM1VnQVpRcVBHcUluRXdJZWNFM0JCcnVBUjBLbmZtOXZyanhvWnd1K0t0eW5DcXVnWDFqUDV6b1dDR1pNOUFGRGhOUnBYbGN6QzVmNjVHM21zZjY3SEtRaUZyT3lick1GbGJsZkdHN0EwUjhBRHUxeWR4K0h3NURVMENUSDQvY0hvRndXY2ZUMGVDK1lnR0RmQmpyMDFtN21SOE1CSUo4MEtwQVJEWEl0T3VYRHdaTHdFWHczUVlCbEJFY0FMUE45a3YwUnpVYUo0Rzd3Q2JOYzF3bmFydjJzcWQxRzl3RFBTS05XQkZpeEV3UXgwNDdtZE5Md1h3MkU0SzlCTktCNlIvUXdUU3drZjJhcTlSaEs1MlFJV1RFdGdReG11ZmdKWTcvSmpzbmlBMkNRZERKQWhyWnZHN2h6eTNIZndFR0FGZHdvSkh6c0JHREFBQUFBRWxGVGtTdVFtQ0MpIG5vLXJlcGVhdCA1cHg7cG9zaXRpb246IGFic29sdXRlO2JvdHRvbToyNXB4O3JpZ2h0OiAxNzVweDsgbWFyZ2luLWxlZnQ6LTI1cHg7IG1hcmdpbi10b3A6LTQwcHg7ei1pbmRleDogOTk7IGN1cnNvcjpwb2ludGVyO31cclxuXHJcbi5wYXVzZS1idHt3aWR0aDo1MHB4OyBoZWlnaHQ6NTBweDsgYm9yZGVyLXJhZGl1czo1MCU7IHBhZGRpbmc6NXB4OyBiYWNrZ3JvdW5kOiMwMDAgdXJsKGRhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBRElBQUFBeUNBWUFBQUFlUDRpeEFBQUFHWFJGV0hSVGIyWjBkMkZ5WlFCQlpHOWlaU0JKYldGblpWSmxZV1I1Y2NsbFBBQUFBdzFKUkVGVWVOcmNtdHRMVkVFY3gyY1AycFZTb3d0S0ZCUVJTcG9ZVzFRRVFVVlJJWFNCSHBJSXVtQlBJUVUrOUJkMGdTS2hySjZqaU9oQ2dUNEVaUTlkS1NoSktib1JoRVdCYUNtNWF0UDMxODdBWVpoZDk4eWM0NTdaTDN4d1BjNjY1N056enBscmduUE9Rc29pa0JSVWduSlFCaWFEQkVpQlB2QU5mQVF2d1RQUUNmN1lmbmpDVW1RZTJBbDJnRG93eWVCL3ZBVnQ0SW9RTXd1SkdGQUpXa0V2RHkram9CMXNNam1ub0c4b0JTZkFBSTgydDBCMVZDTHJRQmNmdi9TRHByQkZqb0lVejArdWlpdkJXcVNGNXo5UFFFVzI4eHpycVhVUkhHRHh5R3V3R1h6Vi9kSEw4c1pUTVpLZzFJQWJvQ1NJQ0FrY1lmSExjbkJKTkxCak5vaEx3U013aGNVM1RlQk1OcEZpMEFGV3NuaG5RSFNGdWpOZFd2c2RrS0JNQlNjejFVaXA2TUROWmU1a0E3aEhMNHA4Qi9kYVNBeUM3NXB2YmJaeTdEZjRvUnliQm1ZYWZtNnpGSkVOeWdUTDdzZGRVS3l3UzFQdXNxYmNRY3VPWmgwNXlIdGtqUmhEbUdZVURHc3dMWmRyNlB3Yi9EZjdkdHR4VFk1dFZLN2xnbVFMbU9pSlIrNWE1bTVvWkxxRVJCYUlYMXdOMWZJS0Vxa1N0ZUp5bG5tTzE0Yk1RazlNSUxpZU9TUXlvd0JFcG51R1V6aHh5Ly9ITHk4QUVlNkpMckhyR2ZRMG5UMFgwMHNpbnd0QXBJZEUzaFdBU0RlSnZBRy9IQmQ1VGlJOUxEMW41R3FHd0ZQNStHMTNXT1FGK0NUSEFqZkJpT1hBU3ZkTnFSbkpzVnlRWEtmS2tHTjJ1ay91aThHOFNXYUJWY3F4R2syNWNrMjVLZ3VKZm5CTm5VWFpDdTQ0ZGxtMWdrT3FDRjFtRDhGcVJ5U29SMUlMM3F2ajViL2dtRU45cnhZcG9kYUl6SG5RR0hNSldrQk4rdHMvblFoTjJ6OW1kdE5EVVlhbWo5YUwyNEQ1N3dzMXRCYmVFT1BXdmxtVnlDUkNvY1g4UFpadFN4UTVDMDdyUnlUWjF3OTNnMkVlajF5d1hRemRCdnJ5TEhFOHJPWHBKT2pNZ3dDdHRlOExlOE5BR1RnM2poSVBRRzFVV3pqa0RvaU9DQVUrZ0VaUUZPVmVGRC8xb0EwTWhTVHdDaHdXTlIvNGZHeTNPVkdxUVQzWUtIcThKVG0rTHlXRzJiVDRlbHY4VEptZVJCZ2kvbFNJYnZsaU1KK21NbGw2Q1k3YUs5cGM5aE44RVgya0xwYmVnQlpLVy9WUGdBRUFHWVh2Mi90UDFMMEFBQUFBU1VWT1JLNUNZSUk9KSBuby1yZXBlYXQgNXB4O3Bvc2l0aW9uOiBhYnNvbHV0ZTtib3R0b206MjVweDtyaWdodDogMTc1cHg7IG1hcmdpbi1sZWZ0Oi0yNXB4OyBtYXJnaW4tdG9wOi00MHB4O3otaW5kZXg6IDk5OyBjdXJzb3I6cG9pbnRlcjt9XHJcblxyXG5cclxuLnN0b3AtYnR7d2lkdGg6NTBweDsgaGVpZ2h0OjUwcHg7IGJvcmRlci1yYWRpdXM6NTAlOyBwYWRkaW5nOjVweDsgYmFja2dyb3VuZDojMDAwIHVybChkYXRhOmltYWdlL3BuZztiYXNlNjQsaVZCT1J3MEtHZ29BQUFBTlNVaEVVZ0FBQURJQUFBQXlDQVlBQUFBZVA0aXhBQUFBR1hSRldIUlRiMlowZDJGeVpRQkJaRzlpWlNCSmJXRm5aVkpsWVdSNWNjbGxQQUFBQXh4SlJFRlVlTnJjbWt0b0ZERVl4elBEYW11eEtvcGRhRlZRRVYrZ2lGVmFGYlJXUkVSOEliUkZLaXBWMEtNSGJ4NjhlVlZRUlBTeUY4V0xGUEdCVk1SblMydFBvbGdvaW1nVldsOTFpN0R0MXZqLzJBeU00MDduc1VrMzJULzhMcnZ6eUgrU2ZFbSt4T0tjTTBtYUJoYUFEV0F4cUFGelFTV3dRUVo4QjUvQUVPZ0VmZUNMakpjbkpOeGZENXJBWnJBRWxFVzQveWQ0QWU2QjIrQkQ3SkpRamNTZ0Fod0ZuVnlldm9HTFlHV2NNc1V4c1IvMGNuVktDME1MVlJtcEFTaytlZm9JV21VYmFRRDl2RGk2SnBweXdVYm9xNHp3NHFvREpBc3hjaHlNY1QzVUJhcjl5bXBOTUk2MGdoVFRTOC9BTGpEcy9jUFB5RVlSMnl1WmZyb0Ztc0dvKzBjN3o0VTBHbC9WMUFScEh6Z1Zwa2F1Z0dOTWI0MkFSdER0WjRUK3ZBdW1CanlJcXBVckxPZ1VuOWJpMWlPd0hXUzlScWp3ajBGZHdBUEd4UVA2Rkpwb0I2dENYTnNDYm5nbmpidERtSEJFTTlZQmhUVXlGdks2MDZMeloyeFhwMitMK05WVWlWcUdGZkxhTldDYk8yb3RCMXVabVRyc05ySlg4VmRXcVFhUXRFVS9hV1RtYWc2MUpqSlNCZFl5czFWTFJoYUI2WVlicVNjajYwSU1QcnFyaWd6TVpPWnJOaG1aVlFKR0tzaklqQkl3a3JCWmlZaU0vQ29CSDFrNzM3TFJRUDIyUlQ3V2RQMGdJejNnaitGR1BwT1JkeUJ0dUpFblpJUlMvQzhOTi9MS0Zrdlhod2FiK0VybGQ4YVI5Z2pMUzkxRWxURGtHSGtMT2d3MWtuS3ZFQ2xxWFk1d2MwWmh3VVlqUk5FZXB3TGNXUlJLa1Q1bnVYUnAwR3hnaFhoaFFySUpMcElQWWRkSDUwUTUva3ZRMFQ3Z0F4YWNvQnRYT1BaWUlUOFE5WTBkTEUrQ3p0RWxjRUx6ZnBFV1NZZGVkelB4Nmd4NG83bVJzMjRUZmpWQ3FoTjlSc2RGMTAxdzBHbFNFOVVJcVF1Y1pKNDlDQTFFaWVzMnJ3a1drSFM0em5MYkM3cVlvWWphNURjdkRGb2gwbUJ6UklQRjEzMndSOHdMZlFKM3VPM3BUZUIxa1RaQjZmQkFtY3dEQTBteDU1MmRKQVB2UVl2S0l4dzdRYmRDQThQZ1BKaXYraXdLVVE0T2dhY1M5K0VId1FXd0xFNlpMQW5udFdyQkFiQUZyQWJsRWVaVmd5TFUwNWgxaCtYT2NzV2IxMGc4ZUVienMzbGdQVmpLY3VuK2F2YnZ3VFBLMkF5STZFUGh0RitZS1ZoL0JSZ0FFNGgrMXR2UVhIb0FBQUFBU1VWT1JLNUNZSUk9KSBuby1yZXBlYXQgNXB4O3Bvc2l0aW9uOiBhYnNvbHV0ZTtib3R0b206MjVweDtyaWdodDogMTAwcHg7IG1hcmdpbi1sZWZ0Oi0yNXB4OyBtYXJnaW4tdG9wOi00MHB4O3otaW5kZXg6IDk5OyBjdXJzb3I6cG9pbnRlcjt9XHJcblxyXG4ubXV0ZS1idHt3aWR0aDo1MHB4OyBoZWlnaHQ6NTBweDsgYm9yZGVyLXJhZGl1czo1MCU7IHBhZGRpbmc6NXB4OyBiYWNrZ3JvdW5kOiMwMDAgdXJsKGRhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBRElBQUFBeUNBWUFBQUFlUDRpeEFBQUFHWFJGV0hSVGIyWjBkMkZ5WlFCQlpHOWlaU0JKYldGblpWSmxZV1I1Y2NsbFBBQUFBNjFKUkVGVWVOcnNtVWxJVlZFWXh6TTFTMHNiSk12TU1CUTBYS2dOMElBTkJvVldGQVZCcmxwWUpMcXdWYldMYUdWSlJrVzBNUnBXTmtvaFJGU1lvRUhESXFrb1VsQWJ6QXcxVVhMZzlqOXdEdjM3OGoyOXZYZXZyN2dmL1BBNzAvWDgzNW51K1c2WVpWbVQvZ2NMODRSNFFqd2huaEJQeUNpMkcyU0RDNkRaWnR1TklCYmNCaU0rYXlraERsTm0vYko3SU54RzIyMWdXTGM5NXErdWt3SW1nMVBXNzlZSjRtdzhvMUswMytLMmtPbmdwdlduZmJJcEpBZDhwL2F0WUk1YlFoYUNSbXQwRzQrUUtKRXVFczg0NzRhUWJOQmkrYmF4aE93QnplQ09xRmREenhnRVdVNEtLUURkbG4vekowUnRBbTFVOXlLVnBZa3BkczBwSVFmQWlEVzIrUk9pam9KYm92NG1LcStnL0I5YVhGQ0ZITGZHYjJOTnJTVFFSZldmZ2toZGxnTDZxYXdpV0VMVW9yeHMyVE1wWklsZVYvemNZdEZtRjVYeC8ydmhqWUZQOWloUURCYU44OFJkQmxiYlBLVS9nM1RRQTdhQzZ5QVNsSUl6dXM0VTBBVFNkUG9CeU5QK1psQkx6MXNMNnVUSmZ0aHkzbmhFemxGK0QwaWd2aHlrTXJYMk11bDgra0JsNWFiTlpGS1g3Zko3WGpYNTZsMnFoTkpYOUtncFUzM00xMzZmSGlGaks4RU1VOG5Zc010Q0hvSWFTdThGMGRyL0FocW9iQjM1bksrbVg0b1VNaEZXU2Y0Q3NKelNkZVJubVY5ZXJ4OWo4U0ExRklRMGdvK1V6aVgvQ2ZuelFMTDIyOEVRVGJ2a1VCRFNEMTVRbWtkRTNWc0d6YjFKaTFIMkRmUlN2WVJRRUtMc0ZmbHp5ZS9UR0p1cC93N1FSbUEyaXBBUXdwMmFSdjRRVFNGenpwbE5hWkR5STBORmlPeThzWENOM0ZWVm55TW9meVJVaEtTUjMwVysyb3BqS0czV3hWVGF3Y3dVbkhBaDZwZk5vZlJMOHBQRWFIWFFXb21qL001UUVKSUpGbFA2a1hpWE05WU5XclUvWDQrS3NiWlFFTEtQK3RBalR1MDFZcVRNdEVzWEFwdWxrQWlYUldUbzF4SmpONml6MGVKd3JDZC9CZmt0NEwwVTB1Q3lrRUthSW1ybkthZXk3WG9LR2F1bHJUYVA4cCtCcjNJVVR1dWhHczk5UkoyMEJXQnBBRUtlazYvdUlxOHBYU0ttVmFQMmM4VFV1aHVNU0dPc2lHNzh6UTF4Zzc0QmhsRmV2bWhUU21VbmZBWDdBcjJ2UjRDelFieXpSNE0zVkwrTjZzOEdIVlIyMVlrb3lxRWdDVGtwNmhmNWlDRXJXK1ZVWEVzRjF3WUNGUEtPNmo3V0k2N3laNEYyS3F0M090S1lLNGJmcnBEOW9GZFByMVRLbDhId0FqZGl2K21nS1lEWWJ5S0lvZlI2TUVUUHFIVXpHaDhQN2djaEdwOG9Zc2w5SU1QdDd5TXFlRllWNFBlUkk2SjkyVVI4NkRFY0ZZdlV6aGVySGRTMjJsOWR0NzRoRnVvMzNTcncxbWJiblNBUlhCSzNTZStycmlmRUUrSUorWWZ0cHdBREFKczR1ekxXb3BXNUFBQUFBRWxGVGtTdVFtQ0MpIG5vLXJlcGVhdCA1cHg7cG9zaXRpb246IGFic29sdXRlOyBib3R0b206MjVweDtyaWdodDogMjVweDsgbWFyZ2luLWxlZnQ6LTI1cHg7IG1hcmdpbi10b3A6LTI1cHg7ei1pbmRleDogOTk7IGN1cnNvcjpwb2ludGVyO31cclxuXHJcbi5tdXRlLWJ0LnN0b3B7YmFja2dyb3VuZDojMDAwIHVybChkYXRhOmltYWdlL3BuZztiYXNlNjQsaVZCT1J3MEtHZ29BQUFBTlNVaEVVZ0FBQURJQUFBQXlDQVlBQUFBZVA0aXhBQUFBR1hSRldIUlRiMlowZDJGeVpRQkJaRzlpWlNCSmJXRm5aVkpsWVdSNWNjbGxQQUFBQXFwSlJFRlVlTnJzbVUxSVZVRVV4NTlTWm9pS2lCVjlXR0FhcVl1S3NLS0NRbHRFdEk1Y0pFSWY2MElNSVlQYXBBdWgwQ2lLRnRMbVNRVFJ1aFNFUHFBV3JXb1hoS0pQalQ0b0F5bTkvUWZPeGNQaHpudjM5dDU0NzhRYytQRzRNMmZ1dkQ5ejVzekhMZkk4TC9VL1dKRVQ0b1E0SVU2SUV4Smc3V0F2dUE4K0dPbEJDVEhNZFcvWjNvTFZKdm94T1NJbDRCN29ZR1Z6b0I1OHR5VzAxbE1ZblJUbDAyQ25MVUthd0dPd0k2RE9HaUdIUUJwczB0UmJJZVEwaFZOWkZoOWpRZ3FWTlhxOGNEWUZLazFrclh4Zm9FYjBqaGZlcEpDRG9CYzBSZXozTExnRUtnb2hwQXFrdldqR2hkU0JuMVNlb2Vjdy9mYXg5OTMweTFlSlNPc0RSOVRjQ1JHVjY4QzJQS0s2Z3MwbmxhNmZnRll3bTZYTlpjSzNqYm81WXRyNGlLaXd2Q1hxeDBDcFppUk9nRVhtK3hYczBZWFdTZ3BSbElCUjRYTTdRTVJ1OElYNS9DWmhxYVFJVVZUVEhvemJWVmEvRlh3UzllZHpaYTA0aFBoL2RscjR0dE9JdlJEbC9XSFNiMXhDRksxZ2dmbCtwam5ETFIxMkhZbFRTSXBHNFkrbTdXdFFib3NRUlZlQW1IZGdRN1oyeFFrOHRiNVhxNElvbXdLWktIdXR1RWVrR2N4bzJ0Nk5zdGVLVThnVzhKSDVxdkNhRmUyN2t5NWtEWGdwZkcrQUJ0cUgrYllFVGlWWnlBUGg5NVJkVXJUUlN1N2JQTmlYUkNFRHdtY2NsQW1mRHVFeklYZkxjUXZwRlBXVG9GWXpENjRFcE9RYW5aQTNOTWtXNlRjYitRclpEbjZ4dWgvZ1FJNDFaa1M4YjFCM0hqa01ha0Z4UUM2WDFnSUdRZVUvcmhmbFlLMjRqWHlWbzAwbnFBTEgySm1vSUdmMi9RR2J2U2pua1l2Z09UZ1Q4V1Q2a0JKQ1l5RnZHaHZCQ0dnTzRadjQ2NkFhTUF5TzJ5N0V2K3NkQXVkc3Z0ZmlYTFB4WGt2SEJYYlZzeUpDVEg1V09Bb2VnV3BXbHFIUSttYmJGNnRkbE5FYTZIa0cxSUY1Rzc5WWJRYlBhSC9VYmFvZjl6SFVDWEZDbkJBbkpCYjdLOEFBNWpOcHVGY1FoYWdBQUFBQVNVVk9SSzVDWUlJPSkgbm8tcmVwZWF0IDVweDt9XHJcbi52aWRlb3t3aWR0aDogMTAwJTsgaGVpZ2h0OiAxMDAlOyBwb3NpdGlvbjpyZWxhdGl2ZTsgYmFja2dyb3VuZDp1cmwoZGF0YTppbWFnZS9qcGVnO2Jhc2U2NCwvOWovNEFBUVNrWkpSZ0FCQWdBQVpBQmtBQUQvN0FBUlJIVmphM2tBQVFBRUFBQUFaQUFBLys0QURrRmtiMkpsQUdUQUFBQUFBZi9iQUlRQUFRRUJBUUVCQVFFQkFRRUJBUUVCQVFFQkFRRUJBUUVCQVFFQkFRRUJBUUVCQVFFQkFRRUJBUUVCQVFJQ0FnSUNBZ0lDQWdJQ0F3TURBd01EQXdNREF3RUJBUUVCQVFFQ0FRRUNBZ0lCQWdJREF3TURBd01EQXdNREF3TURBd01EQXdNREF3TURBd01EQXdNREF3TURBd01EQXdNREF3TURBd01EQXdNREF3TUQvOEFBRVFnQ2lnUUFBd0VSQUFJUkFRTVJBZi9FQUw0QUFBSUNBd0VCQVFFQUFBQUFBQUFBQUFZSEJBVURDQWtDQVFvQUFRQUNBd0VCQVFFQUFBQUFBQUFBQUFBREJBQUJBZ1VHQndnUUFBRUNCUUlFQXdRSUJBTUZCZ1FDQ3dFQ0F3QVJJUVFGTVJKQlVXRUdjU0lUZ1RJVUIvQ1JvYkhCMFVJSTRWSWpGZkZpY3JJekpEUVdncUxDUTNNMWtsTTJDZEpqc3lVWDRvTjBrMFIxSmpjUkFBSUNBUVFDQVFNREF3UUJCQUlEQVFBQkVRSURJVEVTQkVFRkUxRmhJbkV5Qm9HUkZLR3hRaU5TOE1IUkZUTVc0WElrQi8vYUFBd0RBUUFDRVFNUkFEOEEvT2ZiT2hDd0NaZnhqeWZHVDVJRk5rbFR4QUFtSmFqNi93QUlvaGJKUG5DQUNWQWdVbHpIV0tJRmxwajNYR1ZGU0ZKQlFTQ2VSaUl0YmtPMWJGdGNDYXYvQUREcjRtSnNIV3dRMzF3bDYyVWhKQkpBRlBBeFhJc0FWZjBYWktvZVIrblNOMWFKNU0zcUZWUUp3YVV6RnorOWJaUGNKZU1XQjFKZGpjamNyU0MwQlhDQmtseENpbW9sL0NHcUF6d0ZnTEFqRjNCQ3B5YWtwS2lWQUdvbENtUzZEVlBtRzJsNEszQ1grUDV4ejhsMXlOa2pOa2x1UUV3T1B0alN1b0lMYTlJOVZIR1NoUDY1L2pHdWFJT0hzSEl0dE1xUXBTUVRTdkh6Q2tYeVRJSE9jZlM3Yk9FRUVGT3ZzK3FLc2FxTGUyVTM2Z1NWaWNCR2FoT3E5YXRiZE8xYVZLQmxMandnZGcwb0o4RVdiVkQxMnB4SUx6SzZHZkZCQTZRS0gvUW5KQ3R5Q1JkWHQ0cWNnSEZFSGdaQVFXcWduSkE4dDVURG0xSTNUTWlZcXlGc3JrWmZhNTNzZXNxaEJGUGIwZ1lzMmtNckJPRy95amRvQjcvRWVJSGp4ZzFBTnRXUGUzd2k4YmJKdUZCVzMrWTZVSDhZNkdJSlhZejNBSmFiVWticGthZUloeXUwQTdMVXM3Y0x0MHRxQ1Nkd0I4STJ0ekRSVWR6MnlyeENWZ0VsQ1JwL3BsQjFzQ1MxQkt5dDFuSFBza0VLVVpBY1RWVVdYZGZpQXVUdExpM1FwSHBMSUhHWDhla1lzOUJKN2kweXlubHBMUWFVU0R3bFA3NFd1VFVpV09KZmRLVkxiV2dUQnJBQ1BjUDhmYnJiU2xNaklDVTRuZ2dUSWJJUlhXV25XSUN1RWVDeHFyc2lVNXpvQjR3YWdKRnRjNGE4dDhoSkxEaWtDYzFTcHI0OVlib0dvV0xXUDJMRGkxYkRTWU0rRmVVTTEyMUIyM0dCa0xsdktZVm5IN2twOUZ0VWp6cFBoV0piWXk5aldqT1lHNWJ1YmowbWx1QlN6VVZFaU9zSlcvY0I4Z3ZiNEs5U3BSTERncVRvUHpqVnYyL2NLdGlZTVU4aGFWS1FzUzRmVkhPemVUZFMwWXhqcmpnWHRWTG43WVFlNGRJWXVHYStGU2duVVMxNlFPeFljVzE0MkVqekRUVDJlMk9aMmxKdXBUWm9vdlVnSVVDVWNCNHh4YjFZWVRmYzdUakIyaEJNeCtIM3dLSUdzSUpZUkx3dkcxcWJVQUZHc2h6NDE2UjZEMTdTaGp0Uzc3a3UwdHZMZHB1RzZTZWZHUFdkWi9qL0FFQ01oWXUyY3VBaTZVa3BTcmp3NS9qQWMzN2dGaWRtY2lpMnRBMmtoU3BLRzNqTTA0K01YamFrcEE3MlhqTHpKNVI1MXhoeHRDWHR3VVFBQ2tCSm5RbU94Z3RvSHJzYmRZV3h0MFdpWEhMaEtGTU5Ka0NUK21RNVE3a2ErTXNUdmZ6eHlIY0Z0YnNqZWxRS2R5ZEtCSThZODUzOWFzZ1lZd0RCNFVvV1pGTzNYV2lUemppZFJmOEFjLzFDNHR3UTdoeVl5Rmg2YmNpU1RRZFpDR2ZjT2NYOUIxYkRvK1RqMXBhcmxlWENMY0xhS1FWa2dUVWdwNEFuVXg4czlqUnR1QUdVTnU4c0ZhTkpOMnpkSWRENEsvS1ZFYW5tQnlqbmRXalFoY1V0dllGOFBsdnpoQlVDUndrcVBTZFhSQWpOYXR1SlB3WVFUNmhJbjdZNkNaTmkwdE1DOVo1QkQreFVrOGVHb01YNUlQckQ0OHF4N2R6WGNlRmVRUGh4aG5HbTJReFhGOTZhdzNJVFNvZmZLT3hnVGdwN0REd0Y5SzNYTUNyYXZ1aDZpQStSZWQxMnE3dHdsQ1NhazA4RERlTk9ReTIrd0dXbUxlUWhXNXRZTXpMNjRObS9ab2JydUNHY3NIUmNxOGl1UDN4eVArVFlkYkFsZE5QTkpNbTFHWGhGMjJNV1VrM0NJTHJ4RG5rMDEvaE9Bc1V5MVlmSXNFRktDbFlOQnpnMks2TDY2WmJXdU5DcERkQnJXa0xsb3dzc08wRVhRRDZuU2tKLzFmZ0k1MmVsbTlBS3hXYTBQZVV3N2RvMnNJZENwY2lUcDR3dDhWamtkckRlVCs3YVVnM1NXaW9CVlB0bkJLcG9YcFIxY0Z0ZTR1YjYzQVpoUjY4SndlclIxc0MwQnZOMmZwc2ExMkdmMnhzNitEY1JtY0phdUJJVDg1L0dMT3hqL2FXK0N5cmRvamN0U1U3VE16NFZpckpzMWJZSjdudkN6VTJVZXMxTThJMTE2L21DVzRvKzRNazFkdXViRkpNOUplMk9obS9iSnUyeDh3bDQ3ajErc2xzcUJseVBEcVk0SGJVNkY0aG5ZenV0NVlLUXhVRGttZW5qQ1dLckQzSzNQM3p1VGFVVk5GRzFNdEI0Y0REMVUwSzNGU3B2MGxxQi9tUDN3OWlFTXBaV2pnM0FSMk1MWEgrZ0FON0FwS0VpZldPZDJQM0FMN2xEbEgwTk9LQklBbkw2ekM2TU0rV2xvdDlKY2JTVlNHNm5LR3NXNEcyNWxZdjEyNzZHU2lSVXZiV1g1eDE4VDBNaFV0YWk0MFpVMnBPbk1DR2sxQkF4dGJsSnhpbWdRVmtqeThUUXhtMWtRRDhqWk91QlNsSVVsSjQ4SUR5SjRGeGZZNHRPclhXUituS0Izc29DVUtxMXlQd1Rxa29HOHFPMGlsSmdBNnloVzlpN2x4OGFzcVE2RWFlYVZJSElJT01VVGw3RndMSHBtVzBEaVpHWENjZEhya0lxTVQ4RzZFekpBOGVmV1VkQ3JBWEo0b3FYS1g0eHZram41ZHozYUVldWZaOWtUa2hPMjRWb2NEaktrZ2lhVUsrd1RqUzFabUpLVzBzVGVQT0lFNnJJcDR3MWpOSmF5VWZkZmJLN1FoZm1JMkJYSGltY0h5V1hBdkkxeCs0SVloSHBPcENxRFNaNGRESEU3VFRSemJWMUNHNmJiZm1sS3dlVWowcEhuT3o1RDRGcVhkcmpDbTJDcSs2ZWZBVGhDdXJPbnhZcXU1TDROT3JiTWhKVzJ2V2thdW1ib29BNUZ3aHQ1QzVnelVEQVlnS0dURjZ4ZDdHeTRrS0lTSmNUSURTS0tJV1lhRm0ycFNUdUFCTS9FUlQyRDFZQVBPWEYxUDAyVktDdUtRSkg3ZXNjdnNibXllMjQrMnlsQzJWSmtEcVA0eHpMZnVJaUkrZ0tRNG9tUkNWR1hnSWF4N0I2N0FReGxIVTNvUUdpWk9sSU1od256TU4xTnJjS3NsY1hEL3BvTENnQzJtc2gvS09zV0hXd1dkdVk1c1kvZXR3SklJb1o4Wm5oUFNEWWR5eXdRODIzZGVudUcwZnE5cDlzUDMvYVlzVmw0Nm4xVmxCQ3A4b1hxbWdQa2hzMndkS3lzN0pneW5UNlZnUFlYMElVT1R4aVNTZDAvci9qSE10VmtCaFpWWnZKU0J1Qk5UOXZHRXNyVmR5QkxiYmJ0bVpVRWt5KzcyeHJIZE1yUU1iWHVWR0F4cVVKMnJXajlKbE15QTV5NVIxOEYwWElSWVR1emN0TjJFQ2JrcEpwMWx4bHhoOTVhL0cxOWlES3g5ay8zT2xYcnRxdDIwcEtnc2lRSUEzVDhzNDRHVnptMElDUGNxYkx0dTJ1R0VYU0hWdm9VbmJNa2dtc3FnY282UFdxeUNReGRxN2ZQT3lRcVMzbkNDT3ExRVIzdXVvMVpHTlRGOXB1b3R3NDRGb1NKVEpueUo0UTFuYzBTUmkyeGQyVm1FWEl0d1pwRXZOWCtFY210R3JmMUFzNWd1NDVhVmxVaUpkVEhwT0IwZ3N3Nml3bElJMEdwRStIV0EycVE5SnZtbTcwRlN3SnVvbk9YOHdCbkdOVVFkYk4zWXF4aUZJY1NWZWhNeUExbEZGMTNGKzY2dys2cVRsUXRVcGVKNVJtelFkR1Z0ZnBrQlJwN1NKYWNmQ0F1eFpUWlpJVVZPdG1hak9ncHhpNjNSRDdqR1ZPaFBxQ1UveGhtbGpGaTF1Y1FsMUEyejUwbkIwd0ovV2VJQVVRWnpuTGpyQjhkV3dkd3J0c1k0eGF1eVNUTk02K01OVm93UUpYVDY3VndsWWxJbldGOHlaQU55V1RYZFhSUWd6U1o2ZU1jdk0yZzFDMXhUempaQklscFdjYzY3czJiTHk5dVduR0pLVU4walRXc2p4alZYWWd1MXRLdUxvcFNDUVhKUnRjaURPN2Z3eTJFcGRHNEFBS05TQjErK0dLSmtEdDh0dVdxMjFxa1NBT3RBWU02dURWUUQvdDYvakI2UUpUd016ekVCZFdIVDBNN3Rrc0svcWxTUUR4SmxTQjNyb1IyZ24zbVRjWnRHMlVHZzhzd1pVSkE4WXpXcFhNb2xPRVZUVXUrOTdhYXdScUNjd2h3WGFxY3FyMUhBYUtueHBXVVZBRzlnenZjV3pnYlJhV2pVQUdSOEo4VEdIU2RoZXpMUDVjM3kzYzFhdnJIOHV0ZjFBOG9KU3JSaEczZVd2MnJyQ05zSTIrb0NxZ0FCcWxNcWdkSWV4L1FOWFlIckJYcTdXbGZwSS9DRzZsdG9OamptaGJlbzZkc205eWFVUEg3NEpWYWdXMUlJT3Urb3B4dVFLWmxOUU5KeTR3WmFJeEtLajAyMmJ0RFFsdFVhMDhPSHRpdVNNMzFSVzkwV2FSYnVyYVFsUkU1QUpIV00yY29UdFhVVGh4VHoxd1hIR2FIcFRXRjdtUzArRHVFb0NFTUNTZU1nUHJwT0FrSmRzeVFvQXByTUEwNHhSQWpSanZXU0NCVUNJQnVHbmF0czh4ZHNvOVB5RlZTUnBVZU1Hb1pTazJEYndsaGRzYjNOZ2NQRGFKMUhPR3FCYXFFTEx1SEFscDV6MFVrcDRTcHhNTVZaaTFRZFV3cXlRaFEzVFVRa2drNkdRaTdXVUEzc1c3T0h0THByMUNFbFJTVktCU0pnd20vd0J3SHlZRyszN0paVWphbVpKSHVENnRJMWI5b1ZiSDgvOEFMNFhEYW5HbWlhY0VrZUVjN01icnVCTjlnN3pGT2xnMjhraWVvNWV5RUh1SFd4RFh2YlFDUkk4cWRJSFlzaHFmdWtBcVFsUm4xKzZPZjJGSnVoNng3OXdYRmhhZmVNak16NERuSEx0UU1aOGxoYlhJYlZ2SzJrY0pkSmNQR0ZiVkdzVUZBN2hMT3hTcFRaRXhVVWwxanI5SnhBOVFFaGd2NzVsME1yQ3ZSWE9aRStZbHA0eDZqclhpcHZ3SDJiN2R0OEgyK2tXbm5lUVYwa1owU25uT01aY2lrQllTbHZhdVpPL0xWenVTa0xUeFBPZWtWUzY1RklmdUd4Yk9JczIxc0lTU3RyekhhQWVJMWxIWHdYMERWMkl0M25WdEZiVGFpTjVLU0FaZlNzTzJ5ZmhCb3hZM0NYTisrbktGcFN3eVpsUm1SWHhueWppZHQ4a1FtOXhxY3VtSEdVQ1NqUHlpbkFqaExqSEp3TGhrYis0WEh1QlZoamkyc0p1TnlVMDFKSSsySjdPNnRqL29PcllPRzJ5eTAycHRhMEJCQ2dVS0taN1NEcUNPVWVDN09CM2JnQm1MYTU3MHZibGh1eVZNcGFUNlltWm1VelUvWENlUHF1bTRoY0srekZXeXJhOCtJV0VyY0Npa0dzeVZnOFlkeC9nQ2dLYlBCcFc2THBDWmxCSkZLYXc3anRKQmoyK0dSZDQwdnVvQ1hLVUNhMUJPc0ZSQzN4ZDAxYnRKc25GQktFOFRMaklmaEQrQ3NzaFR2NHBWMWVLV3dDdEpXQ0NKeWxPTzNncG9VOWc4czdGT1B0Z1h5V3lwdWs1MW5NY2ZDSGExQXZlRERZV0xGODZ2ZVFSdU1xVG1KNjhvWm9neTJMYTY3V2JTSnRJQkVwbW5NUk0zN0RkUmE1enRvRzRWTkV0ZUVjai9BSk1PdFFXZTdRYmNIdTY5RCtNYWFrMHFTVnA3TGRhVnZZYVVTZVV4OUtRdGROQWN1SmhGaWUzTGdraTRiVWdKOTBtZklSTWN2WUoxdXUzNENxMXdiU0ZBRW1ZTkJXRzZVc3h5M1Zzd25hVDhLMFdFQ1pVS1FldlZkdytMcFdhMkJMTkJ4SlVGZ2cxMWlyZFJwSEw3ZlRodlFYbVB5TnpaWnBUaFRKb2JmTk9udkdPZGx4OFdjUEppNDJEOXp1UzM5S1pjVHVrZVdzb0ZzeG5Fb0F1L3o2cnNyUVpiUk1EU0NLeU9wZzNRQVpPMGJ1VkZSTlprL2ZHNjJUWjJNZjdTaGR4SzF0cWJiM3pWUVNKSDNRVnhCZGdYdXUwOG9nbDVMYjJ3YW5ldVgzeGVCcmxvRHJ1RExyTHJGd1czTndVTlptY09abitQOURkdGd2c3JnRmxDS1RGTlB4amg5alV2RUYrRVJKWm1QZVAzZ0Q2aEMrS29lNFNaQzFTbGtrMG1pZkxXR2xYNkMxeFU1aGxEWld0UENmU0c4U09mbUtLeHZCdlNOMWZHT2pqdG9BQ3EyeWJ6YWdBS0NVSlozcUJzVm1XRnhlQUtiU1NyY0NaZjZnZUhTRjFaU0Rld1JZUEtyeDdTbTMwaE81QlQ1Z0RxSmNSRFdOZ0xibUIwSnVidHQ1RXBCemZUVGp5OFk2T084SW9ZRnBiS3VVSlVCUGFrQ25RUzRRd3I2RUxmRldlN0tNc3VURFpuT3BsN3lScHB4aXJXSXdyN3hzcld5d3EzYmNoVGczMGxMOUlnTHVRU2VWYUs3RkxtMGIxQXpBQTVRTzl6ZFJTcHRydEYwOHBUWjJsYzB6bnBJUXRheHF4ZXRQdkpSNWt5QUg1UktBdGkxeEhjRjh4Y3QyOXMwRnBXcXVuNVIxT3UwaURVS1gzN1EzVnczNmF4S25pRFBRZElhNUFMZ3M5ZWhLaUFvVDVmU2tYek9mbDNNOWc0VnVsWFByMGpTYkU3YmhQWSt2TlkyMFVDUHJFb1l4bExjSWNIYm0zdVVsUWx1Y21aMTFoMVdTcUVuUUlPOGJKdTV0aVVnRStrbW9BMTJEbEMyWExwQUs1cjNlMlM3ZEt2VFNkd0ovR09abnR5UXRFc28yTWsvYXYvQU5RRUpBMUovT09KMktqV0Ntb3kyTTR5YkJQblR1S1ZBaVEvbGwrTUpZcTZuUjQ2Q1c3bHQwMzc3aTJ6TWhlNGdlTUd2UXNDSG1YR2txM2dncEhscWVFTFhxUXByZkpYbHZjSmRTRmYwMUg5VkRXWEdrRDRrR05iM1RtZHNGS2VFbEtrUHJFWnN0QTFkajR5L2pzU2dOdU9wQzA2aFFCTlBHT1YyRFpFdkwxaDVDbkdpa2dpa2dBT21ncEhOc3BzUkMzdjh0Y3R1dU5vVE5KTzNob1pDY05ZMW9IcnNSc2N4dXVHM25CTHpoUnBwMWhsYkcxdUhHUnlGcWx0R3hTQ1V0cEdnL2xBTVhJZGJFT3h6RjQ4a01XNk55RkdrcWFReGczTENFV2I2V0JjdnBVZzhhbmtEMGpveE5RZG5KN2FzVXIvQUtreVppZkdCOFFSUlhsODYwNkdtMDBDd25ocFA2NFh5MGJJVzRzbDNOdVhGSk13aWNLdkdUL1lFTHF3UzRzaFZGQWtBZUZPa280UGRwYWRDSGkyYWR0blV0Z0hiUG4xSDV4T3ZpdXlGOWNkdG92Ylg0aHhTZ0ZhMVZMVGtES09uU3RxN2tMWHRySFd3dVUyOXc0VU10bEpDcTErM3BHYlptdnhKSTY3M3ZObkE0eHRqR2x0MVpaTGFxSkN2Tk5Pc2pNZ0dBVlR0a1RJSVBLTHZlNUwzMXJzTFFrTzdoSlJsS3ZBUzV4M2V0amNJcjdCZmhyS3p4V3haVW55MU14T3AxMWpzMHJGU3dzdSs4MnZobFdqSmJPNlVwSlNEUVM0RHJHbW5HcG0yeFIybVZmUzZIQ21sSzBnTnFvQjVPZVRtUVljZDlOS3AvU1VkMlVkSUsyTFJrV1NYMCs4Wi9jSURhR1FBL2hMbTd5TGlBbWFVdUpsOVlQS0EycXlERmNMMlBzRWcwbXpMVXdPMnhhM0E2eHZIbFBMVWVEaXZ0TUFiMUNxeUxxNHloQ2dDcXNvSGJZMzl6NDNkSmRvbzBnZGR5YkZtYjVxM1pTVzFlYWZoU1hpWWFvL0JpeGJZM0lQdm5hYWlWS25sRFZFMkJMOWhpN1c4MFVKbUN0TTlkSTZmWHJ0SUs0M2NkaVV2MnUxeFBtV2hJbExuTG5EZWlCaTU3MTdWRERMemlHNVNCT2tvVXl3eWJpVngySlV1N1FGSit6cUk1ZWJFM3NHcUd6K0krSHR3dENhbnA0ZmRDVnNEazJWcHhyanFUTlBBL2RPS1dCeVFqNHZFbjRoYWxJOXhjL3FrWUxYRTBRUEc4Z0xSc05FZ1VDWmVFTVZvUW83N0t1Slg1VmVRNjFnenBvV21TTEhJajB3NGxYbitoZ1R4aFZjbE5KdjhpK1VxVE5veUkxck1ucEE3ME0yc1lzaGlYa0lHNUZFMStxcCs2TVZvQjVsQ2wrM0RpVWJxb1VBclNrb3UxQ2N3Nnh2Y3JXTGIyTXVTVW9EaUJXaC9DTUtqWUs5eWMvbFY1bGxTN2d6RXVjNlFhdUlIeVFROW1Lc3JPN1pDRlNVSmNCd0k2OUlJc1RMUTZ6M0F3MkFDNUlkVC9HQzFvdzlkZ3d3YmpEc245MDVqY0RUZ0p3MVdzQTdibDVlNVorNFFHV2p1U2diVE04UFo0d1NVa1llMGdQa01vTWU4bExpdHU4elB0RStsREdYa1cza0Q1UERWMGk5ZVEreXJjQngraGdYUFVJOWdwUmFJdkdKUGp5blhqRTVDMS85UU15cU1iWkxVMkRMYjBTUHhnZHJJQ1VxTWxZS0lRMHVhakpKRk9OS1NKckF1UkFqc2NMYlhHMXhLWnFYVTA1L3hpdVNaQW1hN2ZmYUtBMjNReTRjUHFNVGtEc2cxeGVDdVcwQjVMZFVTUEg4dWtHcFkxV3BhZjNISk1YSVprUHJQTURsRFZiSUp4SkdVZVg4RUhuZmZPNmYxQTZ4dm1aYUZQbGNra2xTU3FXMHhoNWRBRnF5ZjJEemlsT0thVXJ5azdkZUVoQW5kU0E0dVJzNGJEZkhiWDJVN2dtU2xHWFA2K0ppV3pWNHdHVmRCajJOdjhPZ0ljRWtnVjlrSVpjaVpxdFlLWHVMdDdINUJoeDlLZHpwbkx5ZzZnbldjSk8yb1VSMlQ3Y0tIRk5sdVFIVHIvQ01XdEpHNEs0WUI3YVFsdVlrZnBwQ21WU2FwWWdmMk81WTlaZnB5Q1FWY1JvSVJ2UU9uSXY4cG1ubUxwREJNdlB0SW4wUDVRdGJISXppY0daMXdYREpVcXRCOVB0aHpCVjFIcVdSL1lSeEZ2ZHRsSnFKZmZIZjY5dngvb2E1S0FuN2l2R0hNZVN0WDlRN3BqaG9KUXZtdStRS3pGYlkyTFl2UzhSUlNnWnk2eHZGYVdaNWFqVVZkSmFzQW1jcE1rSjA2L1ZIV3hYaW9aVzBGZjZOOWQzMjlwRzV0THhKTXpwVWNvTGJQK01HdVE5dTFyaFRGZ2JCVkM2RWdqbnorK0VjdVJNbklyczlaTVdicTFVQ3hPVlBweGhHelNjaGNkbElHM0NVdkk4dnZHY0k5cXp1aDFXVUZ3emFYUnQya3FUNUZTU05lSmwrTWNpdUdYcUJ5dE11RWRucVMxOFNwcVc1SldETDZjWUZtd3BiSVJ2L0FLRWpEMmI0Y0liVFJ0UkIxcEkveGptWmF0QWh6NEM2UTAwR3JneUprSmZTVUd3TnJjZzE4VWxtNXR3eWlyYXBmZER0VkxJQzJVeHFtcjV4TGFmS05QclA0UjFldlF5N0xZTWUwbUVGNGg4VVNreXB5Qmp1NGFhRk95Z3o5enJYYyttMHpVTkVEd2tTZUhqRE95QjdzZzRVdU1Pb0JwTlZZeXN5VGdZclhRYmFIR2ZnVktKOHdTSmZWRlpzeWRJTjFxd0F2bUVYRGhXb1RKL0grTWN0V20zOVJtbFFNdWt2cHVWTk1pZzA0Y1pjb1BLR2ExK3BaNHg5S1ZsRnlRTm84WmZYQU1tcHExVXkrWlZhWEN0cVRNemxvT1B0aldDbzMxYVZXNW51TVFvSjlSdEdnQ3ZaSFd4WXVSMUZpcS9BUDNGdmZKWDZxVWlTUEhoN0k3Zlc2NmpWRDJMSFdxMkt1NXQzTDFXNjRGVE9mSFdOWnVzb2NJNVBkNjZ2TUlHNzN0NWd6VUUxOEIrYzQ4djNNVVdqN25sTzEwM3pCVzh4TExFeXNFQVQrbjFRaDhGbnFacDFMQVBmM3RqYUtJM3lNNUFVOE9jVjhOa1BZY0RUMUswWFl1S3RIY0I5M3NpVnBaTTZkYXhVOXRYYnpUeWVBQjYvbHhnMXRLbWJQd0Y3bWF0emkxdHJYL1ZNcENuSlUrTUN3dUxnMDlSVWY5UDNlYXl5aXkzdlN1VXFIaVQwaDdOYmxYVDZHMjVNdVQ3ZXZNTzJGcmEyY09QRDJDT1piQzdNSmlyQjd3MlFlUXMrcVpKVFhYaEtLcjEyZ3RpMnl2Y3pialhwdE9BcUNkdW8xRk9mT0NySHgzRnJnU1UzZVJTdHRLZHlsa3lFeld2aERHTkNPUlNZMnUxN3l6VDhTKzF0U25VeU5KMTRpR1VvVUFIVndZVGs3SmgwdHJYSWp3L09GOGxHd05xc25NOXdZdHIzblpmL0FBL25DL3h2bG9DWmh1TWxZM3FoNmJrd0NEdy9BdzVqeHVCZTI1ZFl0NjIzSkc2a3hQVDg1dzFTcktHcmk3MjBaWUtVcjh4QUlGUHpnNldoQ0tyS3VNNUZKYk1wVGxXWEVSaS8wS2tsWmZMM0Y1YXFhZlA5TXpuVW5oWFdBRmN3VWNhWGNOK21CTkEwNHdPN04wc1VkM1pzdHBKV0pIaFFhd0ZzdTlnVXVRNHBSYmFydU8wRC9DTHJkSUZ5R0QyVDIyMzZDcjIrUnRVMHJjREtkQ284NWNJZnc1RVh5UmZkd1pkQ1ZLdHJaVTJqUHBwVFFVaGxaSkFYc0xzYmwzSjMrNXo5dkNnaTFmVVJ5T1EvdzJOTG14U1JNRWorTU5VMUU3YjZEY3hXQnQxTkFsUG1DWm1uRVEzUmFTWkltUXRoWkxDa2lXMHpHa2J0YjhTRlJjNVJWd3l0QzFjTm9yUFNuNFFsa2JNT3d1Y2lHdlZLU2ZNWjhCemhXK3BpcWxnOWtlM2pjV3BmUTNPYzZ5NlFqbnBLME9oZ3JERmhrTXJkNDVTcmNIYUVjSm53aFRIamhqMXRnVS82a2ZiZTg2NkxVQnJ6cEJiMEJGdGRPSnZMWXVwcVFqY2ZIMmVNTDJ4eVFIc2F5aTVXcERsWnJVTk9TaUlIOFJFRXIxei9BR1prdFc1bEwyYUExZ09URTRZYWdJUFcxem1IeTg4bmNoZXBCSjFQMnh4K3hSeWJDeTN4amJWcWhFdEFScDA1ZElTZU9XUmFNcVhzVFpPS1daVFVLNkQ4NEpXc0I2N0FwbVBpYkFTdHhJRVUxRUcyUnVwVVlsTjlrWFFtNFROQlVRZFRTY3RDSUVucUhRNXNCaTdHeUtBcWhvZEIrY09ZSHFVd3F5eFpldEZNdEdaTS93QW82bFhLQk5NaWpFM2pObWw3WkpDZ1FEWDhvS3NjbFFDam1FdTMzbHVKYm50VnUrcXM5SXhmRVh4SmE3eDZ5Q2Jaenkrb0FpVTlla0szeGZRbkZtSnpITmJTKzRKSDNoN2F4eXV4MVhkbE5Sb1ZseGFPcmFWY01KbUUwbnB3UEgyUTExZWsrT3hST1JrTGhPTERLdFJ3bi9sRUV5OVZwTWdIcXpqakQ2a2hVbENockhDelliTEp2b1F6cXphSkJUNjVUMHIvQUJoM3I0aUYxak1vYnFZYU01UjM4Q1NSQzNlWnZuNU5vVFBlT1ovS09oV3lnbTVOWTdiK0Z0bFh0MmpZcEhHWE9aUExsRWJSaTJ4WG95Q25uL2hiVXo1Q2ZNeUdrQmFrQ2FRTjRRSmIrSkNhODVlMk9wSjFlSmplend0MEMwM2tGSjBuemtQd2lHWHB1RkdFdFVPQTNVcXJRcFUvK3lZeXlpSGs3aTZ1WEF5RlRRbFcyWFNBWEpHc0dXMXhhV215U256S0UvYWFtRm51RXFpdXZMT1NpU05PbjBwR0xCTmlSWTJMandHMGZaQTFvelBMd1FzaWh5M1VVSzFCL2hET055WnM1RGJ0ZENWbHZlSnprUHdqb1k2d0RObU96KzE3ZTlhVTg0MEZCQ2Q0b09BbjlzZERFNE1XVW1idUc3WnhOdzB6Ym5ab2sxNUovTVJ1OXdmQUJlNGNqOFZhdUljVk1xVFQ2b1Z2Y3ZpTGJHV0RhcmxLZ25sOThENUtBaVVCWmNXN1hwQkRpYWVIMOKApik7fVxyXG4udHZfdmlkZW97aGVpZ2h0OiAzMHZoO3dpZHRoOiAxMDAlOyBvYmplY3QtZml0OiBjb3Zlcjt9XHJcblxyXG4uaWIge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrIWltcG9ydGFudDtcclxuXHJcbn1cclxuLnc2MHtcclxuICAgIHdpZHRoOiA2MCUhaW1wb3J0YW50O1xyXG59XHJcbi53NDB7XHJcbiAgICB3aWR0aDogNDAlIWltcG9ydGFudDtcclxufVxyXG4ubm8tbWFyZ2luIHtcclxuICAgIG1hcmdpbjogYXV0byFpbXBvcnRhbnQ7XHJcbn0iXX0= */");

/***/ }),

/***/ "dE4j":
/*!**********************************************!*\
  !*** ./src/app/cameras/cameras.component.ts ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CamerasComponent = void 0;
var http_1 = __webpack_require__(/*! @angular/common/http */ "tk/3");
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var environment_1 = __webpack_require__(/*! environments/environment */ "AytR");
var ngx_toastr_1 = __webpack_require__(/*! ngx-toastr */ "EApP");
var CamerasComponent = /** @class */ (function () {
    function CamerasComponent(http, toastr) {
        var _this = this;
        this.http = http;
        this.toastr = toastr;
        this.fileToUpload = null;
        this.cameraDetails = {
            camera_url: '',
            co_ordinates: "",
            parking_area: 0,
            id: 0
        };
        this.parkings = [];
        this.cameras = [];
        this.getCameras();
        this.getParkings();
        setTimeout(function () {
            var myLatLng = { lat: 25.2048, lng: 55.2708 };
            var marker = new google.maps.Marker({
                position: myLatLng,
                title: "Hello World!"
            });
            marker.setMap(_this.map);
            var myLatLng2 = { lat: 25.21, lng: 55.30 };
            var marker = new google.maps.Marker({
                position: myLatLng2,
                title: "Hello World!"
            });
            marker.setMap(_this.map);
        }, 2000);
    }
    CamerasComponent.prototype.ngAfterViewInit = function () {
        var myLatLng = { lat: 25.2048, lng: 55.2708 };
        this.map = new google.maps.Map(document.getElementById("googleMap"), {
            zoom: 10,
            center: myLatLng,
        });
    };
    CamerasComponent.prototype.getParkings = function () {
        var _this = this;
        var headers = new http_1.HttpHeaders();
        headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
        this.http.get(environment_1.apiEndpoint + 'car/parking_map', { headers: headers }).subscribe(function (res) {
            _this.parkings = res.data;
        });
    };
    CamerasComponent.prototype.ngOnInit = function () {
    };
    CamerasComponent.prototype.handleFileInput = function (files) {
        this.fileToUpload = files.item(0);
    };
    CamerasComponent.prototype.addCamera = function () {
        var _this = this;
        var headers = new http_1.HttpHeaders();
        headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
        if (!this.cameraDetails.camera_url) {
            this.toastr.success('Please add camera url!');
            return;
        }
        if (!this.cameraDetails.co_ordinates) {
            this.toastr.success('Please add camera coordinates!');
            return;
        }
        if (!this.cameraDetails.co_ordinates) {
            this.toastr.success('Please add camera coordinates!');
            return;
        }
        if (this.cameraDetails.id) {
            this.http.put(environment_1.apiEndpoint + 'camera/update_camera/' + this.cameraDetails.id, this.cameraDetails, { headers: headers }).subscribe(function (res) {
                _this.getCameras();
                _this.submit(_this.cameraDetails.id);
            });
            return;
        }
        if (!this.fileToUpload) {
            this.toastr.success('Please add video!');
            return;
        }
        this.http.post(environment_1.apiEndpoint + 'camera/add_camera', this.cameraDetails, { headers: headers }).subscribe(function (res) {
            _this.getCameras(true);
        });
    };
    CamerasComponent.prototype.delete = function () {
        var _this = this;
        var headers = new http_1.HttpHeaders();
        headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
        if (this.cameraDetails.id) {
            this.http.delete(environment_1.apiEndpoint + 'camera/delete_camera/' + this.cameraDetails.id, { headers: headers }).subscribe(function (res) {
                _this.getCameras();
                _this.cameraDetails = {
                    camera_url: '',
                    co_ordinates: "",
                    parking_area: 0,
                    id: 0
                };
            });
            return;
        }
    };
    CamerasComponent.prototype.getCameras = function (flag) {
        var _this = this;
        if (flag === void 0) { flag = false; }
        var headers = new http_1.HttpHeaders();
        headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
        this.http.get(environment_1.apiEndpoint + 'camera/get_cameras', { headers: headers }).subscribe(function (res) {
            _this.cameras = res.data;
            if (_this.cameras && _this.cameras.length) {
                for (var _i = 0, _a = _this.cameras; _i < _a.length; _i++) {
                    var cam = _a[_i];
                    var coordinats = cam.co_ordinates.split(',');
                    if (coordinats.length == 2) {
                        var myLatLng = { lat: Number(coordinats[0].replaceAll(' ', '')), lng: Number(coordinats[1].replaceAll(' ', '')) };
                        var marker = new google.maps.Marker({
                            position: myLatLng,
                            title: "Hello World!"
                        });
                        marker.setMap(_this.map);
                    }
                }
            }
            if (flag) {
                _this.submit(_this.cameras.at(-1).id);
                _this.cameraDetails = {
                    camera_url: '',
                    co_ordinates: "",
                    parking_area: 0,
                    id: 0
                };
            }
        });
    };
    CamerasComponent.prototype.startTest = function () {
        var _this = this;
        this.parkings.map(function (par) {
            if (par.id == 18) {
                var payload = new FormData();
                payload.append('camera_url', 'http://18.118.226.189/media/Camera_' + '1' + '.mp4');
                payload.append('placement', par.placement);
                _this.http.post('http://52.14.22.200:8080/start_process', payload).subscribe(function (res) {
                });
            }
        });
    };
    CamerasComponent.prototype.submit = function (id) {
        if (this.fileToUpload) {
            var formData = new FormData();
            var fileArray = this.fileToUpload.name.split('.');
            fileArray[0] = 'Camera_' + id;
            var fileName = fileArray.join('.');
            formData.append('video_file', this.fileToUpload, fileName);
            this.http.post(environment_1.apiEndpoint + 'upload_video', formData).subscribe(function (res) {
                console.log(res);
            });
        }
    };
    CamerasComponent.ctorParameters = function () { return [
        { type: http_1.HttpClient },
        { type: ngx_toastr_1.ToastrService }
    ]; };
    CamerasComponent = __decorate([
        core_1.Component({
            selector: 'app-cameras',
            template: __webpack_require__(/*! raw-loader!./cameras.component.html */ "aO28").default,
            styles: [__webpack_require__(/*! ./cameras.component.css */ "dDHc").default]
        }),
        __metadata("design:paramtypes", [http_1.HttpClient, ngx_toastr_1.ToastrService])
    ], CamerasComponent);
    return CamerasComponent;
}());
exports.CamerasComponent = CamerasComponent;


/***/ }),

/***/ "dL06":
/*!*********************************************************!*\
  !*** ./src/app/parking-area/parking-area.component.css ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@import url(https://fonts.googleapis.com/css?family=Roboto:400,300);\r\n* {\r\n  box-sizing: border-box;\r\n}\r\nhtml {\r\n  height: 100%;\r\n}\r\nbody {\r\n  height: 100%;\r\n  font-family: Roboto, san-serif;\r\n  font-weight: 300;\r\n  background-color: #f5f6f7;\r\n}\r\n/* Form element setup */\r\nform {\r\n    \r\n  /* position: absolute; */\r\n  top: 0;\r\n  /* left: 50%; */\r\n  width: 300px;\r\n  /* transform: translateX(-50%); */\r\n  margin: 2rem 0;\r\n  z-index: 1;\r\n}\r\nfieldset {\r\n  margin: 0 0 1rem 0;\r\n  padding: 0;\r\n  border: none;\r\n}\r\nlegend {\r\n  font-weight: 400;\r\n}\r\nlegend,\r\nlabel {\r\n  display: inline-block;\r\n  margin-bottom: 0.5rem;\r\n}\r\ninput[type=text],\r\ntextarea,\r\nselect {\r\n  display: block;\r\n  padding: 0.5rem;\r\n  width: 100%;\r\n  background-color: white;\r\n  border-radius: 0.25rem;\r\n  border: 1px solid #e5e5e5;\r\n  outline: none;\r\n  /* List some properties that might change */\r\n  transition-property: none;\r\n  transition-duration: none;\r\n}\r\ninput[type=text]:focus,\r\ntextarea:focus,\r\nselect:focus {\r\n  border-color: #ef7ead;\r\n}\r\ntextarea {\r\n  max-width: 300px;\r\n  height: 100px;\r\n}\r\ninput[type=text],\r\nselect {\r\n  height: 34px;\r\n}\r\nselect {\r\n  font-size: 0.875rem;\r\n}\r\ninput[type=checkbox],\r\ninput[type=radio] {\r\n  position: relative;\r\n  top: 5px;\r\n  width: 22px;\r\n  height: 22px;\r\n  margin: 0 0.5rem;\r\n  background-color: white;\r\n  border: 1px solid #e5e5e5;\r\n  outline: none;\r\n  -moz-appearance: none;\r\n  -webkit-appearance: none;\r\n  /* List some properties that might change */\r\n  transition-property: none;\r\n  transition-duration: none;\r\n}\r\ninput[type=checkbox] {\r\n  border-radius: 5px;\r\n}\r\ninput[type=checkbox]:checked {\r\n  background-color: #ef7ead;\r\n  border: none;\r\n}\r\ninput[type=checkbox]:checked:after {\r\n  display: block;\r\n  content: \"\";\r\n  height: 4px;\r\n  width: 10px;\r\n  border-bottom: 3px solid #fff;\r\n  border-left: 3px solid #fff;\r\n  transform: translate(5px, 6px) rotate(-45deg) scale(1);\r\n}\r\ninput[type=radio] {\r\n  border-radius: 50%;\r\n}\r\ninput[type=radio]:checked {\r\n  border-width: 5px;\r\n  border-color: white;\r\n  background-color: #ef7ead;\r\n}\r\nbutton {\r\n  display: block;\r\n  margin: 3em auto;\r\n  padding: 0.5rem 2rem;\r\n  font-size: 125%;\r\n  color: white;\r\n  border: none;\r\n  border-radius: 0.25rem;\r\n  background-color: #ef7ead;\r\n  outline: none;\r\n  box-shadow: 0 0.4rem 0.1rem -0.3rem rgba(0, 0, 0, 0.1);\r\n  /* We'll talk about this next */\r\n  transform: perspective(300px) scale(0.95) translateZ(0);\r\n  transform-style: preserve-3d;\r\n  /* List the properties that you're looking to transition.\r\n     Try not to use 'all' */\r\n  transition-property: none;\r\n  /* This applies to all of the above properties */\r\n  transition-duration: none;\r\n}\r\nbutton:hover {\r\n  cursor: pointer;\r\n  background-color: #ff96c8;\r\n  box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);\r\n  transform: scale(1.1) rotateX(0);\r\n}\r\nbutton:active {\r\n  background-color: #ef7ead;\r\n  transform: scale(1.05) rotateX(-10deg);\r\n}\r\n.rightBox {\r\n    border: 1px solid;\r\n    width: 100%;\r\n    min-height: 30vh;\r\n}\r\ntable { border: 1px solid #ccc; border-collapse: collapse; margin: 0; padding: 0; width: 100%; table-layout: fixed;}\r\ntable caption { font-size: 1.5em; margin: .5em 0 .75em;}\r\ntable tr { border: 1px solid #ddd; padding: .35em;}\r\ntable th,\r\n      table td { padding: .625em; text-align: center;}\r\ntable th { font-size: .85em; letter-spacing: .1em; text-transform: uppercase;}\r\n@media screen and (max-width: 600px) {\r\n        table { border: 0; }\r\n        table caption { font-size: 1.3em; }\r\n        table thead { border: none; clip: rect(0 0 0 0); height: 1px; margin: -1px; overflow: hidden; padding: 0; position: absolute; width: 1px;}\r\n        table tr { border-bottom: 3px solid #ddd; display: block; margin-bottom: .625em; }\r\n        table td { border-bottom: 1px solid #ddd; display: block; font-size: .8em; text-align: right;}\r\n        table td::before { content: attr(data-label); float: left; font-weight: bold; text-transform: uppercase; }\r\n        table td:last-child { border-bottom: 0; }\r\n      }\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhcmtpbmctYXJlYS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLG1FQUFtRTtBQUNuRTtFQUNFLHNCQUFzQjtBQUN4QjtBQUVBO0VBQ0UsWUFBWTtBQUNkO0FBRUE7RUFDRSxZQUFZO0VBQ1osOEJBQThCO0VBQzlCLGdCQUFnQjtFQUNoQix5QkFBeUI7QUFDM0I7QUFFQSx1QkFBdUI7QUFDdkI7O0VBRUUsd0JBQXdCO0VBQ3hCLE1BQU07RUFDTixlQUFlO0VBQ2YsWUFBWTtFQUNaLGlDQUFpQztFQUNqQyxjQUFjO0VBQ2QsVUFBVTtBQUNaO0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLFlBQVk7QUFDZDtBQUVBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBRUE7O0VBRUUscUJBQXFCO0VBQ3JCLHFCQUFxQjtBQUN2QjtBQUVBOzs7RUFHRSxjQUFjO0VBQ2QsZUFBZTtFQUNmLFdBQVc7RUFDWCx1QkFBdUI7RUFDdkIsc0JBQXNCO0VBQ3RCLHlCQUF5QjtFQUN6QixhQUFhO0VBQ2IsMkNBQTJDO0VBQzNDLHlCQUF5QjtFQUN6Qix5QkFBeUI7QUFDM0I7QUFDQTs7O0VBR0UscUJBQXFCO0FBQ3ZCO0FBRUE7RUFDRSxnQkFBZ0I7RUFDaEIsYUFBYTtBQUNmO0FBRUE7O0VBRUUsWUFBWTtBQUNkO0FBRUE7RUFDRSxtQkFBbUI7QUFDckI7QUFFQTs7RUFFRSxrQkFBa0I7RUFDbEIsUUFBUTtFQUNSLFdBQVc7RUFDWCxZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLHVCQUF1QjtFQUN2Qix5QkFBeUI7RUFDekIsYUFBYTtFQUNiLHFCQUFxQjtFQUNyQix3QkFBd0I7RUFDeEIsMkNBQTJDO0VBQzNDLHlCQUF5QjtFQUN6Qix5QkFBeUI7QUFDM0I7QUFFQTtFQUNFLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UseUJBQXlCO0VBQ3pCLFlBQVk7QUFDZDtBQUNBO0VBQ0UsY0FBYztFQUNkLFdBQVc7RUFDWCxXQUFXO0VBQ1gsV0FBVztFQUNYLDZCQUE2QjtFQUM3QiwyQkFBMkI7RUFDM0Isc0RBQXNEO0FBQ3hEO0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIseUJBQXlCO0FBQzNCO0FBRUE7RUFDRSxjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBQ2YsWUFBWTtFQUNaLFlBQVk7RUFDWixzQkFBc0I7RUFDdEIseUJBQXlCO0VBQ3pCLGFBQWE7RUFDYixzREFBc0Q7RUFDdEQsK0JBQStCO0VBQy9CLHVEQUF1RDtFQUN2RCw0QkFBNEI7RUFDNUI7MkJBQ3lCO0VBQ3pCLHlCQUF5QjtFQUN6QixnREFBZ0Q7RUFDaEQseUJBQXlCO0FBQzNCO0FBQ0E7RUFDRSxlQUFlO0VBQ2YseUJBQXlCO0VBQ3pCLG9DQUFvQztFQUNwQyxnQ0FBZ0M7QUFDbEM7QUFDQTtFQUNFLHlCQUF5QjtFQUN6QixzQ0FBc0M7QUFDeEM7QUFHQTtJQUNJLGlCQUFpQjtJQUNqQixXQUFXO0lBQ1gsZ0JBQWdCO0FBQ3BCO0FBR0EsUUFBUSxzQkFBc0IsRUFBRSx5QkFBeUIsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxtQkFBbUIsQ0FBQztBQUM3RyxnQkFBZ0IsZ0JBQWdCLEVBQUUsb0JBQW9CLENBQUM7QUFDdkQsV0FBVyxzQkFBc0IsRUFBRSxjQUFjLENBQUM7QUFDbEQ7aUJBQ1csZUFBZSxFQUFFLGtCQUFrQixDQUFDO0FBQy9DLFdBQVcsZ0JBQWdCLEVBQUUsb0JBQW9CLEVBQUUseUJBQXlCLENBQUM7QUFDN0U7UUFDRSxRQUFRLFNBQVMsRUFBRTtRQUNuQixnQkFBZ0IsZ0JBQWdCLEVBQUU7UUFDbEMsY0FBYyxZQUFZLEVBQUUsbUJBQW1CLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxnQkFBZ0IsRUFBRSxVQUFVLEVBQUUsa0JBQWtCLEVBQUUsVUFBVSxDQUFDO1FBQ3pJLFdBQVcsNkJBQTZCLEVBQUUsY0FBYyxFQUFFLHFCQUFxQixFQUFFO1FBQ2pGLFdBQVcsNkJBQTZCLEVBQUUsY0FBYyxFQUFFLGVBQWUsRUFBRSxpQkFBaUIsQ0FBQztRQUM3RixtQkFBbUIseUJBQXlCLEVBQUUsV0FBVyxFQUFFLGlCQUFpQixFQUFFLHlCQUF5QixFQUFFO1FBQ3pHLHNCQUFzQixnQkFBZ0IsRUFBRTtNQUMxQyIsImZpbGUiOiJwYXJraW5nLWFyZWEuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgdXJsKGh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1Sb2JvdG86NDAwLDMwMCk7XHJcbioge1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbn1cclxuXHJcbmh0bWwge1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG5cclxuYm9keSB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIGZvbnQtZmFtaWx5OiBSb2JvdG8sIHNhbi1zZXJpZjtcclxuICBmb250LXdlaWdodDogMzAwO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmNWY2Zjc7XHJcbn1cclxuXHJcbi8qIEZvcm0gZWxlbWVudCBzZXR1cCAqL1xyXG5mb3JtIHtcclxuICAgIFxyXG4gIC8qIHBvc2l0aW9uOiBhYnNvbHV0ZTsgKi9cclxuICB0b3A6IDA7XHJcbiAgLyogbGVmdDogNTAlOyAqL1xyXG4gIHdpZHRoOiAzMDBweDtcclxuICAvKiB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSk7ICovXHJcbiAgbWFyZ2luOiAycmVtIDA7XHJcbiAgei1pbmRleDogMTtcclxufVxyXG5cclxuZmllbGRzZXQge1xyXG4gIG1hcmdpbjogMCAwIDFyZW0gMDtcclxuICBwYWRkaW5nOiAwO1xyXG4gIGJvcmRlcjogbm9uZTtcclxufVxyXG5cclxubGVnZW5kIHtcclxuICBmb250LXdlaWdodDogNDAwO1xyXG59XHJcblxyXG5sZWdlbmQsXHJcbmxhYmVsIHtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPXRleHRdLFxyXG50ZXh0YXJlYSxcclxuc2VsZWN0IHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwYWRkaW5nOiAwLjVyZW07XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgYm9yZGVyLXJhZGl1czogMC4yNXJlbTtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZTVlNWU1O1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbiAgLyogTGlzdCBzb21lIHByb3BlcnRpZXMgdGhhdCBtaWdodCBjaGFuZ2UgKi9cclxuICB0cmFuc2l0aW9uLXByb3BlcnR5OiBub25lO1xyXG4gIHRyYW5zaXRpb24tZHVyYXRpb246IG5vbmU7XHJcbn1cclxuaW5wdXRbdHlwZT10ZXh0XTpmb2N1cyxcclxudGV4dGFyZWE6Zm9jdXMsXHJcbnNlbGVjdDpmb2N1cyB7XHJcbiAgYm9yZGVyLWNvbG9yOiAjZWY3ZWFkO1xyXG59XHJcblxyXG50ZXh0YXJlYSB7XHJcbiAgbWF4LXdpZHRoOiAzMDBweDtcclxuICBoZWlnaHQ6IDEwMHB4O1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPXRleHRdLFxyXG5zZWxlY3Qge1xyXG4gIGhlaWdodDogMzRweDtcclxufVxyXG5cclxuc2VsZWN0IHtcclxuICBmb250LXNpemU6IDAuODc1cmVtO1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPWNoZWNrYm94XSxcclxuaW5wdXRbdHlwZT1yYWRpb10ge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB0b3A6IDVweDtcclxuICB3aWR0aDogMjJweDtcclxuICBoZWlnaHQ6IDIycHg7XHJcbiAgbWFyZ2luOiAwIDAuNXJlbTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZTVlNWU1O1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbiAgLW1vei1hcHBlYXJhbmNlOiBub25lO1xyXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcclxuICAvKiBMaXN0IHNvbWUgcHJvcGVydGllcyB0aGF0IG1pZ2h0IGNoYW5nZSAqL1xyXG4gIHRyYW5zaXRpb24tcHJvcGVydHk6IG5vbmU7XHJcbiAgdHJhbnNpdGlvbi1kdXJhdGlvbjogbm9uZTtcclxufVxyXG5cclxuaW5wdXRbdHlwZT1jaGVja2JveF0ge1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxufVxyXG5pbnB1dFt0eXBlPWNoZWNrYm94XTpjaGVja2VkIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWY3ZWFkO1xyXG4gIGJvcmRlcjogbm9uZTtcclxufVxyXG5pbnB1dFt0eXBlPWNoZWNrYm94XTpjaGVja2VkOmFmdGVyIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBjb250ZW50OiBcIlwiO1xyXG4gIGhlaWdodDogNHB4O1xyXG4gIHdpZHRoOiAxMHB4O1xyXG4gIGJvcmRlci1ib3R0b206IDNweCBzb2xpZCAjZmZmO1xyXG4gIGJvcmRlci1sZWZ0OiAzcHggc29saWQgI2ZmZjtcclxuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSg1cHgsIDZweCkgcm90YXRlKC00NWRlZykgc2NhbGUoMSk7XHJcbn1cclxuXHJcbmlucHV0W3R5cGU9cmFkaW9dIHtcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbn1cclxuaW5wdXRbdHlwZT1yYWRpb106Y2hlY2tlZCB7XHJcbiAgYm9yZGVyLXdpZHRoOiA1cHg7XHJcbiAgYm9yZGVyLWNvbG9yOiB3aGl0ZTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWY3ZWFkO1xyXG59XHJcblxyXG5idXR0b24ge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbjogM2VtIGF1dG87XHJcbiAgcGFkZGluZzogMC41cmVtIDJyZW07XHJcbiAgZm9udC1zaXplOiAxMjUlO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgYm9yZGVyLXJhZGl1czogMC4yNXJlbTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWY3ZWFkO1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbiAgYm94LXNoYWRvdzogMCAwLjRyZW0gMC4xcmVtIC0wLjNyZW0gcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG4gIC8qIFdlJ2xsIHRhbGsgYWJvdXQgdGhpcyBuZXh0ICovXHJcbiAgdHJhbnNmb3JtOiBwZXJzcGVjdGl2ZSgzMDBweCkgc2NhbGUoMC45NSkgdHJhbnNsYXRlWigwKTtcclxuICB0cmFuc2Zvcm0tc3R5bGU6IHByZXNlcnZlLTNkO1xyXG4gIC8qIExpc3QgdGhlIHByb3BlcnRpZXMgdGhhdCB5b3UncmUgbG9va2luZyB0byB0cmFuc2l0aW9uLlxyXG4gICAgIFRyeSBub3QgdG8gdXNlICdhbGwnICovXHJcbiAgdHJhbnNpdGlvbi1wcm9wZXJ0eTogbm9uZTtcclxuICAvKiBUaGlzIGFwcGxpZXMgdG8gYWxsIG9mIHRoZSBhYm92ZSBwcm9wZXJ0aWVzICovXHJcbiAgdHJhbnNpdGlvbi1kdXJhdGlvbjogbm9uZTtcclxufVxyXG5idXR0b246aG92ZXIge1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmY5NmM4O1xyXG4gIGJveC1zaGFkb3c6IDAgMCAwIDAgcmdiYSgwLCAwLCAwLCAwKTtcclxuICB0cmFuc2Zvcm06IHNjYWxlKDEuMSkgcm90YXRlWCgwKTtcclxufVxyXG5idXR0b246YWN0aXZlIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWY3ZWFkO1xyXG4gIHRyYW5zZm9ybTogc2NhbGUoMS4wNSkgcm90YXRlWCgtMTBkZWcpO1xyXG59XHJcblxyXG5cclxuLnJpZ2h0Qm94IHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtaW4taGVpZ2h0OiAzMHZoO1xyXG59XHJcblxyXG5cclxudGFibGUgeyBib3JkZXI6IDFweCBzb2xpZCAjY2NjOyBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlOyBtYXJnaW46IDA7IHBhZGRpbmc6IDA7IHdpZHRoOiAxMDAlOyB0YWJsZS1sYXlvdXQ6IGZpeGVkO31cclxuICAgICAgdGFibGUgY2FwdGlvbiB7IGZvbnQtc2l6ZTogMS41ZW07IG1hcmdpbjogLjVlbSAwIC43NWVtO31cclxuICAgICAgdGFibGUgdHIgeyBib3JkZXI6IDFweCBzb2xpZCAjZGRkOyBwYWRkaW5nOiAuMzVlbTt9XHJcbiAgICAgIHRhYmxlIHRoLFxyXG4gICAgICB0YWJsZSB0ZCB7IHBhZGRpbmc6IC42MjVlbTsgdGV4dC1hbGlnbjogY2VudGVyO31cclxuICAgICAgdGFibGUgdGggeyBmb250LXNpemU6IC44NWVtOyBsZXR0ZXItc3BhY2luZzogLjFlbTsgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTt9XHJcbiAgICAgIEBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XHJcbiAgICAgICAgdGFibGUgeyBib3JkZXI6IDA7IH1cclxuICAgICAgICB0YWJsZSBjYXB0aW9uIHsgZm9udC1zaXplOiAxLjNlbTsgfVxyXG4gICAgICAgIHRhYmxlIHRoZWFkIHsgYm9yZGVyOiBub25lOyBjbGlwOiByZWN0KDAgMCAwIDApOyBoZWlnaHQ6IDFweDsgbWFyZ2luOiAtMXB4OyBvdmVyZmxvdzogaGlkZGVuOyBwYWRkaW5nOiAwOyBwb3NpdGlvbjogYWJzb2x1dGU7IHdpZHRoOiAxcHg7fVxyXG4gICAgICAgIHRhYmxlIHRyIHsgYm9yZGVyLWJvdHRvbTogM3B4IHNvbGlkICNkZGQ7IGRpc3BsYXk6IGJsb2NrOyBtYXJnaW4tYm90dG9tOiAuNjI1ZW07IH1cclxuICAgICAgICB0YWJsZSB0ZCB7IGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZGRkOyBkaXNwbGF5OiBibG9jazsgZm9udC1zaXplOiAuOGVtOyB0ZXh0LWFsaWduOiByaWdodDt9XHJcbiAgICAgICAgdGFibGUgdGQ6OmJlZm9yZSB7IGNvbnRlbnQ6IGF0dHIoZGF0YS1sYWJlbCk7IGZsb2F0OiBsZWZ0OyBmb250LXdlaWdodDogYm9sZDsgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTsgfVxyXG4gICAgICAgIHRhYmxlIHRkOmxhc3QtY2hpbGQgeyBib3JkZXItYm90dG9tOiAwOyB9XHJcbiAgICAgIH1cclxuXHJcbiJdfQ== */");

/***/ }),

/***/ "dRrA":
/*!********************************************!*\
  !*** ./src/app/resize/resize.component.ts ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResizableDraggableComponent = void 0;
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var ResizableDraggableComponent = /** @class */ (function () {
    function ResizableDraggableComponent() {
        this.status = 0 /* OFF */;
    }
    ResizableDraggableComponent.prototype.ngOnInit = function () { };
    ResizableDraggableComponent.prototype.ngAfterViewInit = function () {
        this.loadBox();
        this.loadContainer();
    };
    ResizableDraggableComponent.prototype.loadBox = function () {
        var _a = this.box.nativeElement.getBoundingClientRect(), left = _a.left, top = _a.top;
        this.boxPosition = { left: left, top: top };
    };
    ResizableDraggableComponent.prototype.loadContainer = function () {
        var left = this.boxPosition.left - this.left;
        var top = this.boxPosition.top - this.top;
        var right = left + 600;
        var bottom = top + 450;
        this.containerPos = { left: left, top: top, right: right, bottom: bottom };
    };
    ResizableDraggableComponent.prototype.setStatus = function (event, status) {
        if (status === 1)
            event.stopPropagation();
        else if (status === 2)
            this.mouseClick = { x: event.clientX, y: event.clientY, left: this.left, top: this.top };
        else
            this.loadBox();
        this.status = status;
    };
    ResizableDraggableComponent.prototype.onMouseMove = function (event) {
        this.mouse = { x: event.clientX, y: event.clientY };
        if (this.status === 1 /* RESIZE */)
            this.resize();
        else if (this.status === 2 /* MOVE */)
            this.move();
    };
    ResizableDraggableComponent.prototype.resize = function () {
        if (this.resizeCondMeet()) {
            this.width = Number(this.mouse.x > this.boxPosition.left) ? this.mouse.x - this.boxPosition.left : 0;
            this.height = Number(this.mouse.y > this.boxPosition.top) ? this.mouse.y - this.boxPosition.top : 0;
        }
    };
    ResizableDraggableComponent.prototype.resizeCondMeet = function () {
        return (this.mouse.x < this.containerPos.right && this.mouse.y < this.containerPos.bottom);
    };
    ResizableDraggableComponent.prototype.move = function () {
        if (this.moveCondMeet()) {
            this.left = this.mouseClick.left + (this.mouse.x - this.mouseClick.x);
            this.top = this.mouseClick.top + (this.mouse.y - this.mouseClick.y);
        }
    };
    ResizableDraggableComponent.prototype.moveCondMeet = function () {
        var offsetLeft = this.mouseClick.x - this.boxPosition.left;
        var offsetRight = this.width - offsetLeft;
        var offsetTop = this.mouseClick.y - this.boxPosition.top;
        var offsetBottom = this.height - offsetTop;
        // return (
        //   this.mouse.x > this.containerPos.left + offsetLeft && 
        //   this.mouse.x < this.containerPos.right - offsetRight &&
        //   this.mouse.y > this.containerPos.top + offsetTop &&
        //   this.mouse.y < this.containerPos.bottom - offsetBottom
        //   );
        return true;
    };
    ResizableDraggableComponent.propDecorators = {
        width: [{ type: core_1.Input, args: ['width',] }],
        height: [{ type: core_1.Input, args: ['height',] }],
        left: [{ type: core_1.Input, args: ['left',] }],
        top: [{ type: core_1.Input, args: ['top',] }],
        box: [{ type: core_1.ViewChild, args: ["box",] }],
        onMouseMove: [{ type: core_1.HostListener, args: ['window:mousemove', ['$event'],] }]
    };
    ResizableDraggableComponent = __decorate([
        core_1.Component({
            selector: 'app-resizable-draggable',
            template: __webpack_require__(/*! raw-loader!./resize.component.html */ "Sz8D").default,
            styles: [__webpack_require__(/*! ./resize.component.css */ "lt5q").default]
        })
    ], ResizableDraggableComponent);
    return ResizableDraggableComponent;
}());
exports.ResizableDraggableComponent = ResizableDraggableComponent;


/***/ }),

/***/ "eouj":
/*!*****************************************!*\
  !*** ./src/app/test/test.component.css ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@import url(https://fonts.googleapis.com/css?family=Roboto:400,300);\r\n* {\r\n  box-sizing: border-box;\r\n}\r\nhtml {\r\n  height: 100%;\r\n}\r\nbody {\r\n  height: 100%;\r\n  font-family: Roboto, san-serif;\r\n  font-weight: 300;\r\n  background-color: #f5f6f7;\r\n}\r\n/* Form element setup */\r\nform {\r\n    \r\n  /* position: absolute; */\r\n  top: 0;\r\n  /* left: 50%; */\r\n  width: 300px;\r\n  /* transform: translateX(-50%); */\r\n  margin: 2rem 0;\r\n  z-index: 1;\r\n}\r\nfieldset {\r\n  margin: 0 0 1rem 0;\r\n  padding: 0;\r\n  border: none;\r\n}\r\nlegend {\r\n  font-weight: 400;\r\n}\r\nlegend,\r\nlabel {\r\n  display: inline-block;\r\n  margin-bottom: 0.5rem;\r\n}\r\ninput[type=text],\r\ntextarea,\r\nselect {\r\n  display: block;\r\n  padding: 0.5rem;\r\n  width: 100%;\r\n  background-color: white;\r\n  border-radius: 0.25rem;\r\n  border: 1px solid #e5e5e5;\r\n  outline: none;\r\n  /* List some properties that might change */\r\n  transition-property: none;\r\n  transition-duration: none;\r\n}\r\ninput[type=text]:focus,\r\ntextarea:focus,\r\nselect:focus {\r\n  border-color: #ef7ead;\r\n}\r\ntextarea {\r\n  max-width: 300px;\r\n  height: 100px;\r\n}\r\ninput[type=text],\r\nselect {\r\n  height: 34px;\r\n}\r\nselect {\r\n  font-size: 0.875rem;\r\n}\r\ninput[type=checkbox],\r\ninput[type=radio] {\r\n  position: relative;\r\n  top: 5px;\r\n  width: 22px;\r\n  height: 22px;\r\n  margin: 0 0.5rem;\r\n  background-color: white;\r\n  border: 1px solid #e5e5e5;\r\n  outline: none;\r\n  -moz-appearance: none;\r\n  -webkit-appearance: none;\r\n  /* List some properties that might change */\r\n  transition-property: none;\r\n  transition-duration: none;\r\n}\r\ninput[type=checkbox] {\r\n  border-radius: 5px;\r\n}\r\ninput[type=checkbox]:checked {\r\n  background-color: #ef7ead;\r\n  border: none;\r\n}\r\ninput[type=checkbox]:checked:after {\r\n  display: block;\r\n  content: \"\";\r\n  height: 4px;\r\n  width: 10px;\r\n  border-bottom: 3px solid #fff;\r\n  border-left: 3px solid #fff;\r\n  transform: translate(5px, 6px) rotate(-45deg) scale(1);\r\n}\r\ninput[type=radio] {\r\n  border-radius: 50%;\r\n}\r\ninput[type=radio]:checked {\r\n  border-width: 5px;\r\n  border-color: white;\r\n  background-color: #ef7ead;\r\n}\r\nbutton {\r\n  display: block;\r\n  margin: 3em auto;\r\n  padding: 0.5rem 2rem;\r\n  font-size: 125%;\r\n  color: white;\r\n  border: none;\r\n  border-radius: 0.25rem;\r\n  background-color: #ef7ead;\r\n  outline: none;\r\n  box-shadow: 0 0.4rem 0.1rem -0.3rem rgba(0, 0, 0, 0.1);\r\n  /* We'll talk about this next */\r\n  transform: perspective(300px) scale(0.95) translateZ(0);\r\n  transform-style: preserve-3d;\r\n  /* List the properties that you're looking to transition.\r\n     Try not to use 'all' */\r\n  transition-property: none;\r\n  /* This applies to all of the above properties */\r\n  transition-duration: none;\r\n}\r\nbutton:hover {\r\n  cursor: pointer;\r\n  background-color: #ff96c8;\r\n  box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);\r\n  transform: scale(1.1) rotateX(0);\r\n}\r\nbutton:active {\r\n  background-color: #ef7ead;\r\n  transform: scale(1.05) rotateX(-10deg);\r\n}\r\n.rightBox {\r\n    border: 1px solid;\r\n    width: 100%;\r\n    height: 30vh;\r\n}\r\ntable { border: 1px solid #ccc; border-collapse: collapse; margin: 0; padding: 0; width: 100%; table-layout: fixed;}\r\ntable caption { font-size: 1.5em; margin: .5em 0 .75em;}\r\ntable tr { border: 1px solid #ddd; padding: .35em;}\r\ntable th,\r\n      table td { padding: .625em; text-align: center;}\r\ntable th { font-size: .85em; letter-spacing: .1em; text-transform: uppercase;}\r\n@media screen and (max-width: 600px) {\r\n        table { border: 0; }\r\n        table caption { font-size: 1.3em; }\r\n        table thead { border: none; clip: rect(0 0 0 0); height: 1px; margin: -1px; overflow: hidden; padding: 0; position: absolute; width: 1px;}\r\n        table tr { border-bottom: 3px solid #ddd; display: block; margin-bottom: .625em; }\r\n        table td { border-bottom: 1px solid #ddd; display: block; font-size: .8em; text-align: right;}\r\n        table td::before { content: attr(data-label); float: left; font-weight: bold; text-transform: uppercase; }\r\n        table td:last-child { border-bottom: 0; }\r\n      }\r\nbody{ margin:0; padding:0;}\r\n.play-bt{width:50px; height:50px; border-radius:50%; padding:5px; background:#000 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA9dJREFUeNrMmmtIFFEUx8dVLHsSlqVhqRioJFL4ocjIyEdi9iALAoMgCulFEESBVEQfCvtoUPRAksCgsKgkiqRAyzJ6oCGKabgklYb5SDO16X/YM7KKj507d+7OgR/rLs7d+587995z/ncDdF3XJMZiEA3CwDJ+nQGGwW/gBs2gHTSAbllfHGTxehdIABvBWpAEFoFpU1zXy6JqwHPwDLRa6UiA4IgEg61gN8jg91aCRqgMlIBKoRZIiEk2gyrdnugHt0CS2X6Z+edYUKKriU5wCoTIFpINmnX1UQHiZQk5CgZ0/4UbpFoVck53RvwEOaJCCnRnxS+QPlF/J1p+aVkt5n3CSfEVZIFaX/aRFbxBzdOcGdUgc2xWMPaO0458SUDEP4VCVoGC8VIM7zgEVgs0TvnTEfBFkZjDLGjcnT0CtAlOxHpuIwoUg78KJn+592T3HpGDIFzwDgWCEB6RPSCbE0I7I4PnyqhHayGvVLLiKUgDJ8EPm4TQzdtvaDCEpINIyV9Eq8p5sAHctUlMGtc/I0LybHwE6kAu2AHqJbc9B2yjVxISAZYrWGnugFRQyNWirKARjyEhiSxGRdB8OQ7Wg8eS2owHa1xcWwco3p1pRcsB+aDFYlu0UCW6FD1W48UQuALWgav8XiSm00JFQmL9nDu5eRlNE67XebKHOSQZfMEb3Anwzaz14JLggMiMPnABpIDroN+ML+XE+MzZwbAZIX0OE7ESlINSMMuMkDaHCKAa6Ayo4CrQtOXZ4AAR29k6PQ3mmp3o5FQGcS7kr4jlzudZXCDcJKQJDPhgPMuMEK5/jrHpbSXI/H5Dj9ZH3pRURTqvSIUSRGjsqNS4uG54pUAAnZ0UgYeU5Els9wGNirGP3LRRgFHJVfLjJHMDJp/rCU0NQ8hL8M4GEcl8xyg5jLKh/TKjlHZ5zfzLEr8gFJzVPEZflk0j3T2qz16WCp1FvBe0Zhq92skFnxTYQUWTmdhbwJBAo3UgAZQqPGqInEwIecE3BBruAR0Knfm9vrjxoZwu+KtynCqugX1jP5zoWCGZM9AFDhNRpXlczC5f65G3msf67HKQiFrOybrMFlblfGG7A0R8ADu1ydx+Hw5DU0CTH4/cHoFwWcfT0eC+YgGDfBjr01m7mR8MBIJ80KpARDXItOuXDwZLwEXw3QYBlBEcALPN9kv0RzUaJ4G7wCbNc1wnarv2sqd1G9wDPSKNWBFixEwQx047mdNLwXw2E4K9BNKB6R/QwTSwkf2aq9RhK52QIWTEtgQxmufgJY7/JjsniA2CQdDJAhrZvG7hzy3HfwEGAFdwoJHzsBGDAAAAAElFTkSuQmCC) no-repeat 5px;position: absolute;bottom:25px;right: 175px; margin-left:-25px; margin-top:-40px;z-index: 99; cursor:pointer;}\r\n.pause-bt{width:50px; height:50px; border-radius:50%; padding:5px; background:#000 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAw1JREFUeNrcmttLVEEcx2cP2pVSowtKFBQRSpoYW1QEQUVRIXSBHpIIumBPIQU+9Bd0gSKhrJ6jiOhCgT4EZQ9dKShJKboRhEWBaCm5atP3187AYZhd98yc457ZL3xwPc6657NzzplrgnPOQsoikBRUgnJQBiaDBEiBPvANfAQvwTPQCf7YfnjCUmQe2Al2gDowyeB/vAVt4IoQMwuJGFAJWkEvDy+joB1sMjmnoG8oBSfAAI82t0B1VCLrQBcfv/SDprBFjoIUz0+uiivBWqSF5z9PQEW28xzrqXURHGDxyGuwGXzV/dHL8sZTMZKg1IAboCSICAkcYfHLcnBJNLBjNohLwSMwhcU3TeBMNpFi0AFWsnhnQHSFujNdWvsdkKBMBScz1Uip6MDNZe5kA7hHL4p8B/daSAyC75pvbbZy7Df4oRybBmYafm6zFJENygTL7sddUKywS1PusqbcQcuOZh05yHtkjRhDmGYUDGswLZdr6Pwb/Df7dttxTY5tVK7lgmQLmOiJR+5a5m5oZLqERBaIX1wN1fIKEqkSteJylnmO14bMQk9MILieOSQyowBEpnuGUzhxy//HLy8AEe6JLrHrGfQ0nT0X00sinwtApIdE3hWASDeJvAG/HBd5TiI9LD1n5GqGwFP5+G13WOQF+CTHAjfBiOXASvdNqRnJsVyQXKfKkGN2uk/ui8G8SWaBVcqxGk25ck25KguJfnBNnUXZCu44dlm1gkOqCF1mD8FqRySoR1IL3qvj5b/gmEN9rxYpodaIzHnQGHMJWkBN+ts/nQhN2z9mdtNDUYamj9aL24D57ws1tBbeEOPWvlmVyCRCocX8PZZtSxQ5C07rRyTZ1w93g2Eej1ywXQzdBvryLHE8rOXpJOjMgwCtte8Le8NAGTg3jhIPQG1UWzjkDoiOCAU+gEZQFOVeFD/1oA0MhSTwChwWNR/4fGy3OVGqQT3YKHq8JTm+LyWG2bT4elv8TJmeRBgi/lSIbvliMJ+mMll6CY7aK9pc9hN8EX2kLpbegBZKW/VPgAEAGYXv2/tP1L0AAAAASUVORK5CYII=) no-repeat 5px;position: absolute;bottom:25px;right: 175px; margin-left:-25px; margin-top:-40px;z-index: 99; cursor:pointer;}\r\n.stop-bt{width:50px; height:50px; border-radius:50%; padding:5px; background:#000 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAxxJREFUeNrcmktoFDEYxzPDamuxKopdaFVQEV+giFVaFbRWRER8IbRFKipV0KMHbx68eVVQRPSyF8WLFPGBVMRnS2tPolgoimgVWl91i7Dt1vj/2AyM407nsUk32T/8LrvzyH+SfEm+xOKcM0maBhaADWAxqAFzQSWwQQZ8B5/AEOgEfeCLjJcnJNxfD5rAZrAElEW4/yd4Ae6B2+BD7JJQjcSgAhwFnVyevoGLYGWcMsUxsR/0cnVKC0MLVRmpASk+efoIWmUbaQD9vDi6JppywUboq4zw4qoDJAsxchyMcT3UBar9ympNMI60ghTTS8/ALjDs/cPPyEYR2yuZfroFmsGo+0c7z4U0Gl/V1ARpHzgVpkaugGNMb42ARtDtZ4T+vAumBjyIqpUrLOgUn9bi1iOwHWS9Rqjwj0FdwAPGxQP6FJpoB6tCXNsCbngnjbtDmHBEM9YBhTUyFvK606LzZ2xXp2+L+NVUiVqGFfLaNWCbO2otB1uZmTrsNrJX8VdWqQaQtEU/aWTmag61JjJSBdYys1VLRhaB6YYbqScj60IMPrqrigzMZOZrNhmZVQJGKsjIjBIwkrBZiYiM/CoBH1k737LRQP22RT7WdP0gIz3gj+FGPpORdyBtuJEnZIRS/C8NN/LKFkvXhwab+Erld8aR9gjLS91ElTDkGHkLOgw1knKvEClqXY5wc0ZhwUYjRNEepwLcWRRKkT5nuXRp0GxghXhhQrIJLpIPYddH50Q5/kvQ0T7gAxacoBtXOPZYIT8Q9Y0dLE+CztElcELzfpEWSYdedzPx6gx4o7mRs24TfjVCqhN9RsdF101w0GlSE9UIqQucZJ49CA1Eies2rwkWkHS4znLbC7qYoYja5DcvDFoh0mBzRIPF132wR8wLfQJ3uO3pTeB1kTZB6fBAmcwDA0mx552dJAPvQYvKIxw7QbdCA8PgPJiv+iwKUQ4OgacS9+EHwQWwLE6ZLAnntWrBAbAFrAblEeZVgyLU05h1h+XOcsWb10g8eEbzs3lgPVjKcun+avbvwTPK2AyI6EPhtF+YKVh/BRgAE4h+1tvQXHoAAAAASUVORK5CYII=) no-repeat 5px;position: absolute;bottom:25px;right: 100px; margin-left:-25px; margin-top:-40px;z-index: 99; cursor:pointer;}\r\n.mute-bt{width:50px; height:50px; border-radius:50%; padding:5px; background:#000 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA61JREFUeNrsmUlIVVEYxzM1S0sbJMvMMBQ0XKgN0IANBoVWFAVBrlpYJLqwVbWLaGVJRkW0MRpWNkohRFSYoEHDIqkoUlAbzAw1UXLg9j9wDv378j29vXevr7gf/PA70/X835nu+W6YZVmT/gcL84R4QjwhnhBPyCi2G2SDC6DZZtuNIBbcBiM+aykhDlNm/bJ7INxG221gWLc95q+ukwImg1PW79YJ4mw8o1K03+K2kOngpvWnfbIpJAd8p/atYI5bQhaCRmt0G4+QKJEuEs8474aQbNBi+baxhOwBzeCOqFdDzxgEWU4KKQDdln/zJ0RtAm1U9yKVpYkpds0pIQfAiDW2+ROijoJbov4mKq+g/B9aXFCFHLfGb2NNrSTQRfWfgkhdlgL6qawiWELUorxs2TMpZIleV/zcYtFmF5Xx/2vhjYFP9ihQDBaN88RdBlbbPKU/g3TQA7aC6yASlIIzus4U0ATSdPoByNP+ZlBLz1sL6uTJfthy3nhEzlF+D0igvhykMrX2Mul8+kBl5abNZFKX7fJ7XjX56l2qhNJX9KgpU33M136fHiFjK8EMU8nYsMtCHoIaSu8F0dr/AhqobB35nK+mX4oUMhFWSf4CsJzSdeRnmV9erx9j8SA1FIQ0go+UziX/CfnzQLL228EQTbvkUBDSD15QmkdE3VsGzb1Ji1H2DfRSvYRQEKLsFflzye/TGJup/w7QRmA2ipAQwp2aRv4QTSFzzplNaZDyI0NFiOy8sXCN3FVVnyMofyRUhKSR30W+2opjKG3WxVTawcwUnHAh6pfNofRL8pPEaHXQWomj/M5QEJIJFlP6kXiXM9YNWrU/X4+KsbZQELKP+tAjTu01YqTMtEsXApulkAiXRWTo1xJjN6iz0eJwrCd/Bfkt4L0U0uCykEKaImrnKaey7XoKGaulrTaP8p+Br3IUTuuhGs99RJ20BWBpAEKek6/uIq8pXSKmVaP2c8TUuhuMSGOsiG78zQ1xg74BhlFevmhTSmUnfAX7Ar2vR4CzQbyzR4M3VL+N6s8GHVR21YkoyqEgCTkp6hf5iCErW+VUXEsF1wYCFPKO6j7WI67yZ4F2Kqt3OtKYK4bfrpD9oFdPr1TKl8HwAjdiv+mgKYDYbyKIofR6METPqHUzGh8P7gchGp8oYsl9IMPt7yMqeFYV4PeRI6J92UR86DEcFYvUzherHdS22l9dt74hFuo33Srw1mbbnSARXBK3Se+rrifEE+IJ+YftpwADAJs4uzLWopW5AAAAAElFTkSuQmCC) no-repeat 5px;position: absolute; bottom:25px;right: 25px; margin-left:-25px; margin-top:-25px;z-index: 99; cursor:pointer;}\r\n.mute-bt.stop{background:#000 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAqpJREFUeNrsmU1IVUEUx59SZoiKiBV9WGAaqYuKsKKCQltEtI5cJEIf60IMIYPapAuh0CiKFtLmSQTRuhSEPqAWrWoXhKJPjT4oAym9/QfOxcPhznv39t5478Qc+PG4M2fuvD9z5szHLfI8L/U/WJET4oQ4IU6IExJg7WAvuA8+GOlBCTHMdW/Z3oLVJvoxOSIl4B7oYGVzoB58tyW01lMYnRTl02CnLUKawGOwI6DOGiGHQBps0tRbIeQ0hVNZFh9jQgqVNXq8cDYFKk1krXxfoEb0jhfepJCDoBc0Rez3LLgEKgohpAqkvWjGhdSBn1Seoecw/fax9930y1eJSOsDR9TcCRGV68C2PKK6gs0nla6fgFYwm6XNZcK3jbo5Ytr4iKiwvCXqx0CpZiROgEXm+xXs0YXWSgpRlIBR4XM7QMRu8IX5/CZhqaQIUVTTHozbVVa/FXwS9edzZa04hPh/dlr4ttOIvRDl/WHSb1xCFK1ggfl+pjnDLR12HYlTSIpG4Y+m7WtQbosQRVeAmHdgQ7Z2xQk8tb5Xq4IomwKZKHutuEekGcxo2t6NsteKU8gW8JH5qvCaFe27ky5kDXgpfG+ABtqH+bYETiVZyAPh95RdUrTRSu7bPNiXRCEDwmcclAmfDuEzIXfLcQvpFPWToFYzD64EpOQanZA3NMkW6Tcb+QrZDn6xuh/gQI41ZkS8b1B3HjkMakFxQC6X1gIGQeU/rhflYK24jXyVo00nqALH2JmoIGf2/QGbvSjnkYvgOTgT8WT6kBJCYyFvGhvBCGgO4Zv466AaMAyO2y7Ev+sdAudsvtfiXLPxXkvHBXbVsyJCTH5WOAoegWpWlqHQ+mbbF6tdlNEa6HkG1IF5G79YbQbPaH/Ubaof9zHUCXFCnBAnJBb7K8AA5jNpuFcQhagAAAAASUVORK5CYII=) no-repeat 5px;}\r\n.video{width: 100%; height: 100%; position:relative; background:url(data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAZAAA/+4ADkFkb2JlAGTAAAAAAf/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgCigQAAwERAAIRAQMRAf/EAL4AAAICAwEBAQEAAAAAAAAAAAYHBAUDCAkCAQoAAQACAwEBAQEAAAAAAAAAAAADBAABAgUGBwgQAAECBQIEAwQIBAMFBgQCCwECAwARIQQFMRJBUWEGcSITgTIUB/CRobHB0UII4VIjFfFicrIzJDQWgqLCQ3M1klM2CdJjsyUX4oN0k0R1JjcRAAICAQQCAQMDAwQBBAIDAQABEQIDITESBEEFE1FhInEyBoGRFKGxQiNS8MHRFTMW4XIkB//aAAwDAQACEQMRAD8A/OfbOhCwCZfxjyfGT5IFNklTxAAmJaj6/wAIohbJPnCACVAgUlzHWKIFlpj3XGVFSFJBQSCeRiItbkO1bFtcCav/ADDr4mJsHWwQ31wl62UhJBJAFPAxXIsAVf0XZKoeR+nSN1aJ5M3qFVQJwaUzFz+9bZPcJeMWB1JdjcjcrSC0BXCBklxCimol/CGqAzwFgLAjF3BCpyakpKiVAGolCmS6DVPmG2l4K3CX+P5xz8l1yNkjNkluQEwOPtjSuoILa9I9VHGShP65/jGuaIOHsHIttMqQpSQTSvHzCkXyTIHOcfS7bOEEEFOvs+qKsaqLe2U36gSVicBGahOq9atbdO1aVKBlLjwgdg0oJ8EWbVD12pxILzK6GfFBA6QKH/QnJCtyCRdXt4qcgHFEHgZAQWqgnJA8t5TDm1I3TMiYqyFsrkZfa53sesqhBFPb0gYs2kMrBOG/yjdoB7/EeIHjxg1ANtWPe3wi8bbJuFBW3+Y6UH8Y6GIJXYz3AJabUkbpkaeIhyu0A7LUs7cLt0tqCSdwB8I2tzDRUdz2yrxCVgElCRp/plB1sCS1BKyt1nHPskEKUZAcTVUWXdfiAuTtLi3QpHpLIHGX8ekYs9BJ7i0yynlpLQaUSDwlP74WuTUiWOJfdKVLbWgTBrACPcP8fbrbSlMjICU4nggTIbIRXWWnWICuEeCxqrsiU5zoB4wagJFtc4a8t8hJLDikCc1Spr49YboGoWLWP2LDi1bDSYM+FeUM121B23GBkLlvKYVnH7kp9FtUjzpPhWJbYy9jWjOYG5bubj0mluBSzUVEiOsJW/cB8gvb4K9SpRLDgqToPzjVv2/cKtiYMU8haVKQsS4fVHOzeTdS0YxjrjgXtVLn7YQe4dIYuGa+FSgnUS16QOxYcW142EjzDTT2e2OZ2lJupTZoovUgIUCUcB4xxb1YYTfc7TjB2hBMx+H3wKIGsIJYRLwvG1qbUAFGshz416R6D17ShjtS77ku0tvLdpuG6SefGPWdZ/j/AECMhYu2cuAi6UkpSrjw5/jAc37gFidmcii2tA2khSpKG3jM04+MXjakpA72XjLzJ5R51xhxtCXtwUQACkBJnQmOxgtoHrsbdYWxt0WiXHLhKFMNJkCT+mQ5Q7ka+MsTvfzxyHcFtbsjelQKdydKBI8Y8539asgYYwDB4UoWZFO3XWiTzjidRf8Ac/1C4twQ7hyYyFh6bciSTQdZCGfcOcX9B1bDo+Tj1parleXCLcLaKQVkgTUgp4AnUx8s9jRtuAGUNu8sFaNJN2zdIdD4K/KVEanmByjndWjQhcUtvYF8PlvzhBUCRwkqPSdXRAjNatuJPwYQT6hIn7Y6CZNi0tMC9Z5BD+xUk8eGoMX5IPrD48qx7dzXceFeQPhxhnGm2QxXF96aw3ITSoffKOxgTgp7DDwF9K3XMCravuh6iA+Red12q7twlCSak08DDeNOQy2+wGWmLeQhW5tYMzL64Nm/ZobruCGcsHRcq8iuP3xyP+TYdbAldNPNJMm1GXhF22MWUk3CILrxDnk01/hOAsUy1YfIsEFKClYNBzg2K6L66ZbWuNCpDdBrWkLlowssO0EXQD6nSkJ/1fgI52elm9AKxWa0PeUw7do2sIdCpciTp4wt8VjkdrDeT+7aUg3SWioBVPtnBKpoXpR1cFte4ub63AZhR68JwerR1sC0BvN2fpsa12Gf2xs6+DcRmcJauBIT85/GLOxj/aW+CyrdojctSU7TMz4VirJs1bYJ7nvCzU2Ues1M8I116/mCW4o+4Mk1duubFJM9Je2Ohm/bJu2x8wl47j1+slsqBlyPDqY4HbU6F4hnYzut5YKQxUDkmenjCWKrD3K3P3zuTaUVNFG1MtB4cDD1U0K3FSpv0lqB/mP3w9iEMpZWjg3AR2MLXH+gAN7ApKEifWOd2P3AL7lDlH0NOKBIAnL6zC6MM+Wlot9JcbSVSG6nKGsW4G25lYv1276GSiRUvbWX5x18T0MhUtai40ZU2pOnMCGk1BAxtblJximgQVkjy8TQxm1kQD8jZOuBSlIUlJ48IDyJ4FxfY4tOrXWR+nKB3soCUKq1yPwTqkoG8qO0ilJgA6yhW9i7lx8asqQ6EaeaVIHIIOMUTl7FwLHpmW0DiZGXCcdHrkIqMT8G6EzJA8efWUdCrAXJ4oqXKX4xvkjn5dz3aEeufZ9kTkhO24VocDjKkgiaUK+wTjS1ZmJKW0sTePOIE6rIp4w1jNJayUfdfbK7QhfmI2BXHimcHyWXAvI1x+4IYhHpOpCqDSZ4dDHE7TTRzbV1CG6bbfmlKweUj0pHnOz5D4FqXdrjCm2Cq+6efAThCurOnxYqu5L4NOrbMhJW2vWkaumbooA5Fwht5C5gzUDAYgKGTF6xd7Gy4kKISJcTIDSKKIWYaFm2pSTuABM/ERT2D1YAPOXF1P02VKCuKQJH7escvsbmye24+2ylC2VJkDqP4xzLfuIiI+gKQ4omRCVGXgIax7B67AQxlHU3oQGiZOlIMhwnzMN1NrcKslcXD/poLCgC2msh/KOsWHWwWduY5sY/etwJIIoZ8ZnhPSDYdyywQ823denuG0fq9p9sP3/aYsVl46n1VlBCp8oXqmgPkhs2wdKys7JgynT6VgPYX0IUOTxiSSd0/r/jHMtVkBhZVZvJSBuBNT9vGEsrVdyBLbbbtmZUEky+72xrHdMrQMbXuVGAxqUJ2rWj9JlMyA5y5R18F0XIRYTuzctN2ECbkpJp1lxlxh95a/G19iDKx9k/3OlXrtqt20pKgsiQIA3T8s44GVzm0ICPcqbLtu2uGEXSHVvoUnbMkgmsqgco6PWqyCQxdq7fPOyQqS3nCCOq1ER3uuo1ZGNTF9puotw44FoSJTJnyJ4Q1nc0SRi2xd2VmEXItwZpEvNX+EcmtGrf1As5gu45aVlUiJdTHpOB0gsw6iwlII0GpE+HWA2qQ9Jvmm70FSwJuonOX8wBnGNUQdbN3YqxiFIcSVehMyA1lFF13F+66w+6qTlQtUpeJ5RmzQdGVtfpkBRp7SJacfCAuxZTZZIUVOtmajOgpxi63RD7jGVOhPqCU/xhmljFi1ucQl1A2z50nB0wJ/WeIAUQZznLjrB8dWwdwrtsY4xauySTNM6+MNVowQJXT67VwlYlInWF8yZANyWTXdXRQgzSZ6eMcvM2g1C1xTzjZBIlpWcc67s2bLy9uWnGJKUN0jTWsjxjVXYgu1tKuLopSCQXJRtciDO7fwy2EpdG4AAKNSB1++GKJkDt8tuWq21qkSAOtAYM6uDVQD/t6/jB6QJTwMzzEBdWHT0M7tksK/qlSQDxJlSB3roR2gn3mTcZtG2UGg8swZUJA8YzWpXMolOEVTUu+97aawRqCcwhwXaqcqr1HAaKnxpWUVAG9gzvcWzgbRaWjUAGR8J8TGHSdhezLP5c3y3c1avrH8utf1A8oJSrRhG3eWv2rrCNsI2+oCqgABqlMqgdIex/QNXYHrBXq7WlfpI/CG6ltoNjjmhbeo6dsm9yaUPH74JVagW1IIOu+opxuQKZlNQNJy4wZaIxKKj022btDQltUa08OHtiuSM31RW90WaRburaQlRE5AJHWM2coTtXUThxTz1wXHGaHpTWF7mS0+DuEoCEMCSeMgPrpOAkJdsyQoAprMA04xRAjRjvWSCBUCIBuGnats8xdso9PyFVSRpUeMGoZSk2Dbwlhdsb3NgcPDaJ1HOGqBaqELLuHAlp5z0Ukp4SpxMMVZi1QdUwqyQhQ3TUQkgk6GQi7WUA3sW7OHtLpr1CElRSVKBSJgwm/wBwHyYG+37JZUjamZJHuD6tI1b9oVbH8/8AL4XDanGmiacEkeEc7MbruBN9g7zFOlg28kieo5eyEHuHWxDXvbQCRI8qdIHYshqfukAqQlRn1+6Of2FJuh6x79wXFhafeMjMz4DnHLtQMZ8lhbXIbVvK2kcJdJcPGFbVGsUFA7hLOxSpTZExUUl1jr9JxA9QEhgv75l0MrCvRXOZE+Ylp4x6jrXipvwH2b7dt8H2+kWnneQV0kZ0SnnOMZcikBYSlvauZO/LVzuSkLTxPOekVS65FIfuGxbOIs21sISStrzHaAeI1lHXwX0DV2It3nVtFbTaiN5KSAZfSsO2yfhBoxY3CXN++nKFpSwyZlRmRXxnyjidt8kQm9xqcumHGUCSjPyinAjhLjHJwLhkb+4XHuBVhji2sJuNyU01JI+2J7O6tj/oOrYOG2yy02pta0BBCgUKKZ7SDqCOUeC7OB3bgBmLa570vblhuyVMpaT6YmZmUzU/XCePqum4hcK+zFWyra8+IWErcCikGsyVg8Ydx/gCgKbPBpW6LpCZlBJFKaw7jtJBj2+GRd40vuoCXKUCa1BOsFRC3xd01btJsnFBKE8TLjIfhD+CsshTv4pV1eKWwCtJWCCJylOO3gpoU9g8s7FOPtgXyWypuk51nMcfCHa1AveDDYWLF86veQRuMqTmJ68oZogy2La67WbSJtIBEpmnMRM37DdRa5ztoG4VNEteEcj/AJMOtQWe7QbcHu69D+Maak0qSVp7LdaVvYaUSeUx9KQtdNAcuJhFie3Lgki4bUgJ90mfIRMcvYJ1uu34Cq1wbSFAEmYNBWG6Usxy3VswnaT8K0WECZUKQevVdw+LpWa2BLNBxJUFgg11irdRpHL7fThvQXmPyNzZZpThTJobfNOnvGOdlx8WcPJi42D9zuS39KZcTukeWsoFsxnEoAu/z6rsrQZbRMDSCKyOpg3QAZO0buVFRNZk/fG62TZ2Mf7ShdxK1tqbb3zVQSJH3QVxBdgXuu08ogl5Lb2waneuX3xeBrloDruDLrLrFwW3NwUNZmcOZn+P9DdtgvsrgFlCKTFNPxjh9jUvEF+ERJZmPeP3gD6hC+Koe4SZC1Slkk0mifLWGlX6C1xU5hlDZWtPCfSG8SOfmKKxvBvSN1fGOjjtoACq2ybzagAKCUJZ3qBsVmWFxeAKbSSrcCZf6geHSF1ZSDewRYPKrx7Sm30hO5BT5gDqJcRDWNgLbmB0Jubtt5EpBzfTTjy8Y6OO8IoYFpbKuUJUBPakCnQS4Qwr6ELfFWe7KMsuTDZnOpl7yRppxirWIwr7xsrWywq3bchTg30lL9IgLuQSeVaK7FLm0b1AzAA5QO9zdRSptrtF08pTZ2lc0znpIQtaxqxetPvJR5kyAH5RKAti1xHcF8xct29s0FpWqun5R1Ou0iDUKX37Q3Vw36axKniDPQdIa5ALgs9ehKiAoT5fSkXzOfl3M9g4VulXPr0jSbE7bhPY+vNY20UCPrEoYxlLcIcHbm3uUlQlucmZ11h1WSqEnQIO8bJu5tiUgE+kmoA12DlC2XLpAK5r3e2S7dKvTSdwJ/GOZntyQtEso2Mk/av/ANQEJA1J/OOJ2KjWCmoy2M4ybBPnTuKVAiQ/ll+MJYq6nR46CW7lt0377i2zMhe4geMGvQsCHmXGkq3ggpHlqeELXqQprfJXlvcJdSFf01H9VDWXGkD4kGNb3TmdsFKeElKkPrEZstA1dj4y/jsSgNuOpC06hQBNPGOV2DZEvL1h5CnGikgikgAOmgpHNspsRC3v8tctuuNoTNJO3hoZCcNY1oHrsRscxuuG3nBLzhRpp1hlbG1uHGRyFqltGxSCUtpGg/lAMXIdbEOxzF48kMW6NyFGkqaQxg3LCEWb6WBcvpUg8ankD0joxNQdnJ7asUr/AKkyZifGB8QRRXl8606Gm00CwnhpP64Xy0bIW4sl3NuXFJMwicKvGT/YELqwS4shVFAkAeFOko4PdpadCHi2adtnUtgHbPn1H5xOviuyF9cdtovbX4hxSgFa1VLTkDKOnStq7kLXtrHWwuU29w4UMtlJCq1+3pGbZmvxJI673vNnA4xtjGlt1ZZLaqJCvNNOsjMgGAVTtkTIIPKLve5L31rsLQkO7hJRlKvAS5x3etjcIr7BfhrKzxWxZUny1MxOp11js0rFSwsu+82vhlWjJbO6UpJSDQS4DrGmnGpm2xR2mVfS6HCmlK0gNqoB5OeTmQYcd9NKp/SUd2UdIK2LRkWSX0+8Z/cIDaGQA/hLm7yLiAmaUuJl9YPKA2qyDFcL2PsEg0mzLUwO2xa3A6xvHlPLUeDivtMAb1CqyLq4yhCgCqsoHbY39z43dJdoo0gddybFmb5q3ZSW1eafhSXiYao/BixbY3IPvnaaiVKnlDVE2BL9hi7W80UJmCtM9dI6fXrtIK43cdiUv2u1xPmWhIlLnLnDeiBi5717VDDLziG5SBOkoUywybiVx2JUu7QFJ+zqI5ebE3sGqGz+I+HtwtCanp4fdCVsDk2VpxrjqTNPA/dOKWByQj4vEn4halI9xc/qkYLXE0QPG8gLRsNEgUCZeEMVoQo77KuJX5VeQ61gzpoWmSLHIj0w4lXn+hgTxhVclNJv8i+UqTNoyI1rMnpA70M2sYshiXkIG5FE1+qp+6MVoB5lCl+3DiUbqoUArSkou1Ccw6xvcrWLb2MuSUoDiBWh/CMKjYK9yc/lV5llS7gzEuc6QauIHyQQ9mKsrO7ZCFSUJcBwI69IIsTLQ6z3Aw2AC5IdT/GC1ow9dgwwbjDsn905jcDTgJw1WsA7bl5e5Z+4QGWjuSgbTM8PZ4wSUkYe0gPkMoMe8lLitu8zPtE+lDGXkW3kD5PDV0i9eQ+yrcBx+hgXPUI9gpRaIvGJPjynXjE5C1/9QMyqMbZLU2DLb0SPxgdrICUqMlYKIQ0uajJJFONKSJrAuRAjscLbXG1xKZqXU05/xiuSZAma7ffaKA23Qy4cPqMTkDsg1xeCuW0B5LdUSPH8ukGpY1Wpaf3HJMXIZkPrPMDlDVbIJxJGUeX8EHnffO6f1A6xvmZaFPlckklSSqW0xh5dAFqyf2DzilOKaUryk7deEhAndSA4uRs4bDfHbX2U7gmSlGXP6+JiWzV4wGVdBj2Nv8OgIcEkgV9kIZciZqtYKXuLt7H5Bhx9KdzpnLyg6gnWcJO2oUR2T7cKHFNluQHTr/CMWtJG4K4YB7aQluYkfppCmVSapYgf2O5Y9ZfpyCQVcRoIRvQOnIv8pmnmLpDBMvPtIn0P5QtbHIzicGZ1wXDJUqtB9PthzBV1HqWR/YRxFvdtlJqJffHf69vx/oa5KAn7ivGHMeStX9Q7pjhoJQvmu+QKzFbY2LYvS8RRSgZy6xvFaWZ5ajUVdJasAmcpMkJ06/VHWxXioZW0Ff6N9d329pG5tLxJMzpUcoLbP+MGuQ9u1rhTFgbBVC6Egjnz++EcuRMnIrs9ZMWbq1UCxOVPpxhGzSchcdlIG3CUvI8vvGcI9qzuh1WUFwzaXRt2kqT5FSSNeJl+MciuGXqBytMuEdnqS18SpqW5JWDL6cYFmwpbIRv/AKEjD2b4cIbTRtRB1pI/xjmZatAhz4C6Q00GrgyJkJfSUGwNrcg18Ulm5twyirapfdDtVLIC2Uxqmr5xLafKNPrP4R1evQy7LYMe0mEF4h8USkypyBju4aaFOygz9zrXc+m0zUNEDwkSeHjDOyB7sg4UuMOoBpNVYysyTgYrXQbaHGfgVKJ8wSJfVFZsydIN1qwAvmEXDhWoTJ/H+MctWm39RmlQMukvpuVNMig04cZcoPKGa1+pZ4x9KVlFyQNo8ZfXAMmpq1Uy+ZVaXCtqTMzloOPtjWCo31aVW5nuMQoJ9RtGgCvZHWxYuR1Fiq/AP3FvfJX6qUiSPHh7I7fW66jVD2LHWq2Ku5t3L1W64FTOfHWNZusocI5Pd66vMIG73t5gzUE18B+c48v3MUWj7nlO103zBW8xLLEysEAT+n1Qh8FnqZp1LAPf3tjaKI3yM5AU8OcV8NkPYcDT1K0XYuKtHcB93siVpZM6daxU9tXbzTyeAB6/lxg1tKmbPwF7matzi1trX/VMpCnJU+MCwuLg09RUf9P3eayyiy3vSuUqHiT0h7NblXT6G25MuT7evMO2Fra2cOPD2COZbC7MJirB7w2QeQs+qZJTXXhKKr12gti2yvczbjXptOAqCduo1FOfOCrHx3FrgSU3eRSttKdylkyEzWvhDGNCORSY2u17yzT8S+1tSnUyNJ14iGUoUAHVwYTk7Jh0trXIjw/OF8lGwNqsnM9wYtr3nZf/AA/nC/xvloCZhuMlY3qh6bkwCDw/Aw5jxuBe25dYt623JG6kxPT85w1SrKGri720ZYKUr8xAIFPzg6WhCKrKuM5FJbMpTlWXERi/0KklZfL3F5aqafP9MznUnhXWAFcwUcaXcN+mBNA04wO7N0sUd3ZstpJWJHhQawFsu9gUuQ4pRbaruO0D/CLrdIFyGD2T2236Cr2+RtU0rcDKdCo85cIfw5EXyRfdwZdCVKtrZU2jPppTQUhlZJAXsLsbl3J3+5z9vCgi1fURyOQ/w2NLmxSRMEj+MNU1E7b6DcxWBt1NAlPmCZmnEQ3RaSZImQthZLCkiW0zGkbtb8SFRc5RVwytC1cNorPSn4QlkbMOwuciGvVKSfMZ8BzhW+piqlg9ke3jcWpfQ3Oc6y6QjnpK0OhgrDFhkMrd45SrcHaEcJnwhTHjhj1tgU/6kfbe866LUBrzpBb0BFtdOJvLYupqQjcfH2eML2xyQHsayi5WpDlZrUNOSiIH8REEr1z/AGZktW5lL2aA1gOTE4YagIPW1zmHy88nchepBJ1P2xx+xRybCy3xjbVqhEtARp05dISeOWRaMqXsTZOKWZTUK6D84JWsB67ApmPibAStxIEU1EG2RupUYlN9kXQm4TNBUQdTSctCIEnqHQ5sBi7GyKAqhodB+cOYHqUwqyxZetFMtGZM/wAo6lXKBNMijE3jNml7ZJCgQDX8oKsclQCjmEu33luJbntVu+qs9IxfEXxJa7x6yCbZzy+oAiU9ekK3xfQnFmJzHNbS+4JH3h7axyux1XdlNRoVlxaOraVcMJmE0npwPH2Q11ek+OxRORkLhOLDKtRwn/lEEy9VpMgHqzjjD6khUlChrHCzYbLJvoQzqzaJBT65T0r/ABh3r4iF1jMobqYaM5R38CSRC3eZvn5NoTPeOZ/KOhWygm5NY7b+FtlXt2jYpHGXOZPLlEbRi2xXoyCnn/hbUz5CfMyGkBakCaQN4QJb+JCa85e2OpJ1eJjezwt0C03kFJ0nzkPwiGXpuFGEtUOA3UqrQpU/+yYyyiHk7i6uXAyFTQlW2XSAXJGsGW1xaWmySnzKE/aamFnuEqiuvLOSiSNOn0pGLBNiRY2LjwG0fZA1ozPLwQsihy3UUK1B/hDONyZs5DbtdCVlveJzkPwjoY6wDNmOz+17e9aU840FBCd4oOAn9sdDE4MWUmbuG7ZxNw0zbnZok15J/MRu9wfABe4cj8VauIcVMqTT6oVvcviLbGWDarlKgnl98D5KAiUBZcW7XpBDiaeH0…);}\r\n.tv_video{height: 30vh;width: 100%; -o-object-fit: cover; object-fit: cover;}\r\n.ib {\r\n    display: inline-block!important;\r\n\r\n}\r\n.w60{\r\n    width: 60%!important;\r\n}\r\n.w40{\r\n    width: 40%!important;\r\n}\r\n.no-margin {\r\n    margin: auto!important;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRlc3QuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxtRUFBbUU7QUFDbkU7RUFDRSxzQkFBc0I7QUFDeEI7QUFFQTtFQUNFLFlBQVk7QUFDZDtBQUVBO0VBQ0UsWUFBWTtFQUNaLDhCQUE4QjtFQUM5QixnQkFBZ0I7RUFDaEIseUJBQXlCO0FBQzNCO0FBRUEsdUJBQXVCO0FBQ3ZCOztFQUVFLHdCQUF3QjtFQUN4QixNQUFNO0VBQ04sZUFBZTtFQUNmLFlBQVk7RUFDWixpQ0FBaUM7RUFDakMsY0FBYztFQUNkLFVBQVU7QUFDWjtBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixZQUFZO0FBQ2Q7QUFFQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUVBOztFQUVFLHFCQUFxQjtFQUNyQixxQkFBcUI7QUFDdkI7QUFFQTs7O0VBR0UsY0FBYztFQUNkLGVBQWU7RUFDZixXQUFXO0VBQ1gsdUJBQXVCO0VBQ3ZCLHNCQUFzQjtFQUN0Qix5QkFBeUI7RUFDekIsYUFBYTtFQUNiLDJDQUEyQztFQUMzQyx5QkFBeUI7RUFDekIseUJBQXlCO0FBQzNCO0FBQ0E7OztFQUdFLHFCQUFxQjtBQUN2QjtBQUVBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGFBQWE7QUFDZjtBQUVBOztFQUVFLFlBQVk7QUFDZDtBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCO0FBRUE7O0VBRUUsa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixXQUFXO0VBQ1gsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQix1QkFBdUI7RUFDdkIseUJBQXlCO0VBQ3pCLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsd0JBQXdCO0VBQ3hCLDJDQUEyQztFQUMzQyx5QkFBeUI7RUFDekIseUJBQXlCO0FBQzNCO0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLHlCQUF5QjtFQUN6QixZQUFZO0FBQ2Q7QUFDQTtFQUNFLGNBQWM7RUFDZCxXQUFXO0VBQ1gsV0FBVztFQUNYLFdBQVc7RUFDWCw2QkFBNkI7RUFDN0IsMkJBQTJCO0VBQzNCLHNEQUFzRDtBQUN4RDtBQUVBO0VBQ0Usa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLHlCQUF5QjtBQUMzQjtBQUVBO0VBQ0UsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUNmLFlBQVk7RUFDWixZQUFZO0VBQ1osc0JBQXNCO0VBQ3RCLHlCQUF5QjtFQUN6QixhQUFhO0VBQ2Isc0RBQXNEO0VBQ3RELCtCQUErQjtFQUMvQix1REFBdUQ7RUFDdkQsNEJBQTRCO0VBQzVCOzJCQUN5QjtFQUN6Qix5QkFBeUI7RUFDekIsZ0RBQWdEO0VBQ2hELHlCQUF5QjtBQUMzQjtBQUNBO0VBQ0UsZUFBZTtFQUNmLHlCQUF5QjtFQUN6QixvQ0FBb0M7RUFDcEMsZ0NBQWdDO0FBQ2xDO0FBQ0E7RUFDRSx5QkFBeUI7RUFDekIsc0NBQXNDO0FBQ3hDO0FBR0E7SUFDSSxpQkFBaUI7SUFDakIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFHQSxRQUFRLHNCQUFzQixFQUFFLHlCQUF5QixFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixDQUFDO0FBQzdHLGdCQUFnQixnQkFBZ0IsRUFBRSxvQkFBb0IsQ0FBQztBQUN2RCxXQUFXLHNCQUFzQixFQUFFLGNBQWMsQ0FBQztBQUNsRDtpQkFDVyxlQUFlLEVBQUUsa0JBQWtCLENBQUM7QUFDL0MsV0FBVyxnQkFBZ0IsRUFBRSxvQkFBb0IsRUFBRSx5QkFBeUIsQ0FBQztBQUM3RTtRQUNFLFFBQVEsU0FBUyxFQUFFO1FBQ25CLGdCQUFnQixnQkFBZ0IsRUFBRTtRQUNsQyxjQUFjLFlBQVksRUFBRSxtQkFBbUIsRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUFFLGdCQUFnQixFQUFFLFVBQVUsRUFBRSxrQkFBa0IsRUFBRSxVQUFVLENBQUM7UUFDekksV0FBVyw2QkFBNkIsRUFBRSxjQUFjLEVBQUUscUJBQXFCLEVBQUU7UUFDakYsV0FBVyw2QkFBNkIsRUFBRSxjQUFjLEVBQUUsZUFBZSxFQUFFLGlCQUFpQixDQUFDO1FBQzdGLG1CQUFtQix5QkFBeUIsRUFBRSxXQUFXLEVBQUUsaUJBQWlCLEVBQUUseUJBQXlCLEVBQUU7UUFDekcsc0JBQXNCLGdCQUFnQixFQUFFO01BQzFDO0FBSUEsTUFBTSxRQUFRLEVBQUUsU0FBUyxDQUFDO0FBQ2hDLFNBQVMsVUFBVSxFQUFFLFdBQVcsRUFBRSxpQkFBaUIsRUFBRSxXQUFXLEVBQUUscTlDQUFxOUMsQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLGlCQUFpQixFQUFFLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxjQUFjLENBQUM7QUFFcm9ELFVBQVUsVUFBVSxFQUFFLFdBQVcsRUFBRSxpQkFBaUIsRUFBRSxXQUFXLEVBQUUseXNDQUF5c0MsQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLGlCQUFpQixFQUFFLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxjQUFjLENBQUM7QUFHMTNDLFNBQVMsVUFBVSxFQUFFLFdBQVcsRUFBRSxpQkFBaUIsRUFBRSxXQUFXLEVBQUUsNnRDQUE2dEMsQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLGlCQUFpQixFQUFFLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxjQUFjLENBQUM7QUFFNzRDLFNBQVMsVUFBVSxFQUFFLFdBQVcsRUFBRSxpQkFBaUIsRUFBRSxXQUFXLEVBQUUsNjVDQUE2NUMsQ0FBQyxrQkFBa0IsRUFBRSxXQUFXLENBQUMsV0FBVyxFQUFFLGlCQUFpQixFQUFFLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxjQUFjLENBQUM7QUFFN2tELGNBQWMscWtDQUFxa0MsQ0FBQztBQUNwbEMsT0FBTyxXQUFXLEVBQUUsWUFBWSxFQUFFLGlCQUFpQixFQUFFLGl5VEFBaXlULENBQUM7QUFDdjFULFVBQVUsWUFBWSxDQUFDLFdBQVcsRUFBRSxvQkFBaUIsRUFBakIsaUJBQWlCLENBQUM7QUFFdEQ7SUFDSSwrQkFBK0I7O0FBRW5DO0FBQ0E7SUFDSSxvQkFBb0I7QUFDeEI7QUFDQTtJQUNJLG9CQUFvQjtBQUN4QjtBQUNBO0lBQ0ksc0JBQXNCO0FBQzFCIiwiZmlsZSI6InRlc3QuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgdXJsKGh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1Sb2JvdG86NDAwLDMwMCk7XHJcbioge1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbn1cclxuXHJcbmh0bWwge1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG5cclxuYm9keSB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIGZvbnQtZmFtaWx5OiBSb2JvdG8sIHNhbi1zZXJpZjtcclxuICBmb250LXdlaWdodDogMzAwO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmNWY2Zjc7XHJcbn1cclxuXHJcbi8qIEZvcm0gZWxlbWVudCBzZXR1cCAqL1xyXG5mb3JtIHtcclxuICAgIFxyXG4gIC8qIHBvc2l0aW9uOiBhYnNvbHV0ZTsgKi9cclxuICB0b3A6IDA7XHJcbiAgLyogbGVmdDogNTAlOyAqL1xyXG4gIHdpZHRoOiAzMDBweDtcclxuICAvKiB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSk7ICovXHJcbiAgbWFyZ2luOiAycmVtIDA7XHJcbiAgei1pbmRleDogMTtcclxufVxyXG5cclxuZmllbGRzZXQge1xyXG4gIG1hcmdpbjogMCAwIDFyZW0gMDtcclxuICBwYWRkaW5nOiAwO1xyXG4gIGJvcmRlcjogbm9uZTtcclxufVxyXG5cclxubGVnZW5kIHtcclxuICBmb250LXdlaWdodDogNDAwO1xyXG59XHJcblxyXG5sZWdlbmQsXHJcbmxhYmVsIHtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPXRleHRdLFxyXG50ZXh0YXJlYSxcclxuc2VsZWN0IHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwYWRkaW5nOiAwLjVyZW07XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgYm9yZGVyLXJhZGl1czogMC4yNXJlbTtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZTVlNWU1O1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbiAgLyogTGlzdCBzb21lIHByb3BlcnRpZXMgdGhhdCBtaWdodCBjaGFuZ2UgKi9cclxuICB0cmFuc2l0aW9uLXByb3BlcnR5OiBub25lO1xyXG4gIHRyYW5zaXRpb24tZHVyYXRpb246IG5vbmU7XHJcbn1cclxuaW5wdXRbdHlwZT10ZXh0XTpmb2N1cyxcclxudGV4dGFyZWE6Zm9jdXMsXHJcbnNlbGVjdDpmb2N1cyB7XHJcbiAgYm9yZGVyLWNvbG9yOiAjZWY3ZWFkO1xyXG59XHJcblxyXG50ZXh0YXJlYSB7XHJcbiAgbWF4LXdpZHRoOiAzMDBweDtcclxuICBoZWlnaHQ6IDEwMHB4O1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPXRleHRdLFxyXG5zZWxlY3Qge1xyXG4gIGhlaWdodDogMzRweDtcclxufVxyXG5cclxuc2VsZWN0IHtcclxuICBmb250LXNpemU6IDAuODc1cmVtO1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPWNoZWNrYm94XSxcclxuaW5wdXRbdHlwZT1yYWRpb10ge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB0b3A6IDVweDtcclxuICB3aWR0aDogMjJweDtcclxuICBoZWlnaHQ6IDIycHg7XHJcbiAgbWFyZ2luOiAwIDAuNXJlbTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZTVlNWU1O1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbiAgLW1vei1hcHBlYXJhbmNlOiBub25lO1xyXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcclxuICAvKiBMaXN0IHNvbWUgcHJvcGVydGllcyB0aGF0IG1pZ2h0IGNoYW5nZSAqL1xyXG4gIHRyYW5zaXRpb24tcHJvcGVydHk6IG5vbmU7XHJcbiAgdHJhbnNpdGlvbi1kdXJhdGlvbjogbm9uZTtcclxufVxyXG5cclxuaW5wdXRbdHlwZT1jaGVja2JveF0ge1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxufVxyXG5pbnB1dFt0eXBlPWNoZWNrYm94XTpjaGVja2VkIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWY3ZWFkO1xyXG4gIGJvcmRlcjogbm9uZTtcclxufVxyXG5pbnB1dFt0eXBlPWNoZWNrYm94XTpjaGVja2VkOmFmdGVyIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBjb250ZW50OiBcIlwiO1xyXG4gIGhlaWdodDogNHB4O1xyXG4gIHdpZHRoOiAxMHB4O1xyXG4gIGJvcmRlci1ib3R0b206IDNweCBzb2xpZCAjZmZmO1xyXG4gIGJvcmRlci1sZWZ0OiAzcHggc29saWQgI2ZmZjtcclxuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSg1cHgsIDZweCkgcm90YXRlKC00NWRlZykgc2NhbGUoMSk7XHJcbn1cclxuXHJcbmlucHV0W3R5cGU9cmFkaW9dIHtcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbn1cclxuaW5wdXRbdHlwZT1yYWRpb106Y2hlY2tlZCB7XHJcbiAgYm9yZGVyLXdpZHRoOiA1cHg7XHJcbiAgYm9yZGVyLWNvbG9yOiB3aGl0ZTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWY3ZWFkO1xyXG59XHJcblxyXG5idXR0b24ge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbjogM2VtIGF1dG87XHJcbiAgcGFkZGluZzogMC41cmVtIDJyZW07XHJcbiAgZm9udC1zaXplOiAxMjUlO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgYm9yZGVyLXJhZGl1czogMC4yNXJlbTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWY3ZWFkO1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbiAgYm94LXNoYWRvdzogMCAwLjRyZW0gMC4xcmVtIC0wLjNyZW0gcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG4gIC8qIFdlJ2xsIHRhbGsgYWJvdXQgdGhpcyBuZXh0ICovXHJcbiAgdHJhbnNmb3JtOiBwZXJzcGVjdGl2ZSgzMDBweCkgc2NhbGUoMC45NSkgdHJhbnNsYXRlWigwKTtcclxuICB0cmFuc2Zvcm0tc3R5bGU6IHByZXNlcnZlLTNkO1xyXG4gIC8qIExpc3QgdGhlIHByb3BlcnRpZXMgdGhhdCB5b3UncmUgbG9va2luZyB0byB0cmFuc2l0aW9uLlxyXG4gICAgIFRyeSBub3QgdG8gdXNlICdhbGwnICovXHJcbiAgdHJhbnNpdGlvbi1wcm9wZXJ0eTogbm9uZTtcclxuICAvKiBUaGlzIGFwcGxpZXMgdG8gYWxsIG9mIHRoZSBhYm92ZSBwcm9wZXJ0aWVzICovXHJcbiAgdHJhbnNpdGlvbi1kdXJhdGlvbjogbm9uZTtcclxufVxyXG5idXR0b246aG92ZXIge1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmY5NmM4O1xyXG4gIGJveC1zaGFkb3c6IDAgMCAwIDAgcmdiYSgwLCAwLCAwLCAwKTtcclxuICB0cmFuc2Zvcm06IHNjYWxlKDEuMSkgcm90YXRlWCgwKTtcclxufVxyXG5idXR0b246YWN0aXZlIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWY3ZWFkO1xyXG4gIHRyYW5zZm9ybTogc2NhbGUoMS4wNSkgcm90YXRlWCgtMTBkZWcpO1xyXG59XHJcblxyXG5cclxuLnJpZ2h0Qm94IHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDMwdmg7XHJcbn1cclxuXHJcblxyXG50YWJsZSB7IGJvcmRlcjogMXB4IHNvbGlkICNjY2M7IGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7IG1hcmdpbjogMDsgcGFkZGluZzogMDsgd2lkdGg6IDEwMCU7IHRhYmxlLWxheW91dDogZml4ZWQ7fVxyXG4gICAgICB0YWJsZSBjYXB0aW9uIHsgZm9udC1zaXplOiAxLjVlbTsgbWFyZ2luOiAuNWVtIDAgLjc1ZW07fVxyXG4gICAgICB0YWJsZSB0ciB7IGJvcmRlcjogMXB4IHNvbGlkICNkZGQ7IHBhZGRpbmc6IC4zNWVtO31cclxuICAgICAgdGFibGUgdGgsXHJcbiAgICAgIHRhYmxlIHRkIHsgcGFkZGluZzogLjYyNWVtOyB0ZXh0LWFsaWduOiBjZW50ZXI7fVxyXG4gICAgICB0YWJsZSB0aCB7IGZvbnQtc2l6ZTogLjg1ZW07IGxldHRlci1zcGFjaW5nOiAuMWVtOyB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO31cclxuICAgICAgQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNjAwcHgpIHtcclxuICAgICAgICB0YWJsZSB7IGJvcmRlcjogMDsgfVxyXG4gICAgICAgIHRhYmxlIGNhcHRpb24geyBmb250LXNpemU6IDEuM2VtOyB9XHJcbiAgICAgICAgdGFibGUgdGhlYWQgeyBib3JkZXI6IG5vbmU7IGNsaXA6IHJlY3QoMCAwIDAgMCk7IGhlaWdodDogMXB4OyBtYXJnaW46IC0xcHg7IG92ZXJmbG93OiBoaWRkZW47IHBhZGRpbmc6IDA7IHBvc2l0aW9uOiBhYnNvbHV0ZTsgd2lkdGg6IDFweDt9XHJcbiAgICAgICAgdGFibGUgdHIgeyBib3JkZXItYm90dG9tOiAzcHggc29saWQgI2RkZDsgZGlzcGxheTogYmxvY2s7IG1hcmdpbi1ib3R0b206IC42MjVlbTsgfVxyXG4gICAgICAgIHRhYmxlIHRkIHsgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkZGQ7IGRpc3BsYXk6IGJsb2NrOyBmb250LXNpemU6IC44ZW07IHRleHQtYWxpZ246IHJpZ2h0O31cclxuICAgICAgICB0YWJsZSB0ZDo6YmVmb3JlIHsgY29udGVudDogYXR0cihkYXRhLWxhYmVsKTsgZmxvYXQ6IGxlZnQ7IGZvbnQtd2VpZ2h0OiBib2xkOyB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlOyB9XHJcbiAgICAgICAgdGFibGUgdGQ6bGFzdC1jaGlsZCB7IGJvcmRlci1ib3R0b206IDA7IH1cclxuICAgICAgfVxyXG5cclxuXHJcblxyXG4gICAgICBib2R5eyBtYXJnaW46MDsgcGFkZGluZzowO31cclxuLnBsYXktYnR7d2lkdGg6NTBweDsgaGVpZ2h0OjUwcHg7IGJvcmRlci1yYWRpdXM6NTAlOyBwYWRkaW5nOjVweDsgYmFja2dyb3VuZDojMDAwIHVybChkYXRhOmltYWdlL3BuZztiYXNlNjQsaVZCT1J3MEtHZ29BQUFBTlNVaEVVZ0FBQURJQUFBQXlDQVlBQUFBZVA0aXhBQUFBR1hSRldIUlRiMlowZDJGeVpRQkJaRzlpWlNCSmJXRm5aVkpsWVdSNWNjbGxQQUFBQTlkSlJFRlVlTnJNbW10SUZGRVV4OGRWTEhzU2xxVmhxUmlvSkZMNG9jakl5RWRpOWlBTEFvTWdDdWxGRUVTQlZFUWZDdnRvVVBSQWtzQ2dzS2draXFSQXl6SjZvQ0dLYWJna2xZYjVTRE8xNlgvWU03S0tqNTA3ZCs3T2dSL3JMczdkKzU4Nzk5NXovbmNEZEYzWEpNWmlFQTNDd0RKK25RR0d3Vy9nQnMyZ0hUU0FibGxmSEdUeGVoZElBQnZCV3BBRUZvRnBVMXpYeTZKcXdIUHdETFJhNlVpQTRJZ0VnNjFnTjhqZzkxYUNScWdNbElCS29SWklpRWsyZ3lyZG51Z0h0MENTMlg2WitlZFlVS0tyaVU1d0NvVElGcElObW5YMVVRSGlaUWs1Q2daMC80VWJwRm9WY2s1M1J2d0VPYUpDQ25SbnhTK1FQbEYvSjFwK2FWa3Q1bjNDU2ZFVlpJRmFYL2FSRmJ4QnpkT2NHZFVnYzJ4V01QYU8wNDU4U1VERVA0VkNWb0dDOFZJTTd6Z0VWZ3MwVHZuVEVmQkZrWmpETEdqY25UMEN0QWxPeEhwdUl3b1VnNzhLSm4rNTkyVDNIcEdESUZ6d0RnV0NFQjZSUFNDYkUwSTdJNFBueXFoSGF5R3ZWTExpS1VnREo4RVBtNFRRemR0dmFEQ0VwSU5JeVY5RXE4cDVzQUhjdFVsTUd0Yy9JMEx5Ykh3RTZrQXUyQUhxSmJjOUIyeWpWeElTQVpZcldHbnVnRlJReU5XaXJLQVJqeUVoaVN4R1JkQjhPUTdXZzhlUzJvd0hhMXhjV3djbzNwMXBSY3NCK2FERllsdTBVQ1c2RkQxVzQ4VVF1QUxXZ2F2OFhpU20wMEpGUW1MOW5EdTVlUmxORTY3WGViS0hPU1FaZk1FYjNBbnd6YXoxNEpMZ2dNaU1QbkFCcElEcm9OK01MK1hFK016WndiQVpJWDBPRTdFU2xJTlNNTXVNa0RhSENLQWE2QXlvNENyUXRPWFo0QUFSMjlrNlBRM21tcDNvNUZRR2NTN2tyNGpsenVkWlhDRGNKS1FKRFBoZ1BNdU1FSzUvanJIcGJTWEkvSDVEajlaSDNwUlVSVHF2U0lVU1JHanNxTlM0dUc1NHBVQUFuWjBVZ1llVTVFbHM5d0dOaXJHUDNMUlJnRkhKVmZMakpITURKcC9yQ1UwTlE4aEw4TTRHRWNsOHh5ZzVqTEtoL1RLamxIWjV6ZnpMRXI4Z0ZKelZQRVpmbGswajNUMnF6MTZXQ3AxRnZCZTBaaHE5MnNrRm54VFlRVVdUbWRoYndKQkFvM1VnQVpRcVBHcUluRXdJZWNFM0JCcnVBUjBLbmZtOXZyanhvWnd1K0t0eW5DcXVnWDFqUDV6b1dDR1pNOUFGRGhOUnBYbGN6QzVmNjVHM21zZjY3SEtRaUZyT3lick1GbGJsZkdHN0EwUjhBRHUxeWR4K0h3NURVMENUSDQvY0hvRndXY2ZUMGVDK1lnR0RmQmpyMDFtN21SOE1CSUo4MEtwQVJEWEl0T3VYRHdaTHdFWHczUVlCbEJFY0FMUE45a3YwUnpVYUo0Rzd3Q2JOYzF3bmFydjJzcWQxRzl3RFBTS05XQkZpeEV3UXgwNDdtZE5Md1h3MkU0SzlCTktCNlIvUXdUU3drZjJhcTlSaEs1MlFJV1RFdGdReG11ZmdKWTcvSmpzbmlBMkNRZERKQWhyWnZHN2h6eTNIZndFR0FGZHdvSkh6c0JHREFBQUFBRWxGVGtTdVFtQ0MpIG5vLXJlcGVhdCA1cHg7cG9zaXRpb246IGFic29sdXRlO2JvdHRvbToyNXB4O3JpZ2h0OiAxNzVweDsgbWFyZ2luLWxlZnQ6LTI1cHg7IG1hcmdpbi10b3A6LTQwcHg7ei1pbmRleDogOTk7IGN1cnNvcjpwb2ludGVyO31cclxuXHJcbi5wYXVzZS1idHt3aWR0aDo1MHB4OyBoZWlnaHQ6NTBweDsgYm9yZGVyLXJhZGl1czo1MCU7IHBhZGRpbmc6NXB4OyBiYWNrZ3JvdW5kOiMwMDAgdXJsKGRhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBRElBQUFBeUNBWUFBQUFlUDRpeEFBQUFHWFJGV0hSVGIyWjBkMkZ5WlFCQlpHOWlaU0JKYldGblpWSmxZV1I1Y2NsbFBBQUFBdzFKUkVGVWVOcmNtdHRMVkVFY3gyY1AycFZTb3d0S0ZCUVJTcG9ZVzFRRVFVVlJJWFNCSHBJSXVtQlBJUVUrOUJkMGdTS2hySjZqaU9oQ2dUNEVaUTlkS1NoSktib1JoRVdCYUNtNWF0UDMxODdBWVpoZDk4eWM0NTdaTDN4d1BjNjY1N056enBscmduUE9Rc29pa0JSVWduSlFCaWFEQkVpQlB2QU5mQVF2d1RQUUNmN1lmbmpDVW1RZTJBbDJnRG93eWVCL3ZBVnQ0SW9RTXd1SkdGQUpXa0V2RHkram9CMXNNam1ub0c4b0JTZkFBSTgydDBCMVZDTHJRQmNmdi9TRHByQkZqb0lVejArdWlpdkJXcVNGNXo5UFFFVzI4eHpycVhVUkhHRHh5R3V3R1h6Vi9kSEw4c1pUTVpLZzFJQWJvQ1NJQ0FrY1lmSExjbkJKTkxCak5vaEx3U013aGNVM1RlQk1OcEZpMEFGV3NuaG5RSFNGdWpOZFd2c2RrS0JNQlNjejFVaXA2TUROWmU1a0E3aEhMNHA4Qi9kYVNBeUM3NXB2YmJaeTdEZjRvUnliQm1ZYWZtNnpGSkVOeWdUTDdzZGRVS3l3UzFQdXNxYmNRY3VPWmgwNXlIdGtqUmhEbUdZVURHc3dMWmRyNlB3Yi9EZjdkdHR4VFk1dFZLN2xnbVFMbU9pSlIrNWE1bTVvWkxxRVJCYUlYMXdOMWZJS0Vxa1N0ZUp5bG5tTzE0Yk1RazlNSUxpZU9TUXlvd0JFcG51R1V6aHh5Ly9ITHk4QUVlNkpMckhyR2ZRMG5UMFgwMHNpbnd0QXBJZEUzaFdBU0RlSnZBRy9IQmQ1VGlJOUxEMW41R3FHd0ZQNStHMTNXT1FGK0NUSEFqZkJpT1hBU3ZkTnFSbkpzVnlRWEtmS2tHTjJ1ay91aThHOFNXYUJWY3F4R2syNWNrMjVLZ3VKZm5CTm5VWFpDdTQ0ZGxtMWdrT3FDRjFtRDhGcVJ5U29SMUlMM3F2ajViL2dtRU45cnhZcG9kYUl6SG5RR0hNSldrQk4rdHMvblFoTjJ6OW1kdE5EVVlhbWo5YUwyNEQ1N3dzMXRCYmVFT1BXdmxtVnlDUkNvY1g4UFpadFN4UTVDMDdyUnlUWjF3OTNnMkVlajF5d1hRemRCdnJ5TEhFOHJPWHBKT2pNZ3dDdHRlOExlOE5BR1RnM2poSVBRRzFVV3pqa0RvaU9DQVUrZ0VaUUZPVmVGRC8xb0EwTWhTVHdDaHdXTlIvNGZHeTNPVkdxUVQzWUtIcThKVG0rTHlXRzJiVDRlbHY4VEptZVJCZ2kvbFNJYnZsaU1KK21NbGw2Q1k3YUs5cGM5aE44RVgya0xwYmVnQlpLVy9WUGdBRUFHWVh2Mi90UDFMMEFBQUFBU1VWT1JLNUNZSUk9KSBuby1yZXBlYXQgNXB4O3Bvc2l0aW9uOiBhYnNvbHV0ZTtib3R0b206MjVweDtyaWdodDogMTc1cHg7IG1hcmdpbi1sZWZ0Oi0yNXB4OyBtYXJnaW4tdG9wOi00MHB4O3otaW5kZXg6IDk5OyBjdXJzb3I6cG9pbnRlcjt9XHJcblxyXG5cclxuLnN0b3AtYnR7d2lkdGg6NTBweDsgaGVpZ2h0OjUwcHg7IGJvcmRlci1yYWRpdXM6NTAlOyBwYWRkaW5nOjVweDsgYmFja2dyb3VuZDojMDAwIHVybChkYXRhOmltYWdlL3BuZztiYXNlNjQsaVZCT1J3MEtHZ29BQUFBTlNVaEVVZ0FBQURJQUFBQXlDQVlBQUFBZVA0aXhBQUFBR1hSRldIUlRiMlowZDJGeVpRQkJaRzlpWlNCSmJXRm5aVkpsWVdSNWNjbGxQQUFBQXh4SlJFRlVlTnJjbWt0b0ZERVl4elBEYW11eEtvcGRhRlZRRVYrZ2lGVmFGYlJXUkVSOEliUkZLaXBWMEtNSGJ4NjhlVlZRUlBTeUY4V0xGUEdCVk1SblMydFBvbGdvaW1nVldsOTFpN0R0MXZqLzJBeU00MDduc1VrMzJULzhMcnZ6eUgrU2ZFbSt4T0tjTTBtYUJoYUFEV0F4cUFGelFTV3dRUVo4QjUvQUVPZ0VmZUNMakpjbkpOeGZENXJBWnJBRWxFVzQveWQ0QWU2QjIrQkQ3SkpRamNTZ0Fod0ZuVnlldm9HTFlHV2NNc1V4c1IvMGNuVktDME1MVlJtcEFTaytlZm9JV21VYmFRRDl2RGk2SnBweXdVYm9xNHp3NHFvREpBc3hjaHlNY1QzVUJhcjl5bXBOTUk2MGdoVFRTOC9BTGpEcy9jUFB5RVlSMnl1WmZyb0Ztc0dvKzBjN3o0VTBHbC9WMUFScEh6Z1Zwa2F1Z0dOTWI0MkFSdER0WjRUK3ZBdW1CanlJcXBVckxPZ1VuOWJpMWlPd0hXUzlScWp3ajBGZHdBUEd4UVA2Rkpwb0I2dENYTnNDYm5nbmpidERtSEJFTTlZQmhUVXlGdks2MDZMeloyeFhwMitMK05WVWlWcUdGZkxhTldDYk8yb3RCMXVabVRyc05ySlg4VmRXcVFhUXRFVS9hV1RtYWc2MUpqSlNCZFl5czFWTFJoYUI2WVlicVNjajYwSU1QcnFyaWd6TVpPWnJOaG1aVlFKR0tzaklqQkl3a3JCWmlZaU0vQ29CSDFrNzM3TFJRUDIyUlQ3V2RQMGdJejNnaitGR1BwT1JkeUJ0dUpFblpJUlMvQzhOTi9MS0Zrdlhod2FiK0VybGQ4YVI5Z2pMUzkxRWxURGtHSGtMT2d3MWtuS3ZFQ2xxWFk1d2MwWmh3VVlqUk5FZXB3TGNXUlJLa1Q1bnVYUnAwR3hnaFhoaFFySUpMcElQWWRkSDUwUTUva3ZRMFQ3Z0F4YWNvQnRYT1BaWUlUOFE5WTBkTEUrQ3p0RWxjRUx6ZnBFV1NZZGVkelB4Nmd4NG83bVJzMjRUZmpWQ3FoTjlSc2RGMTAxdzBHbFNFOVVJcVF1Y1pKNDlDQTFFaWVzMnJ3a1drSFM0em5MYkM3cVlvWWphNURjdkRGb2gwbUJ6UklQRjEzMndSOHdMZlFKM3VPM3BUZUIxa1RaQjZmQkFtY3dEQTBteDU1MmRKQVB2UVl2S0l4dzdRYmRDQThQZ1BKaXYraXdLVVE0T2dhY1M5K0VId1FXd0xFNlpMQW5udFdyQkFiQUZyQWJsRWVaVmd5TFUwNWgxaCtYT2NzV2IxMGc4ZUVienMzbGdQVmpLY3VuK2F2YnZ3VFBLMkF5STZFUGh0RitZS1ZoL0JSZ0FFNGgrMXR2UVhIb0FBQUFBU1VWT1JLNUNZSUk9KSBuby1yZXBlYXQgNXB4O3Bvc2l0aW9uOiBhYnNvbHV0ZTtib3R0b206MjVweDtyaWdodDogMTAwcHg7IG1hcmdpbi1sZWZ0Oi0yNXB4OyBtYXJnaW4tdG9wOi00MHB4O3otaW5kZXg6IDk5OyBjdXJzb3I6cG9pbnRlcjt9XHJcblxyXG4ubXV0ZS1idHt3aWR0aDo1MHB4OyBoZWlnaHQ6NTBweDsgYm9yZGVyLXJhZGl1czo1MCU7IHBhZGRpbmc6NXB4OyBiYWNrZ3JvdW5kOiMwMDAgdXJsKGRhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBRElBQUFBeUNBWUFBQUFlUDRpeEFBQUFHWFJGV0hSVGIyWjBkMkZ5WlFCQlpHOWlaU0JKYldGblpWSmxZV1I1Y2NsbFBBQUFBNjFKUkVGVWVOcnNtVWxJVlZFWXh6TTFTMHNiSk12TU1CUTBYS2dOMElBTkJvVldGQVZCcmxwWUpMcXdWYldMYUdWSlJrVzBNUnBXTmtvaFJGU1lvRUhESXFrb1VsQWJ6QXcxVVhMZzlqOXdEdjM3OGoyOXZYZXZyN2dmL1BBNzAvWDgzNW51K1c2WVpWbVQvZ2NMODRSNFFqd2huaEJQeUNpMkcyU0RDNkRaWnR1TklCYmNCaU0rYXlraERsTm0vYko3SU54RzIyMWdXTGM5NXErdWt3SW1nMVBXNzlZSjRtdzhvMUswMytLMmtPbmdwdlduZmJJcEpBZDhwL2F0WUk1YlFoYUNSbXQwRzQrUUtKRXVFczg0NzRhUWJOQmkrYmF4aE93QnplQ09xRmREenhnRVdVNEtLUURkbG4vekowUnRBbTFVOXlLVnBZa3BkczBwSVFmQWlEVzIrUk9pam9KYm92NG1LcStnL0I5YVhGQ0ZITGZHYjJOTnJTVFFSZldmZ2toZGxnTDZxYXdpV0VMVW9yeHMyVE1wWklsZVYvemNZdEZtRjVYeC8ydmhqWUZQOWloUURCYU44OFJkQmxiYlBLVS9nM1RRQTdhQzZ5QVNsSUl6dXM0VTBBVFNkUG9CeU5QK1psQkx6MXNMNnVUSmZ0aHkzbmhFemxGK0QwaWd2aHlrTXJYMk11bDgra0JsNWFiTlpGS1g3Zko3WGpYNTZsMnFoTkpYOUtncFUzM00xMzZmSGlGaks4RU1VOG5Zc010Q0hvSWFTdThGMGRyL0FocW9iQjM1bksrbVg0b1VNaEZXU2Y0Q3NKelNkZVJubVY5ZXJ4OWo4U0ExRklRMGdvK1V6aVgvQ2ZuelFMTDIyOEVRVGJ2a1VCRFNEMTVRbWtkRTNWc0d6YjFKaTFIMkRmUlN2WVJRRUtMc0ZmbHp5ZS9UR0p1cC93N1FSbUEyaXBBUXdwMmFSdjRRVFNGenpwbE5hWkR5STBORmlPeThzWENOM0ZWVm55TW9meVJVaEtTUjMwVysyb3BqS0czV3hWVGF3Y3dVbkhBaDZwZk5vZlJMOHBQRWFIWFFXb21qL001UUVKSUpGbFA2a1hpWE05WU5XclUvWDQrS3NiWlFFTEtQK3RBalR1MDFZcVRNdEVzWEFwdWxrQWlYUldUbzF4SmpONml6MGVKd3JDZC9CZmt0NEwwVTB1Q3lrRUthSW1ybkthZXk3WG9LR2F1bHJUYVA4cCtCcjNJVVR1dWhHczk5UkoyMEJXQnBBRUtlazYvdUlxOHBYU0ttVmFQMmM4VFV1aHVNU0dPc2lHNzh6UTF4Zzc0QmhsRmV2bWhUU21VbmZBWDdBcjJ2UjRDelFieXpSNE0zVkwrTjZzOEdIVlIyMVlrb3lxRWdDVGtwNmhmNWlDRXJXK1ZVWEVzRjF3WUNGUEtPNmo3V0k2N3laNEYyS3F0M090S1lLNGJmcnBEOW9GZFByMVRLbDhId0FqZGl2K21nS1lEWWJ5S0lvZlI2TUVUUHFIVXpHaDhQN2djaEdwOG9Zc2w5SU1QdDd5TXFlRllWNFBlUkk2SjkyVVI4NkRFY0ZZdlV6aGVySGRTMjJsOWR0NzRoRnVvMzNTcncxbWJiblNBUlhCSzNTZStycmlmRUUrSUorWWZ0cHdBREFKczR1ekxXb3BXNUFBQUFBRWxGVGtTdVFtQ0MpIG5vLXJlcGVhdCA1cHg7cG9zaXRpb246IGFic29sdXRlOyBib3R0b206MjVweDtyaWdodDogMjVweDsgbWFyZ2luLWxlZnQ6LTI1cHg7IG1hcmdpbi10b3A6LTI1cHg7ei1pbmRleDogOTk7IGN1cnNvcjpwb2ludGVyO31cclxuXHJcbi5tdXRlLWJ0LnN0b3B7YmFja2dyb3VuZDojMDAwIHVybChkYXRhOmltYWdlL3BuZztiYXNlNjQsaVZCT1J3MEtHZ29BQUFBTlNVaEVVZ0FBQURJQUFBQXlDQVlBQUFBZVA0aXhBQUFBR1hSRldIUlRiMlowZDJGeVpRQkJaRzlpWlNCSmJXRm5aVkpsWVdSNWNjbGxQQUFBQXFwSlJFRlVlTnJzbVUxSVZVRVV4NTlTWm9pS2lCVjlXR0FhcVl1S3NLS0NRbHRFdEk1Y0pFSWY2MElNSVlQYXBBdWgwQ2lLRnRMbVNRVFJ1aFNFUHFBV3JXb1hoS0pQalQ0b0F5bTkvUWZPeGNQaHpudjM5dDU0NzhRYytQRzRNMmZ1dkQ5ejVzekhMZkk4TC9VL1dKRVQ0b1E0SVU2SUV4Smc3V0F2dUE4K0dPbEJDVEhNZFcvWjNvTFZKdm94T1NJbDRCN29ZR1Z6b0I1OHR5VzAxbE1ZblJUbDAyQ25MVUthd0dPd0k2RE9HaUdIUUJwczB0UmJJZVEwaFZOWkZoOWpRZ3FWTlhxOGNEWUZLazFrclh4Zm9FYjBqaGZlcEpDRG9CYzBSZXozTExnRUtnb2hwQXFrdldqR2hkU0JuMVNlb2Vjdy9mYXg5OTMweTFlSlNPc0RSOVRjQ1JHVjY4QzJQS0s2Z3MwbmxhNmZnRll3bTZYTlpjSzNqYm81WXRyNGlLaXd2Q1hxeDBDcFppUk9nRVhtK3hYczBZWFdTZ3BSbElCUjRYTTdRTVJ1OElYNS9DWmhxYVFJVVZUVEhvemJWVmEvRlh3UzllZHpaYTA0aFBoL2RscjR0dE9JdlJEbC9XSFNiMXhDRksxZ2dmbCtwam5ETFIxMkhZbFRTSXBHNFkrbTdXdFFib3NRUlZlQW1IZGdRN1oyeFFrOHRiNVhxNElvbXdLWktIdXR1RWVrR2N4bzJ0Nk5zdGVLVThnVzhKSDVxdkNhRmUyN2t5NWtEWGdwZkcrQUJ0cUgrYllFVGlWWnlBUGg5NVJkVXJUUlN1N2JQTmlYUkNFRHdtY2NsQW1mRHVFeklYZkxjUXZwRlBXVG9GWXpENjRFcE9RYW5aQTNOTWtXNlRjYitRclpEbjZ4dWgvZ1FJNDFaa1M4YjFCM0hqa01ha0Z4UUM2WDFnSUdRZVUvcmhmbFlLMjRqWHlWbzAwbnFBTEgySm1vSUdmMi9RR2J2U2pua1l2Z09UZ1Q4V1Q2a0JKQ1l5RnZHaHZCQ0dnTzRadjQ2NkFhTUF5TzJ5N0V2K3NkQXVkc3Z0ZmlYTFB4WGt2SEJYYlZzeUpDVEg1V09Bb2VnV3BXbHFIUSttYmJGNnRkbE5FYTZIa0cxSUY1Rzc5WWJRYlBhSC9VYmFvZjl6SFVDWEZDbkJBbkpCYjdLOEFBNWpOcHVGY1FoYWdBQUFBQVNVVk9SSzVDWUlJPSkgbm8tcmVwZWF0IDVweDt9XHJcbi52aWRlb3t3aWR0aDogMTAwJTsgaGVpZ2h0OiAxMDAlOyBwb3NpdGlvbjpyZWxhdGl2ZTsgYmFja2dyb3VuZDp1cmwoZGF0YTppbWFnZS9qcGVnO2Jhc2U2NCwvOWovNEFBUVNrWkpSZ0FCQWdBQVpBQmtBQUQvN0FBUlJIVmphM2tBQVFBRUFBQUFaQUFBLys0QURrRmtiMkpsQUdUQUFBQUFBZi9iQUlRQUFRRUJBUUVCQVFFQkFRRUJBUUVCQVFFQkFRRUJBUUVCQVFFQkFRRUJBUUVCQVFFQkFRRUJBUUVCQVFJQ0FnSUNBZ0lDQWdJQ0F3TURBd01EQXdNREF3RUJBUUVCQVFFQ0FRRUNBZ0lCQWdJREF3TURBd01EQXdNREF3TURBd01EQXdNREF3TURBd01EQXdNREF3TURBd01EQXdNREF3TURBd01EQXdNREF3TUQvOEFBRVFnQ2lnUUFBd0VSQUFJUkFRTVJBZi9FQUw0QUFBSUNBd0VCQVFFQUFBQUFBQUFBQUFZSEJBVURDQWtDQVFvQUFRQUNBd0VCQVFFQUFBQUFBQUFBQUFBREJBQUJBZ1VHQndnUUFBRUNCUUlFQXdRSUJBTUZCZ1FDQ3dFQ0F3QVJJUVFGTVJKQlVXRUdjU0lUZ1RJVUIvQ1JvYkhCMFVJSTRWSWpGZkZpY3JJekpEUVdncUxDUTNNMWtsTTJDZEpqc3lVWDRvTjBrMFIxSmpjUkFBSUNBUVFDQVFNREF3UUJCQUlEQVFBQkVRSURJVEVTQkVFRkUxRmhJbkV5Qm9HUkZLR3hRaU5TOE1IUkZUTVc0WElrQi8vYUFBd0RBUUFDRVFNUkFEOEEvT2ZiT2hDd0NaZnhqeWZHVDVJRk5rbFR4QUFtSmFqNi93QUlvaGJKUG5DQUNWQWdVbHpIV0tJRmxwajNYR1ZGU0ZKQlFTQ2VSaUl0YmtPMWJGdGNDYXYvQUREcjRtSnNIV3dRMzF3bDYyVWhKQkpBRlBBeFhJc0FWZjBYWktvZVIrblNOMWFKNU0zcUZWUUp3YVV6RnorOWJaUGNKZU1XQjFKZGpjamNyU0MwQlhDQmtseENpbW9sL0NHcUF6d0ZnTEFqRjNCQ3B5YWtwS2lWQUdvbENtUzZEVlBtRzJsNEszQ1grUDV4ejhsMXlOa2pOa2x1UUV3T1B0alN1b0lMYTlJOVZIR1NoUDY1L2pHdWFJT0hzSEl0dE1xUXBTUVRTdkh6Q2tYeVRJSE9jZlM3Yk9FRUVGT3ZzK3FLc2FxTGUyVTM2Z1NWaWNCR2FoT3E5YXRiZE8xYVZLQmxMandnZGcwb0o4RVdiVkQxMnB4SUx6SzZHZkZCQTZRS0gvUW5KQ3R5Q1JkWHQ0cWNnSEZFSGdaQVFXcWduSkE4dDVURG0xSTNUTWlZcXlGc3JrWmZhNTNzZXNxaEJGUGIwZ1lzMmtNckJPRy95amRvQjcvRWVJSGp4ZzFBTnRXUGUzd2k4YmJKdUZCVzMrWTZVSDhZNkdJSlhZejNBSmFiVWticGthZUloeXUwQTdMVXM3Y0x0MHRxQ1Nkd0I4STJ0ekRSVWR6MnlyeENWZ0VsQ1JwL3BsQjFzQ1MxQkt5dDFuSFBza0VLVVpBY1RWVVdYZGZpQXVUdExpM1FwSHBMSUhHWDhla1lzOUJKN2kweXlubHBMUWFVU0R3bFA3NFd1VFVpV09KZmRLVkxiV2dUQnJBQ1BjUDhmYnJiU2xNaklDVTRuZ2dUSWJJUlhXV25XSUN1RWVDeHFyc2lVNXpvQjR3YWdKRnRjNGE4dDhoSkxEaWtDYzFTcHI0OVlib0dvV0xXUDJMRGkxYkRTWU0rRmVVTTEyMUIyM0dCa0xsdktZVm5IN2twOUZ0VWp6cFBoV0piWXk5aldqT1lHNWJ1YmowbWx1QlN6VVZFaU9zSlcvY0I4Z3ZiNEs5U3BSTERncVRvUHpqVnYyL2NLdGlZTVU4aGFWS1FzUzRmVkhPemVUZFMwWXhqcmpnWHRWTG43WVFlNGRJWXVHYStGU2duVVMxNlFPeFljVzE0MkVqekRUVDJlMk9aMmxKdXBUWm9vdlVnSVVDVWNCNHh4YjFZWVRmYzdUakIyaEJNeCtIM3dLSUdzSUpZUkx3dkcxcWJVQUZHc2h6NDE2UjZEMTdTaGp0Uzc3a3UwdHZMZHB1RzZTZWZHUFdkWi9qL0FFQ01oWXUyY3VBaTZVa3BTcmp3NS9qQWMzN2dGaWRtY2lpMnRBMmtoU3BLRzNqTTA0K01YamFrcEE3MlhqTHpKNVI1MXhoeHRDWHR3VVFBQ2tCSm5RbU94Z3RvSHJzYmRZV3h0MFdpWEhMaEtGTU5Ka0NUK21RNVE3a2ErTXNUdmZ6eHlIY0Z0YnNqZWxRS2R5ZEtCSThZODUzOWFzZ1lZd0RCNFVvV1pGTzNYV2lUemppZFJmOEFjLzFDNHR3UTdoeVl5Rmg2YmNpU1RRZFpDR2ZjT2NYOUIxYkRvK1RqMXBhcmxlWENMY0xhS1FWa2dUVWdwNEFuVXg4czlqUnR1QUdVTnU4c0ZhTkpOMnpkSWRENEsvS1ZFYW5tQnlqbmRXalFoY1V0dllGOFBsdnpoQlVDUndrcVBTZFhSQWpOYXR1SlB3WVFUNmhJbjdZNkNaTmkwdE1DOVo1QkQreFVrOGVHb01YNUlQckQ0OHF4N2R6WGNlRmVRUGh4aG5HbTJReFhGOTZhdzNJVFNvZmZLT3hnVGdwN0REd0Y5SzNYTUNyYXZ1aDZpQStSZWQxMnE3dHdsQ1NhazA4RERlTk9ReTIrd0dXbUxlUWhXNXRZTXpMNjRObS9ab2JydUNHY3NIUmNxOGl1UDN4eVArVFlkYkFsZE5QTkpNbTFHWGhGMjJNV1VrM0NJTHJ4RG5rMDEvaE9Bc1V5MVlmSXNFRktDbFlOQnpnMks2TDY2WmJXdU5DcERkQnJXa0xsb3dzc08wRVhRRDZuU2tKLzFmZ0k1MmVsbTlBS3hXYTBQZVV3N2RvMnNJZENwY2lUcDR3dDhWamtkckRlVCs3YVVnM1NXaW9CVlB0bkJLcG9YcFIxY0Z0ZTR1YjYzQVpoUjY4SndlclIxc0MwQnZOMmZwc2ExMkdmMnhzNitEY1JtY0phdUJJVDg1L0dMT3hqL2FXK0N5cmRvamN0U1U3VE16NFZpckpzMWJZSjdudkN6VTJVZXMxTThJMTE2L21DVzRvKzRNazFkdXViRkpNOUplMk9obS9iSnUyeDh3bDQ3ajErc2xzcUJseVBEcVk0SGJVNkY0aG5ZenV0NVlLUXhVRGttZW5qQ1dLckQzSzNQM3p1VGFVVk5GRzFNdEI0Y0REMVUwSzNGU3B2MGxxQi9tUDN3OWlFTXBaV2pnM0FSMk1MWEgrZ0FON0FwS0VpZldPZDJQM0FMN2xEbEgwTk9LQklBbkw2ekM2TU0rV2xvdDlKY2JTVlNHNm5LR3NXNEcyNWxZdjEyNzZHU2lSVXZiV1g1eDE4VDBNaFV0YWk0MFpVMnBPbk1DR2sxQkF4dGJsSnhpbWdRVmtqeThUUXhtMWtRRDhqWk91QlNsSVVsSjQ4SUR5SjRGeGZZNHRPclhXUituS0Izc29DVUtxMXlQd1Rxa29HOHFPMGlsSmdBNnloVzlpN2x4OGFzcVE2RWFlYVZJSElJT01VVGw3RndMSHBtVzBEaVpHWENjZEhya0lxTVQ4RzZFekpBOGVmV1VkQ3JBWEo0b3FYS1g0eHZram41ZHozYUVldWZaOWtUa2hPMjRWb2NEaktrZ2lhVUsrd1RqUzFabUpLVzBzVGVQT0lFNnJJcDR3MWpOSmF5VWZkZmJLN1FoZm1JMkJYSGltY0h5V1hBdkkxeCs0SVloSHBPcENxRFNaNGRESEU3VFRSemJWMUNHNmJiZm1sS3dlVWowcEhuT3o1RDRGcVhkcmpDbTJDcSs2ZWZBVGhDdXJPbnhZcXU1TDROT3JiTWhKVzJ2V2thdW1ib29BNUZ3aHQ1QzVnelVEQVlnS0dURjZ4ZDdHeTRrS0lTSmNUSURTS0tJV1lhRm0ycFNUdUFCTS9FUlQyRDFZQVBPWEYxUDAyVktDdUtRSkg3ZXNjdnNibXllMjQrMnlsQzJWSmtEcVA0eHpMZnVJaUkrZ0tRNG9tUkNWR1hnSWF4N0I2N0FReGxIVTNvUUdpWk9sSU1od256TU4xTnJjS3NsY1hEL3BvTENnQzJtc2gvS09zV0hXd1dkdVk1c1kvZXR3SklJb1o4Wm5oUFNEWWR5eXdRODIzZGVudUcwZnE5cDlzUDMvYVlzVmw0Nm4xVmxCQ3A4b1hxbWdQa2hzMndkS3lzN0pneW5UNlZnUFlYMElVT1R4aVNTZDAvci9qSE10VmtCaFpWWnZKU0J1Qk5UOXZHRXNyVmR5QkxiYmJ0bVpVRWt5KzcyeHJIZE1yUU1iWHVWR0F4cVVKMnJXajlKbE15QTV5NVIxOEYwWElSWVR1emN0TjJFQ2JrcEpwMWx4bHhoOTVhL0cxOWlES3g5ay8zT2xYcnRxdDIwcEtnc2lRSUEzVDhzNDRHVnptMElDUGNxYkx0dTJ1R0VYU0hWdm9VbmJNa2dtc3FnY282UFdxeUNReGRxN2ZQT3lRcVMzbkNDT3ExRVIzdXVvMVpHTlRGOXB1b3R3NDRGb1NKVEpueUo0UTFuYzBTUmkyeGQyVm1FWEl0d1pwRXZOWCtFY210R3JmMUFzNWd1NDVhVmxVaUpkVEhwT0IwZ3N3Nml3bElJMEdwRStIV0EycVE5SnZtbTcwRlN3SnVvbk9YOHdCbkdOVVFkYk4zWXF4aUZJY1NWZWhNeUExbEZGMTNGKzY2dys2cVRsUXRVcGVKNVJtelFkR1Z0ZnBrQlJwN1NKYWNmQ0F1eFpUWlpJVVZPdG1hak9ncHhpNjNSRDdqR1ZPaFBxQ1UveGhtbGpGaTF1Y1FsMUEyejUwbkIwd0ovV2VJQVVRWnpuTGpyQjhkV3dkd3J0c1k0eGF1eVNUTk02K01OVm93UUpYVDY3VndsWWxJbldGOHlaQU55V1RYZFhSUWd6U1o2ZU1jdk0yZzFDMXhUempaQklscFdjYzY3czJiTHk5dVduR0pLVU4walRXc2p4alZYWWd1MXRLdUxvcFNDUVhKUnRjaURPN2Z3eTJFcGRHNEFBS05TQjErK0dLSmtEdDh0dVdxMjFxa1NBT3RBWU02dURWUUQvdDYvakI2UUpUd016ekVCZFdIVDBNN3Rrc0svcWxTUUR4SmxTQjNyb1IyZ24zbVRjWnRHMlVHZzhzd1pVSkE4WXpXcFhNb2xPRVZUVXUrOTdhYXdScUNjd2h3WGFxY3FyMUhBYUtueHBXVVZBRzlnenZjV3pnYlJhV2pVQUdSOEo4VEdIU2RoZXpMUDVjM3kzYzFhdnJIOHV0ZjFBOG9KU3JSaEczZVd2MnJyQ05zSTIrb0NxZ0FCcWxNcWdkSWV4L1FOWFlIckJYcTdXbGZwSS9DRzZsdG9OamptaGJlbzZkc205eWFVUEg3NEpWYWdXMUlJT3Urb3B4dVFLWmxOUU5KeTR3WmFJeEtLajAyMmJ0RFFsdFVhMDhPSHRpdVNNMzFSVzkwV2FSYnVyYVFsUkU1QUpIV00yY29UdFhVVGh4VHoxd1hIR2FIcFRXRjdtUzArRHVFb0NFTUNTZU1nUHJwT0FrSmRzeVFvQXByTUEwNHhSQWpSanZXU0NCVUNJQnVHbmF0czh4ZHNvOVB5RlZTUnBVZU1Hb1pTazJEYndsaGRzYjNOZ2NQRGFKMUhPR3FCYXFFTEx1SEFscDV6MFVrcDRTcHhNTVZaaTFRZFV3cXlRaFEzVFVRa2drNkdRaTdXVUEzc1c3T0h0THByMUNFbFJTVktCU0pnd20vd0J3SHlZRyszN0paVWphbVpKSHVENnRJMWI5b1ZiSDgvOEFMNFhEYW5HbWlhY0VrZUVjN01icnVCTjlnN3pGT2xnMjhraWVvNWV5RUh1SFd4RFh2YlFDUkk4cWRJSFlzaHFmdWtBcVFsUm4xKzZPZjJGSnVoNng3OXdYRmhhZmVNak16NERuSEx0UU1aOGxoYlhJYlZ2SzJrY0pkSmNQR0ZiVkdzVUZBN2hMT3hTcFRaRXhVVWwxanI5SnhBOVFFaGd2NzVsME1yQ3ZSWE9aRStZbHA0eDZqclhpcHZ3SDJiN2R0OEgyK2tXbm5lUVYwa1owU25uT01aY2lrQllTbHZhdVpPL0xWenVTa0xUeFBPZWtWUzY1RklmdUd4Yk9JczIxc0lTU3RyekhhQWVJMWxIWHdYMERWMkl0M25WdEZiVGFpTjVLU0FaZlNzTzJ5ZmhCb3hZM0NYTisrbktGcFN3eVpsUm1SWHhueWppZHQ4a1FtOXhxY3VtSEdVQ1NqUHlpbkFqaExqSEp3TGhrYis0WEh1QlZoamkyc0p1TnlVMDFKSSsySjdPNnRqL29PcllPRzJ5eTAycHRhMEJCQ2dVS0taN1NEcUNPVWVDN09CM2JnQm1MYTU3MHZibGh1eVZNcGFUNlltWm1VelUvWENlUHF1bTRoY0srekZXeXJhOCtJV0VyY0Npa0dzeVZnOFlkeC9nQ2dLYlBCcFc2THBDWmxCSkZLYXc3anRKQmoyK0dSZDQwdnVvQ1hLVUNhMUJPc0ZSQzN4ZDAxYnRKc25GQktFOFRMaklmaEQrQ3NzaFR2NHBWMWVLV3dDdEpXQ0NKeWxPTzNncG9VOWc4czdGT1B0Z1h5V3lwdWs1MW5NY2ZDSGExQXZlRERZV0xGODZ2ZVFSdU1xVG1KNjhvWm9neTJMYTY3V2JTSnRJQkVwbW5NUk0zN0RkUmE1enRvRzRWTkV0ZUVjai9BSk1PdFFXZTdRYmNIdTY5RCtNYWFrMHFTVnA3TGRhVnZZYVVTZVV4OUtRdGROQWN1SmhGaWUzTGdraTRiVWdKOTBtZklSTWN2WUoxdXUzNENxMXdiU0ZBRW1ZTkJXRzZVc3h5M1Zzd25hVDhLMFdFQ1pVS1FldlZkdytMcFdhMkJMTkJ4SlVGZ2cxMWlyZFJwSEw3ZlRodlFYbVB5TnpaWnBUaFRKb2JmTk9udkdPZGx4OFdjUEppNDJEOXp1UzM5S1pjVHVrZVdzb0ZzeG5Fb0F1L3o2cnNyUVpiUk1EU0NLeU9wZzNRQVpPMGJ1VkZSTlprL2ZHNjJUWjJNZjdTaGR4SzF0cWJiM3pWUVNKSDNRVnhCZGdYdXUwOG9nbDVMYjJ3YW5ldVgzeGVCcmxvRHJ1RExyTHJGd1czTndVTlptY09abitQOURkdGd2c3JnRmxDS1RGTlB4amg5alV2RUYrRVJKWm1QZVAzZ0Q2aEMrS29lNFNaQzFTbGtrMG1pZkxXR2xYNkMxeFU1aGxEWld0UENmU0c4U09mbUtLeHZCdlNOMWZHT2pqdG9BQ3EyeWJ6YWdBS0NVSlozcUJzVm1XRnhlQUtiU1NyY0NaZjZnZUhTRjFaU0Rld1JZUEtyeDdTbTMwaE81QlQ1Z0RxSmNSRFdOZ0xibUIwSnVidHQ1RXBCemZUVGp5OFk2T084SW9ZRnBiS3VVSlVCUGFrQ25RUzRRd3I2RUxmRldlN0tNc3VURFpuT3BsN3lScHB4aXJXSXdyN3hzcld5d3EzYmNoVGczMGxMOUlnTHVRU2VWYUs3RkxtMGIxQXpBQTVRTzl6ZFJTcHRydEYwOHBUWjJsYzB6bnBJUXRheHF4ZXRQdkpSNWt5QUg1UktBdGkxeEhjRjh4Y3QyOXMwRnBXcXVuNVIxT3UwaURVS1gzN1EzVnczNmF4S25pRFBRZElhNUFMZ3M5ZWhLaUFvVDVmU2tYek9mbDNNOWc0VnVsWFByMGpTYkU3YmhQWSt2TlkyMFVDUHJFb1l4bExjSWNIYm0zdVVsUWx1Y21aMTFoMVdTcUVuUUlPOGJKdTV0aVVnRStrbW9BMTJEbEMyWExwQUs1cjNlMlM3ZEt2VFNkd0ovR09abnR5UXRFc28yTWsvYXYvQU5RRUpBMUovT09KMktqV0Ntb3kyTTR5YkJQblR1S1ZBaVEvbGwrTUpZcTZuUjQ2Q1c3bHQwMzc3aTJ6TWhlNGdlTUd2UXNDSG1YR2txM2dncEhscWVFTFhxUXByZkpYbHZjSmRTRmYwMUg5VkRXWEdrRDRrR05iM1RtZHNGS2VFbEtrUHJFWnN0QTFkajR5L2pzU2dOdU9wQzA2aFFCTlBHT1YyRFpFdkwxaDVDbkdpa2dpa2dBT21ncEhOc3BzUkMzdjh0Y3R1dU5vVE5KTzNob1pDY05ZMW9IcnNSc2N4dXVHM25CTHpoUnBwMWhsYkcxdUhHUnlGcWx0R3hTQ1V0cEdnL2xBTVhJZGJFT3h6RjQ4a01XNk55RkdrcWFReGczTENFV2I2V0JjdnBVZzhhbmtEMGpveE5RZG5KN2FzVXIvQUtreVppZkdCOFFSUlhsODYwNkdtMDBDd25ocFA2NFh5MGJJVzRzbDNOdVhGSk13aWNLdkdUL1lFTHF3UzRzaFZGQWtBZUZPa280UGRwYWRDSGkyYWR0blV0Z0hiUG4xSDV4T3ZpdXlGOWNkdG92Ylg0aHhTZ0ZhMVZMVGtES09uU3RxN2tMWHRySFd3dVUyOXc0VU10bEpDcTErM3BHYlptdnhKSTY3M3ZObkE0eHRqR2x0MVpaTGFxSkN2Tk5Pc2pNZ0dBVlR0a1RJSVBLTHZlNUwzMXJzTFFrTzdoSlJsS3ZBUzV4M2V0amNJcjdCZmhyS3p4V3haVW55MU14T3AxMWpzMHJGU3dzdSs4MnZobFdqSmJPNlVwSlNEUVM0RHJHbW5HcG0yeFIybVZmUzZIQ21sSzBnTnFvQjVPZVRtUVljZDlOS3AvU1VkMlVkSUsyTFJrV1NYMCs4Wi9jSURhR1FBL2hMbTd5TGlBbWFVdUpsOVlQS0EycXlERmNMMlBzRWcwbXpMVXdPMnhhM0E2eHZIbFBMVWVEaXZ0TUFiMUNxeUxxNHloQ2dDcXNvSGJZMzl6NDNkSmRvbzBnZGR5YkZtYjVxM1pTVzFlYWZoU1hpWWFvL0JpeGJZM0lQdm5hYWlWS25sRFZFMkJMOWhpN1c4MFVKbUN0TTlkSTZmWHJ0SUs0M2NkaVV2MnUxeFBtV2hJbExuTG5EZWlCaTU3MTdWRERMemlHNVNCT2tvVXl3eWJpVngySlV1N1FGSit6cUk1ZWJFM3NHcUd6K0krSHR3dENhbnA0ZmRDVnNEazJWcHhyanFUTlBBL2RPS1dCeVFqNHZFbjRoYWxJOXhjL3FrWUxYRTBRUEc4Z0xSc05FZ1VDWmVFTVZvUW83N0t1Slg1VmVRNjFnenBvV21TTEhJajB3NGxYbitoZ1R4aFZjbE5KdjhpK1VxVE5veUkxck1ucEE3ME0yc1lzaGlYa0lHNUZFMStxcCs2TVZvQjVsQ2wrM0RpVWJxb1VBclNrb3UxQ2N3Nnh2Y3JXTGIyTXVTVW9EaUJXaC9DTUtqWUs5eWMvbFY1bGxTN2d6RXVjNlFhdUlIeVFROW1Lc3JPN1pDRlNVSmNCd0k2OUlJc1RMUTZ6M0F3MkFDNUlkVC9HQzFvdzlkZ3d3YmpEc245MDVqY0RUZ0p3MVdzQTdibDVlNVorNFFHV2p1U2diVE04UFo0d1NVa1llMGdQa01vTWU4bExpdHU4elB0RStsREdYa1cza0Q1UERWMGk5ZVEreXJjQngraGdYUFVJOWdwUmFJdkdKUGp5blhqRTVDMS85UU15cU1iWkxVMkRMYjBTUHhnZHJJQ1VxTWxZS0lRMHVhakpKRk9OS1NKckF1UkFqc2NMYlhHMXhLWnFYVTA1L3hpdVNaQW1hN2ZmYUtBMjNReTRjUHFNVGtEc2cxeGVDdVcwQjVMZFVTUEg4dWtHcFkxV3BhZjNISk1YSVprUHJQTURsRFZiSUp4SkdVZVg4RUhuZmZPNmYxQTZ4dm1aYUZQbGNra2xTU3FXMHhoNWRBRnF5ZjJEemlsT0thVXJ5azdkZUVoQW5kU0E0dVJzNGJEZkhiWDJVN2dtU2xHWFA2K0ppV3pWNHdHVmRCajJOdjhPZ0ljRWtnVjlrSVpjaVpxdFlLWHVMdDdINUJoeDlLZHpwbkx5ZzZnbldjSk8yb1VSMlQ3Y0tIRk5sdVFIVHIvQ01XdEpHNEs0WUI3YVFsdVlrZnBwQ21WU2FwWWdmMk81WTlaZnB5Q1FWY1JvSVJ2UU9uSXY4cG1ubUxwREJNdlB0SW4wUDVRdGJISXppY0daMXdYREpVcXRCOVB0aHpCVjFIcVdSL1lSeEZ2ZHRsSnFKZmZIZjY5dngvb2E1S0FuN2l2R0hNZVN0WDlRN3BqaG9KUXZtdStRS3pGYlkyTFl2UzhSUlNnWnk2eHZGYVdaNWFqVVZkSmFzQW1jcE1rSjA2L1ZIV3hYaW9aVzBGZjZOOWQzMjlwRzV0THhKTXpwVWNvTGJQK01HdVE5dTFyaFRGZ2JCVkM2RWdqbnorK0VjdVJNbklyczlaTVdicTFVQ3hPVlBweGhHelNjaGNkbElHM0NVdkk4dnZHY0k5cXp1aDFXVUZ3emFYUnQya3FUNUZTU05lSmwrTWNpdUdYcUJ5dE11RWRucVMxOFNwcVc1SldETDZjWUZtd3BiSVJ2L0FLRWpEMmI0Y0liVFJ0UkIxcEkveGptWmF0QWh6NEM2UTAwR3JneUprSmZTVUd3TnJjZzE4VWxtNXR3eWlyYXBmZER0VkxJQzJVeHFtcjV4TGFmS05QclA0UjFldlF5N0xZTWUwbUVGNGg4VVNreXB5Qmp1NGFhRk95Z3o5enJYYyttMHpVTkVEd2tTZUhqRE95QjdzZzRVdU1Pb0JwTlZZeXN5VGdZclhRYmFIR2ZnVktKOHdTSmZWRlpzeWRJTjFxd0F2bUVYRGhXb1RKL0grTWN0V20zOVJtbFFNdWt2cHVWTk1pZzA0Y1pjb1BLR2ExK3BaNHg5S1ZsRnlRTm84WmZYQU1tcHExVXkrWlZhWEN0cVRNemxvT1B0aldDbzMxYVZXNW51TVFvSjlSdEdnQ3ZaSFd4WXVSMUZpcS9BUDNGdmZKWDZxVWlTUEhoN0k3Zlc2NmpWRDJMSFdxMkt1NXQzTDFXNjRGVE9mSFdOWnVzb2NJNVBkNjZ2TUlHNzN0NWd6VUUxOEIrYzQ4djNNVVdqN25sTzEwM3pCVzh4TExFeXNFQVQrbjFRaDhGbnFacDFMQVBmM3RqYUtJM3lNNUFVOE9jVjhOa1BZY0RUMUswWFl1S3RIY0I5M3NpVnBaTTZkYXhVOXRYYnpUeWVBQjYvbHhnMXRLbWJQd0Y3bWF0emkxdHJYL1ZNcENuSlUrTUN3dUxnMDlSVWY5UDNlYXl5aXkzdlN1VXFIaVQwaDdOYmxYVDZHMjVNdVQ3ZXZNTzJGcmEyY09QRDJDT1piQzdNSmlyQjd3MlFlUXMrcVpKVFhYaEtLcjEyZ3RpMnl2Y3pialhwdE9BcUNkdW8xRk9mT0NySHgzRnJnU1UzZVJTdHRLZHlsa3lFeld2aERHTkNPUlNZMnUxN3l6VDhTKzF0U25VeU5KMTRpR1VvVUFIVndZVGs3SmgwdHJYSWp3L09GOGxHd05xc25NOXdZdHIzblpmL0FBL25DL3h2bG9DWmh1TWxZM3FoNmJrd0NEdy9BdzVqeHVCZTI1ZFl0NjIzSkc2a3hQVDg1dzFTcktHcmk3MjBaWUtVcjh4QUlGUHpnNldoQ0tyS3VNNUZKYk1wVGxXWEVSaS8wS2tsWmZMM0Y1YXFhZlA5TXpuVW5oWFdBRmN3VWNhWGNOK21CTkEwNHdPN04wc1VkM1pzdHBKV0pIaFFhd0ZzdTlnVXVRNHBSYmFydU8wRC9DTHJkSUZ5R0QyVDIyMzZDcjIrUnRVMHJjREtkQ284NWNJZnc1RVh5UmZkd1pkQ1ZLdHJaVTJqUHBwVFFVaGxaSkFYc0xzYmwzSjMrNXo5dkNnaTFmVVJ5T1EvdzJOTG14U1JNRWorTU5VMUU3YjZEY3hXQnQxTkFsUG1DWm1uRVEzUmFTWkltUXRoWkxDa2lXMHpHa2J0YjhTRlJjNVJWd3l0QzFjTm9yUFNuNFFsa2JNT3d1Y2lHdlZLU2ZNWjhCemhXK3BpcWxnOWtlM2pjV3BmUTNPYzZ5NlFqbnBLME9oZ3JERmhrTXJkNDVTcmNIYUVjSm53aFRIamhqMXRnVS82a2ZiZTg2NkxVQnJ6cEJiMEJGdGRPSnZMWXVwcVFqY2ZIMmVNTDJ4eVFIc2F5aTVXcERsWnJVTk9TaUlIOFJFRXIxei9BR1prdFc1bEwyYUExZ09URTRZYWdJUFcxem1IeTg4bmNoZXBCSjFQMnh4K3hSeWJDeTN4amJWcWhFdEFScDA1ZElTZU9XUmFNcVhzVFpPS1daVFVLNkQ4NEpXc0I2N0FwbVBpYkFTdHhJRVUxRUcyUnVwVVlsTjlrWFFtNFROQlVRZFRTY3RDSUVucUhRNXNCaTdHeUtBcWhvZEIrY09ZSHFVd3F5eFpldEZNdEdaTS93QW82bFhLQk5NaWpFM2pObWw3WkpDZ1FEWDhvS3NjbFFDam1FdTMzbHVKYm50VnUrcXM5SXhmRVh4SmE3eDZ5Q2Jaenkrb0FpVTlla0szeGZRbkZtSnpITmJTKzRKSDNoN2F4eXV4MVhkbE5Sb1ZseGFPcmFWY01KbUUwbnB3UEgyUTExZWsrT3hST1JrTGhPTERLdFJ3bi9sRUV5OVZwTWdIcXpqakQ2a2hVbENockhDelliTEp2b1F6cXphSkJUNjVUMHIvQUJoM3I0aUYxak1vYnFZYU01UjM4Q1NSQzNlWnZuNU5vVFBlT1ovS09oV3lnbTVOWTdiK0Z0bFh0MmpZcEhHWE9aUExsRWJSaTJ4WG95Q25uL2hiVXo1Q2ZNeUdrQmFrQ2FRTjRRSmIrSkNhODVlMk9wSjFlSmplend0MEMwM2tGSjBuemtQd2lHWHB1RkdFdFVPQTNVcXJRcFUvK3lZeXlpSGs3aTZ1WEF5RlRRbFcyWFNBWEpHc0dXMXhhV215U256S0UvYWFtRm51RXFpdXZMT1NpU05PbjBwR0xCTmlSWTJMandHMGZaQTFvelBMd1FzaWh5M1VVSzFCL2hET055WnM1RGJ0ZENWbHZlSnprUHdqb1k2d0RObU96KzE3ZTlhVTg0MEZCQ2Q0b09BbjlzZERFNE1XVW1idUc3WnhOdzB6Ym5ab2sxNUovTVJ1OXdmQUJlNGNqOFZhdUljVk1xVFQ2b1Z2Y3ZpTGJHV0RhcmxLZ25sOThENUtBaVVCWmNXN1hwQkRpYWVIMOKApik7fVxyXG4udHZfdmlkZW97aGVpZ2h0OiAzMHZoO3dpZHRoOiAxMDAlOyBvYmplY3QtZml0OiBjb3Zlcjt9XHJcblxyXG4uaWIge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrIWltcG9ydGFudDtcclxuXHJcbn1cclxuLnc2MHtcclxuICAgIHdpZHRoOiA2MCUhaW1wb3J0YW50O1xyXG59XHJcbi53NDB7XHJcbiAgICB3aWR0aDogNDAlIWltcG9ydGFudDtcclxufVxyXG4ubm8tbWFyZ2luIHtcclxuICAgIG1hcmdpbjogYXV0byFpbXBvcnRhbnQ7XHJcbn0iXX0= */");

/***/ }),

/***/ "fMGI":
/*!***********************************************!*\
  !*** ./src/app/profile/profile.component.css ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9maWxlLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "in5m":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"full-screen-container\">\n    <div class=\"login-container\">\n      <h3 class=\"login-title\">Welcome</h3>\n      <form [formGroup]=\"userData\">\n        <div class=\"input-group\">\n          <label>Email</label>\n          <input formControlName=\"email\" type=\"email\" />\n        </div>\n        <div class=\"input-group\" *ngIf=\"!forgotPassword\">\n          <label>Password</label>\n          <input formControlName=\"password\" type=\"password\" />\n        </div>\n        \n        <button type=\"button\" (click)=\"forgotPassword ? null : loginUser()\" class=\"login-button\">{{forgotPassword ? 'Send link' : 'Log In'}}</button>\n        \n        <button type=\"button\" (click)=\"forgotPassword = true\" class=\"login-button mt-2\">Forgot Password</button>\n      </form>\n    </div>\n  </div>");

/***/ }),

/***/ "jGP6":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/footer/footer.component.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<footer class=\"footer\">\n    <div class=\"container-fluid\">\n        <nav class=\"pull-left\">\n            <ul>\n                <li>\n                    <a href=\"#\">\n                        Home\n                    </a>\n                </li>\n                <li>\n                    <a href=\"#\">\n                        Company\n                    </a>\n                </li>\n                <li>\n                    <a href=\"#\">\n                        Portfolio\n                    </a>\n                </li>\n                <li>\n                    <a href=\"#\">\n                        Blog\n                    </a>\n                </li>\n            </ul>\n        </nav>\n        <p class=\"copyright pull-right\">\n            &copy; {{test | date: 'yyyy'}} <a href=\"javascript:void(0)\">Nitish team</a>, made with love for a better web\n        </p>\n    </div>\n</footer>\n");

/***/ }),

/***/ "jQpT":
/*!***************************************************!*\
  !*** ./src/app/shared/footer/footer.component.ts ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FooterComponent = void 0;
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
        this.test = new Date();
    }
    FooterComponent = __decorate([
        core_1.Component({
            selector: 'footer-cmp',
            template: __webpack_require__(/*! raw-loader!./footer.component.html */ "jGP6").default
        })
    ], FooterComponent);
    return FooterComponent;
}());
exports.FooterComponent = FooterComponent;


/***/ }),

/***/ "kMBp":
/*!****************************************************!*\
  !*** ./src/app/my-profile/my-profile.component.ts ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MyProfileComponent = void 0;
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var MyProfileComponent = /** @class */ (function () {
    function MyProfileComponent() {
    }
    MyProfileComponent.prototype.ngOnInit = function () {
    };
    MyProfileComponent.ctorParameters = function () { return []; };
    MyProfileComponent = __decorate([
        core_1.Component({
            selector: 'app-my-profile',
            template: __webpack_require__(/*! raw-loader!./my-profile.component.html */ "7/Ie").default,
            styles: [__webpack_require__(/*! ./my-profile.component.css */ "0rzt").default]
        }),
        __metadata("design:paramtypes", [])
    ], MyProfileComponent);
    return MyProfileComponent;
}());
exports.MyProfileComponent = MyProfileComponent;


/***/ }),

/***/ "lt5q":
/*!*********************************************!*\
  !*** ./src/app/resize/resize.component.css ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".resizable-draggable {\r\n    outline: 1px dashed green;\r\n  }\r\n  .resizable-draggable.active {\r\n    outline-style: solid;\r\n    background-color: #80ff800d;\r\n  }\r\n  .resizable-draggable:hover {\r\n    cursor: all-scroll;\r\n  }\r\n  .resizable-draggable span:first-of-type {\r\n    position: absolute;\r\n    left: 50%;\r\n    transform: translate3d(-50%, -100%, 0);\r\n  }\r\n  .resizable-draggable span:nth-of-type(2) {\r\n    position: absolute;\r\n    top: 50%;\r\n    transform: translate3d(-100%, -50%, 0);\r\n  }\r\n  .resizable-draggable span:nth-of-type(3) {\r\n    position: absolute;\r\n    transform: translate3d(-100%, -100%, 0);\r\n  }\r\n  .resize-action {\r\n    position: absolute;\r\n    left: 100%;\r\n    top: 100%;\r\n    transform: translate3d(-50%, -50%, 0) rotateZ(45deg);\r\n    border-style: solid;\r\n    border-width: 8px;\r\n    border-color: transparent transparent transparent #008000;\r\n  }\r\n  .resize-action:hover, .resize-action:active {\r\n    cursor: nwse-resize;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlc2l6ZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0kseUJBQXlCO0VBQzNCO0VBQ0E7SUFDRSxvQkFBb0I7SUFDcEIsMkJBQTJCO0VBQzdCO0VBQ0E7SUFDRSxrQkFBa0I7RUFDcEI7RUFDQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1Qsc0NBQXNDO0VBQ3hDO0VBQ0E7SUFDRSxrQkFBa0I7SUFDbEIsUUFBUTtJQUNSLHNDQUFzQztFQUN4QztFQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLHVDQUF1QztFQUN6QztFQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixTQUFTO0lBQ1Qsb0RBQW9EO0lBQ3BELG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIseURBQXlEO0VBQzNEO0VBQ0E7SUFDRSxtQkFBbUI7RUFDckIiLCJmaWxlIjoicmVzaXplLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucmVzaXphYmxlLWRyYWdnYWJsZSB7XHJcbiAgICBvdXRsaW5lOiAxcHggZGFzaGVkIGdyZWVuO1xyXG4gIH1cclxuICAucmVzaXphYmxlLWRyYWdnYWJsZS5hY3RpdmUge1xyXG4gICAgb3V0bGluZS1zdHlsZTogc29saWQ7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjODBmZjgwMGQ7XHJcbiAgfVxyXG4gIC5yZXNpemFibGUtZHJhZ2dhYmxlOmhvdmVyIHtcclxuICAgIGN1cnNvcjogYWxsLXNjcm9sbDtcclxuICB9XHJcbiAgLnJlc2l6YWJsZS1kcmFnZ2FibGUgc3BhbjpmaXJzdC1vZi10eXBlIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IDUwJTtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlM2QoLTUwJSwgLTEwMCUsIDApO1xyXG4gIH1cclxuICAucmVzaXphYmxlLWRyYWdnYWJsZSBzcGFuOm50aC1vZi10eXBlKDIpIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogNTAlO1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgtMTAwJSwgLTUwJSwgMCk7XHJcbiAgfVxyXG4gIC5yZXNpemFibGUtZHJhZ2dhYmxlIHNwYW46bnRoLW9mLXR5cGUoMykge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgtMTAwJSwgLTEwMCUsIDApO1xyXG4gIH1cclxuICBcclxuICAucmVzaXplLWFjdGlvbiB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBsZWZ0OiAxMDAlO1xyXG4gICAgdG9wOiAxMDAlO1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgtNTAlLCAtNTAlLCAwKSByb3RhdGVaKDQ1ZGVnKTtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBib3JkZXItd2lkdGg6IDhweDtcclxuICAgIGJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQgdHJhbnNwYXJlbnQgdHJhbnNwYXJlbnQgIzAwODAwMDtcclxuICB9XHJcbiAgLnJlc2l6ZS1hY3Rpb246aG92ZXIsIC5yZXNpemUtYWN0aW9uOmFjdGl2ZSB7XHJcbiAgICBjdXJzb3I6IG53c2UtcmVzaXplO1xyXG4gIH0iXX0= */");

/***/ }),

/***/ "n7sk":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@import url('https://fonts.googleapis.com/css2?family=Quicksand:wght@300;500&display=swap');\r\n\r\n* {\r\n  box-sizing: border-box;\r\n  font-family: 'Quicksand', sans-serif;\r\n}\r\n\r\nhtml,\r\nbody {\r\n  margin: 0;\r\n}\r\n\r\n.full-screen-container {\r\n  /* background-image: url('https://images.unsplash.com/photo-1573496782646-e8d943a4bdd1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1650&q=80'); */\r\n  height: 100vh;\r\n  width: 100vw;\r\n  background-size: cover;\r\n  background-position: center;\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: center;\r\n}\r\n\r\n.login-container {\r\n  background-color: hsla(201, 100%, 6%, 0.6);\r\n  padding: 50px 30px;\r\n  min-width: 400px;\r\n  width: 50%;\r\n  max-width: 600px;\r\n}\r\n\r\n.login-title {\r\n  color: #fff;\r\n  text-align: center;\r\n  margin: 0;\r\n  margin-bottom: 40px;\r\n  font-size: 2.5em;\r\n  font-weight: normal;\r\n}\r\n\r\n.input-group {\r\n  display: flex;\r\n  flex-direction: column;\r\n  margin-bottom: 20px;\r\n}\r\n\r\n.input-group label {\r\n  color: #fff;\r\n  font-weight: lighter;\r\n  font-size: 1.5em;\r\n  margin-bottom: 7px;\r\n}\r\n\r\n.input-group input {\r\n  font-size: 1.5em;\r\n  padding: 0.1em 0.25em;\r\n  background-color: hsla(201, 100%, 91%, 0.3);\r\n  border: 1px solid hsl(201, 100%, 6%);\r\n  outline: none;\r\n  border-radius: 5px;\r\n  color: #fff;\r\n  font-weight: lighter;\r\n}\r\n\r\n.input-group input:focus {\r\n  border: 1px solid hsl(201, 100%, 50%);\r\n}\r\n\r\n.login-button {\r\n  padding: 10px 30px;\r\n  width: 100%;\r\n  border-radius: 5px;\r\n  background: hsla(201, 100%, 50%, 0.1);\r\n  border: 1px solid hsl(201, 100%, 50%);\r\n  outline: none;\r\n  font-size: 1.5em;\r\n  color: #fff;\r\n  font-weight: lighter;\r\n  margin-top: 20px;\r\n  cursor: pointer;\r\n}\r\n\r\n.login-button:hover {\r\n  background-color: hsla(201, 100%, 50%, 0.3);\r\n}\r\n\r\n.login-button:focus {\r\n  background-color: hsla(201, 100%, 50%, 0.5);\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsMkZBQTJGOztBQUUzRjtFQUNFLHNCQUFzQjtFQUN0QixvQ0FBb0M7QUFDdEM7O0FBRUE7O0VBRUUsU0FBUztBQUNYOztBQUVBO0VBQ0UsNkxBQTZMO0VBQzdMLGFBQWE7RUFDYixZQUFZO0VBQ1osc0JBQXNCO0VBQ3RCLDJCQUEyQjtFQUMzQixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtBQUN6Qjs7QUFFQTtFQUNFLDBDQUEwQztFQUMxQyxrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLFVBQVU7RUFDVixnQkFBZ0I7QUFDbEI7O0FBRUE7RUFDRSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0UsV0FBVztFQUNYLG9CQUFvQjtFQUNwQixnQkFBZ0I7RUFDaEIsa0JBQWtCO0FBQ3BCOztBQUVBO0VBQ0UsZ0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQiwyQ0FBMkM7RUFDM0Msb0NBQW9DO0VBQ3BDLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsV0FBVztFQUNYLG9CQUFvQjtBQUN0Qjs7QUFFQTtFQUNFLHFDQUFxQztBQUN2Qzs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLHFDQUFxQztFQUNyQyxxQ0FBcUM7RUFDckMsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixXQUFXO0VBQ1gsb0JBQW9CO0VBQ3BCLGdCQUFnQjtFQUNoQixlQUFlO0FBQ2pCOztBQUVBO0VBQ0UsMkNBQTJDO0FBQzdDOztBQUVBO0VBQ0UsMkNBQTJDO0FBQzdDIiwiZmlsZSI6ImxvZ2luLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IHVybCgnaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3MyP2ZhbWlseT1RdWlja3NhbmQ6d2dodEAzMDA7NTAwJmRpc3BsYXk9c3dhcCcpO1xyXG5cclxuKiB7XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICBmb250LWZhbWlseTogJ1F1aWNrc2FuZCcsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbmh0bWwsXHJcbmJvZHkge1xyXG4gIG1hcmdpbjogMDtcclxufVxyXG5cclxuLmZ1bGwtc2NyZWVuLWNvbnRhaW5lciB7XHJcbiAgLyogYmFja2dyb3VuZC1pbWFnZTogdXJsKCdodHRwczovL2ltYWdlcy51bnNwbGFzaC5jb20vcGhvdG8tMTU3MzQ5Njc4MjY0Ni1lOGQ5NDNhNGJkZDE/aXhsaWI9cmItMS4yLjEmaXhpZD1Nbnd4TWpBM2ZEQjhNSHh3YUc5MGJ5MXdZV2RsZkh4OGZHVnVmREI4Zkh4OCZhdXRvPWZvcm1hdCZmaXQ9Y3JvcCZ3PTE2NTAmcT04MCcpOyAqL1xyXG4gIGhlaWdodDogMTAwdmg7XHJcbiAgd2lkdGg6IDEwMHZ3O1xyXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuLmxvZ2luLWNvbnRhaW5lciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogaHNsYSgyMDEsIDEwMCUsIDYlLCAwLjYpO1xyXG4gIHBhZGRpbmc6IDUwcHggMzBweDtcclxuICBtaW4td2lkdGg6IDQwMHB4O1xyXG4gIHdpZHRoOiA1MCU7XHJcbiAgbWF4LXdpZHRoOiA2MDBweDtcclxufVxyXG5cclxuLmxvZ2luLXRpdGxlIHtcclxuICBjb2xvcjogI2ZmZjtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIG1hcmdpbi1ib3R0b206IDQwcHg7XHJcbiAgZm9udC1zaXplOiAyLjVlbTtcclxuICBmb250LXdlaWdodDogbm9ybWFsO1xyXG59XHJcblxyXG4uaW5wdXQtZ3JvdXAge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcblxyXG4uaW5wdXQtZ3JvdXAgbGFiZWwge1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xyXG4gIGZvbnQtc2l6ZTogMS41ZW07XHJcbiAgbWFyZ2luLWJvdHRvbTogN3B4O1xyXG59XHJcblxyXG4uaW5wdXQtZ3JvdXAgaW5wdXQge1xyXG4gIGZvbnQtc2l6ZTogMS41ZW07XHJcbiAgcGFkZGluZzogMC4xZW0gMC4yNWVtO1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGhzbGEoMjAxLCAxMDAlLCA5MSUsIDAuMyk7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgaHNsKDIwMSwgMTAwJSwgNiUpO1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xyXG59XHJcblxyXG4uaW5wdXQtZ3JvdXAgaW5wdXQ6Zm9jdXMge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkIGhzbCgyMDEsIDEwMCUsIDUwJSk7XHJcbn1cclxuXHJcbi5sb2dpbi1idXR0b24ge1xyXG4gIHBhZGRpbmc6IDEwcHggMzBweDtcclxuICB3aWR0aDogMTAwJTtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgYmFja2dyb3VuZDogaHNsYSgyMDEsIDEwMCUsIDUwJSwgMC4xKTtcclxuICBib3JkZXI6IDFweCBzb2xpZCBoc2woMjAxLCAxMDAlLCA1MCUpO1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxLjVlbTtcclxuICBjb2xvcjogI2ZmZjtcclxuICBmb250LXdlaWdodDogbGlnaHRlcjtcclxuICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuLmxvZ2luLWJ1dHRvbjpob3ZlciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogaHNsYSgyMDEsIDEwMCUsIDUwJSwgMC4zKTtcclxufVxyXG5cclxuLmxvZ2luLWJ1dHRvbjpmb2N1cyB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogaHNsYSgyMDEsIDEwMCUsIDUwJSwgMC41KTtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "oYre":
/*!******************************************!*\
  !*** ./src/app/users/users.component.ts ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersComponent = void 0;
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var UsersComponent = /** @class */ (function () {
    function UsersComponent() {
    }
    UsersComponent.prototype.ngOnInit = function () {
    };
    UsersComponent.ctorParameters = function () { return []; };
    UsersComponent = __decorate([
        core_1.Component({
            selector: 'app-users',
            template: __webpack_require__(/*! raw-loader!./users.component.html */ "sgLn").default,
            styles: [__webpack_require__(/*! ./users.component.css */ "Tu9x").default]
        }),
        __metadata("design:paramtypes", [])
    ], UsersComponent);
    return UsersComponent;
}());
exports.UsersComponent = UsersComponent;


/***/ }),

/***/ "owvD":
/*!********************************************************!*\
  !*** ./src/app/parking-area/parking-area.component.ts ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParkingAreaComponent = void 0;
var http_1 = __webpack_require__(/*! @angular/common/http */ "tk/3");
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var resize_component_1 = __webpack_require__(/*! app/resize/resize.component */ "dRrA");
var environment_1 = __webpack_require__(/*! environments/environment */ "AytR");
var ngx_toastr_1 = __webpack_require__(/*! ngx-toastr */ "EApP");
var ParkingAreaComponent = /** @class */ (function () {
    function ParkingAreaComponent(http, toastr) {
        this.http = http;
        this.toastr = toastr;
        this.parkingAreaDetails = {
            address: '',
            image: '',
            imageTemp: ''
        };
        this.points = [];
        this.pointsTemp = [];
        this.imageSrc = '';
        this.parkings = [];
        this.top = 100;
        this.left = 100;
        this.height = 100;
        this.width = 100;
        this.markingadding = false;
        this.getCameras();
    }
    ParkingAreaComponent.prototype.ngOnInit = function () {
    };
    ParkingAreaComponent.prototype.saveMarkings = function () {
        console.log(this.resizer);
        console.log(this.resizer.left, this.resizer.top, this.resizer.width, this.resizer.height);
        this.addPoint(this.resizer.left, this.resizer.top);
        this.addPoint(this.resizer.left + this.resizer.width, this.resizer.top);
        this.addPoint(this.resizer.left + this.resizer.width, this.resizer.top + this.resizer.height);
        this.addPoint(this.resizer.left, this.resizer.top + this.resizer.height);
        this.markingadding = false;
    };
    ParkingAreaComponent.prototype.addPoint = function (x, y) {
        var c = document.getElementById("myCanvas");
        console.log("Coordinate x: " + x, "Coordinate y: " + y);
        var ctx = c.getContext("2d");
        ctx.strokeStyle = '#000000';
        ctx.fillRect(x - 2, y - 2, 4, 4);
        this.pointsTemp.push([x, y]);
        if (this.pointsTemp.length > 3) {
            this.points.push(this.pointsTemp);
            this.pointsTemp = [];
            this.drawPoints();
        }
    };
    ParkingAreaComponent.prototype.drawPoints = function () {
        var c = document.getElementById("myCanvas");
        var ctx = c.getContext("2d");
        for (var _i = 0, _a = this.points; _i < _a.length; _i++) {
            var box = _a[_i];
            ctx.beginPath();
            ctx.moveTo.apply(ctx, box[0]); // es5 friendly
            ctx.lineTo.apply(// es5 friendly
            ctx, box[1]); // es6+
            ctx.lineTo.apply(// es6+
            ctx, box[2]);
            ctx.lineTo.apply(ctx, box[3]);
            ctx.lineTo.apply(ctx, box[0]);
            ctx.fillStyle = '#000000';
            ctx.stroke();
        }
        // let c: any = document.getElementById("myCanvas");
        //     let ctx = c.getContext("2d");
        //     let img = document.getElementById("imagePlacer");
        //     ctx.drawImage(img, 10, 10);
    };
    ParkingAreaComponent.prototype.onFileChange = function (event) {
        var _this = this;
        var reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            var file = event.target.files[0];
            this.parkingAreaDetails.image = event.target.files.item(0);
            reader.readAsDataURL(file);
            reader.onload = function () {
                _this.imageSrc = reader.result;
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                var img = document.getElementById("imagePlacer");
                console.log(img);
                setTimeout(function () {
                    ctx.drawImage(img, 0, 0, 500, 300);
                }, 2000);
            };
        }
    };
    ParkingAreaComponent.prototype.addCamera = function () {
        var _this = this;
        var headers = new http_1.HttpHeaders();
        headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
        // if(this.parkingAreaDetails.id) {
        //   const formData = new FormData();
        //   formData.append('address', this.parkingAreaDetails.address);
        //   formData.append('image', this.parkingAreaDetails.image, this.parkingAreaDetails.image.name);
        //   this.http.put(apiEndpoint + 'car/add_parking_map/'+this.parkingAreaDetails.id, this.parkingAreaDetails, {headers: headers}).subscribe(res => {
        //     this.getCameras();
        //     this.parkingAreaDetails = {
        //       address: '',
        //       image: '',
        //       imageTemp: ''
        //     }
        //   });
        //   return;
        // }
        var formData = new FormData();
        formData.append('address', this.parkingAreaDetails.address);
        formData.append('image', this.parkingAreaDetails.image);
        if (!this.parkingAreaDetails.address) {
            this.toastr.success('Please add address first!');
            return;
        }
        if (!this.parkingAreaDetails.image) {
            this.toastr.success('Please Select first!');
            return;
        }
        if (!(this.points && this.points.length > 0)) {
            this.toastr.success('Please add markings to proceed!');
            return;
        }
        this.http.post(environment_1.apiEndpoint + 'car/add_parking_map', formData, { headers: headers }).subscribe(function (res) {
            var payload = {
                id: res.data.id,
                placement: JSON.stringify({
                    points: _this.points,
                    resolution: [500, 300]
                })
            };
            _this.http.put(environment_1.apiEndpoint + 'car/update_parking_map', payload, { headers: headers }).subscribe(function (res) {
            });
            _this.getCameras();
            _this.parkingAreaDetails = {
                address: '',
                image: '',
                imageTemp: ''
            };
        });
    };
    ParkingAreaComponent.prototype.delete = function () {
        var _this = this;
        var headers = new http_1.HttpHeaders();
        headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
        if (this.parkingAreaDetails.id) {
            this.http.delete(environment_1.apiEndpoint + 'camera/delete_camera/' + this.parkingAreaDetails.id, { headers: headers }).subscribe(function (res) {
                _this.getCameras();
                _this.parkingAreaDetails = {
                    address: '',
                    image: '',
                    imageTemp: ''
                };
            });
            return;
        }
    };
    ParkingAreaComponent.prototype.getCameras = function () {
        var _this = this;
        var headers = new http_1.HttpHeaders();
        headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
        this.http.get(environment_1.apiEndpoint + 'car/parking_map', { headers: headers }).subscribe(function (res) {
            _this.parkings = res.data;
        });
    };
    ParkingAreaComponent.ctorParameters = function () { return [
        { type: http_1.HttpClient },
        { type: ngx_toastr_1.ToastrService }
    ]; };
    ParkingAreaComponent.propDecorators = {
        resizer: [{ type: core_1.ViewChild, args: [resize_component_1.ResizableDraggableComponent,] }]
    };
    ParkingAreaComponent = __decorate([
        core_1.Component({
            selector: 'app-parking-area',
            template: __webpack_require__(/*! raw-loader!./parking-area.component.html */ "0bGh").default,
            styles: [__webpack_require__(/*! ./parking-area.component.css */ "dL06").default]
        }),
        __metadata("design:paramtypes", [http_1.HttpClient, ngx_toastr_1.ToastrService])
    ], ParkingAreaComponent);
    return ParkingAreaComponent;
}());
exports.ParkingAreaComponent = ParkingAreaComponent;


/***/ }),

/***/ "sgLn":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/users/users.component.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-6\">\n                <form>\n\n                    <fieldset>\n                        <label for=\"firstName\">Id</label>\n                        <input type=\"text\" name=\"name\" id=\"firstName\" placeholder=\"Id\">\n                    </fieldset>\n                    \n\n                    <fieldset>\n                        <label for=\"name\">Name</label>\n                        <input type=\"text\" name=\"name\" id=\"name\" placeholder=\"Name\">\n                    </fieldset>\n                    \n                    <fieldset>\n                        <label for=\"status\">Status</label>\n                        <select name=\"select-choice\" id=\"status\">\n                            <option value=\"Choice 1\">- - select one - -</option>\n                            <option value=\"Choice 2\">Yes!</option>\n                            <option value=\"Choice 3\">They're kinda cool, yeah.</option>\n                            <option value=\"Choice 4\">Meh... not really.</option>\n                            <option value=\"Choice 5\">What's Transformers?</option>\n                        </select>\n                    </fieldset>\n                    <fieldset>\n                        <label for=\"select-choice\">Type</label>\n                        <select name=\"select-choice\" id=\"select-choice\">\n                            <option value=\"Choice 1\">- - select one - -</option>\n                            <option value=\"Choice 2\">Yes!</option>\n                            <option value=\"Choice 3\">They're kinda cool, yeah.</option>\n                            <option value=\"Choice 4\">Meh... not really.</option>\n                            <option value=\"Choice 5\">What's Transformers?</option>\n                        </select>\n                    </fieldset>\n                    <fieldset>\n                        <label for=\"textarea\">Password</label>\n                        <input name=\"textarea\" type=\"password\" id=\"textarea\" placeholder=\"Image\">\n                    </fieldset>\n\n                    <!--                   \n                    <fieldset>\n                      <legend>Group 1</legend>\n                      <input type=\"checkbox\" id=\"check1\" name=\"checkboxes\" checked>\n                      <label for=\"check1\">Checkbox 1</label><br>\n                      <input type=\"checkbox\" id=\"check2\" name=\"checkboxes\">\n                      <label for=\"check2\">Checkbox 2</label><br>\n                      <input type=\"checkbox\" id=\"check3\" name=\"checkboxes\">\n                      <label for=\"check3\">Checkbox 3</label>\n                    </fieldset>\n                    \n                    <fieldset>\n                      <legend>Group 2</legend>\n                      <input type=\"radio\" id=\"radio1\" name=\"radios\" checked>\n                      <label for=\"radio1\">Radio 1</label><br>\n                      <input type=\"radio\" id=\"radio2\" name=\"radios\">\n                      <label for=\"radio2\">Radio 2</label><br>\n                      <input type=\"radio\" id=\"radio3\" name=\"radios\">\n                      <label for=\"radio3\">Radio 3</label>\n                    </fieldset>\n                  \n                    <fieldset>\n                      <label for=\"select-choice\">Transformers fan?</label>\n                      <select name=\"select-choice\" id=\"select-choice\">\n                        <option value=\"Choice 1\">- - select one - -</option>\n                        <option value=\"Choice 2\">Yes!</option>\n                        <option value=\"Choice 3\">They're kinda cool, yeah.</option>\n                        <option value=\"Choice 4\">Meh... not really.</option>\n                        <option value=\"Choice 5\">What's Transformers?</option>\n                      </select>\n                    </fieldset>\n                   -->\n                    <!-- <button>Submit</button> -->\n                </form>\n            </div>\n\n            <div class=\"col-md-6\">\n                <div class=\"rightBox\">\n            \n                    <fieldset>\n                        <legend>Access</legend>\n                        <input type=\"checkbox\" id=\"check1\" name=\"checkboxes\" checked>\n                        <label for=\"check1\">Checkbox 1</label><br>\n                        <input type=\"checkbox\" id=\"check2\" name=\"checkboxes\">\n                        <label for=\"check2\">Checkbox 2</label><br>\n                        <input type=\"checkbox\" id=\"check3\" name=\"checkboxes\">\n                        <label for=\"check3\">Checkbox 3</label>\n                      </fieldset>\n                </div>\n                <div class=\"rightBox\">\n            \n                    <fieldset>\n                        <legend>Assigned Areas</legend>\n                        <input type=\"checkbox\" id=\"check1\" name=\"checkboxes\" checked>\n                        <label for=\"check1\">Checkbox 1</label><br>\n                        <input type=\"checkbox\" id=\"check2\" name=\"checkboxes\">\n                        <label for=\"check2\">Checkbox 2</label><br>\n                        <input type=\"checkbox\" id=\"check3\" name=\"checkboxes\">\n                        <label for=\"check3\">Checkbox 3</label>\n                      </fieldset>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-3\">\n                <button>Create user</button>\n            </div>\n\n            <div class=\"col-md-3\">\n                <button>Save</button>\n            </div>\n            <div class=\"col-md-3\">\n                <button>Modify</button>\n            </div>\n            <div class=\"col-md-3\">\n                <button>Delete</button>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <table>\n                    <thead>\n                      <tr>\n                        <th scope=\"col\">Id</th>\n                        <th scope=\"col\">Name</th>\n                        <th scope=\"col\">Status</th>\n                        <th scope=\"col\">Type</th>\n                        <th scope=\"col\">Password</th>\n                        <th scope=\"col\">Access</th>\n                        <th scope=\"col\">Assigned Areas</th>\n                      </tr>\n                    </thead>\n                    <tbody>\n                      <tr>\n                        <td data-label=\"Account\">1</td>\n                        <td data-label=\"Due Date\">KK</td>\n                        <td data-label=\"Amount\">Active</td>\n                        <td data-label=\"Period\">U1</td>\n                        <td data-label=\"Period\">******</td>\n                        <td data-label=\"Period\">All</td>\n                        <td data-label=\"Period\">All</td>\n                      </tr>\n                    \n                    </tbody>\n                  </table>\n            </div>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "vtpD":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoginComponent = void 0;
var http_1 = __webpack_require__(/*! @angular/common/http */ "tk/3");
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var forms_1 = __webpack_require__(/*! @angular/forms */ "3Pt+");
var router_1 = __webpack_require__(/*! @angular/router */ "tyNb");
var environment_1 = __webpack_require__(/*! environments/environment */ "AytR");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(http, router) {
        this.http = http;
        this.router = router;
        this.userData = new forms_1.FormGroup({
            email: new forms_1.FormControl(''),
            password: new forms_1.FormControl('')
        });
        this.forgotPassword = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.loginUser = function () {
        var _this = this;
        console.log(this.userData.value);
        this.http.post(environment_1.apiEndpoint + 'user/token/', this.userData.value).subscribe(function (res) {
            localStorage.setItem('user', JSON.stringify(res));
            localStorage.setItem('token', res.token.toString());
            _this.router.navigate(['/my-profile']);
        });
    };
    LoginComponent.ctorParameters = function () { return [
        { type: http_1.HttpClient },
        { type: router_1.Router }
    ]; };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'app-login',
            template: __webpack_require__(/*! raw-loader!./login.component.html */ "in5m").default,
            styles: [__webpack_require__(/*! ./login.component.css */ "n7sk").default]
        }),
        __metadata("design:paramtypes", [http_1.HttpClient, router_1.Router])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;


/***/ }),

/***/ "vtrx":
/*!******************************************************************!*\
  !*** ./src/app/layouts/admin-layout/admin-layout.component.scss ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhZG1pbi1sYXlvdXQuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "wCP4":
/*!*******************************************!*\
  !*** ./src/app/sidebar/sidebar.module.ts ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SidebarModule = void 0;
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var common_1 = __webpack_require__(/*! @angular/common */ "ofXK");
var router_1 = __webpack_require__(/*! @angular/router */ "tyNb");
var sidebar_component_1 = __webpack_require__(/*! ./sidebar.component */ "47Jg");
var SidebarModule = /** @class */ (function () {
    function SidebarModule() {
    }
    SidebarModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule, common_1.CommonModule],
            declarations: [sidebar_component_1.SidebarComponent],
            exports: [sidebar_component_1.SidebarComponent]
        })
    ], SidebarModule);
    return SidebarModule;
}());
exports.SidebarModule = SidebarModule;


/***/ }),

/***/ "wYun":
/*!*****************************************************!*\
  !*** ./src/app/complaints/complaints.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@import url(https://fonts.googleapis.com/css?family=Roboto:400,300);\r\n* {\r\n  box-sizing: border-box;\r\n}\r\nhtml {\r\n  height: 100%;\r\n}\r\nbody {\r\n  height: 100%;\r\n  font-family: Roboto, san-serif;\r\n  font-weight: 300;\r\n  background-color: #f5f6f7;\r\n}\r\n/* Form element setup */\r\nform {\r\n    \r\n  /* position: absolute; */\r\n  top: 0;\r\n  /* left: 50%; */\r\n  width: 300px;\r\n  /* transform: translateX(-50%); */\r\n  margin: 2rem 0;\r\n  z-index: 1;\r\n}\r\nfieldset {\r\n  margin: 0 0 1rem 0;\r\n  padding: 0;\r\n  border: none;\r\n}\r\nlegend {\r\n  font-weight: 400;\r\n}\r\nlegend,\r\nlabel {\r\n  display: inline-block;\r\n  margin-bottom: 0.5rem;\r\n}\r\ninput[type=text],\r\ntextarea,\r\nselect {\r\n  display: block;\r\n  padding: 0.5rem;\r\n  width: 100%;\r\n  background-color: white;\r\n  border-radius: 0.25rem;\r\n  border: 1px solid #e5e5e5;\r\n  outline: none;\r\n  /* List some properties that might change */\r\n  transition-property: none;\r\n  transition-duration: none;\r\n}\r\ninput[type=text]:focus,\r\ntextarea:focus,\r\nselect:focus {\r\n  border-color: #ef7ead;\r\n}\r\ntextarea {\r\n  max-width: 300px;\r\n  height: 100px;\r\n}\r\ninput[type=text],\r\nselect {\r\n  height: 34px;\r\n}\r\nselect {\r\n  font-size: 0.875rem;\r\n}\r\ninput[type=checkbox],\r\ninput[type=radio] {\r\n  position: relative;\r\n  top: 5px;\r\n  width: 22px;\r\n  height: 22px;\r\n  margin: 0 0.5rem;\r\n  background-color: white;\r\n  border: 1px solid #e5e5e5;\r\n  outline: none;\r\n  -moz-appearance: none;\r\n  -webkit-appearance: none;\r\n  /* List some properties that might change */\r\n  transition-property: none;\r\n  transition-duration: none;\r\n}\r\ninput[type=checkbox] {\r\n  border-radius: 5px;\r\n}\r\ninput[type=checkbox]:checked {\r\n  background-color: #ef7ead;\r\n  border: none;\r\n}\r\ninput[type=checkbox]:checked:after {\r\n  display: block;\r\n  content: \"\";\r\n  height: 4px;\r\n  width: 10px;\r\n  border-bottom: 3px solid #fff;\r\n  border-left: 3px solid #fff;\r\n  transform: translate(5px, 6px) rotate(-45deg) scale(1);\r\n}\r\ninput[type=radio] {\r\n  border-radius: 50%;\r\n}\r\ninput[type=radio]:checked {\r\n  border-width: 5px;\r\n  border-color: white;\r\n  background-color: #ef7ead;\r\n}\r\nbutton {\r\n  display: block;\r\n  margin: 3em auto;\r\n  padding: 0.5rem 2rem;\r\n  font-size: 125%;\r\n  color: white;\r\n  border: none;\r\n  border-radius: 0.25rem;\r\n  background-color: #ef7ead;\r\n  outline: none;\r\n  box-shadow: 0 0.4rem 0.1rem -0.3rem rgba(0, 0, 0, 0.1);\r\n  /* We'll talk about this next */\r\n  transform: perspective(300px) scale(0.95) translateZ(0);\r\n  transform-style: preserve-3d;\r\n  /* List the properties that you're looking to transition.\r\n     Try not to use 'all' */\r\n  transition-property: none;\r\n  /* This applies to all of the above properties */\r\n  transition-duration: none;\r\n}\r\nbutton:hover {\r\n  cursor: pointer;\r\n  background-color: #ff96c8;\r\n  box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);\r\n  transform: scale(1.1) rotateX(0);\r\n}\r\nbutton:active {\r\n  background-color: #ef7ead;\r\n  transform: scale(1.05) rotateX(-10deg);\r\n}\r\n.rightBox {\r\n    border: 1px solid;\r\n    width: 100%;\r\n    height: 30vh;\r\n}\r\ntable { border: 1px solid #ccc; border-collapse: collapse; margin: 0; padding: 0; width: 100%; table-layout: fixed;}\r\ntable caption { font-size: 1.5em; margin: .5em 0 .75em;}\r\ntable tr { border: 1px solid #ddd; padding: .35em;}\r\ntable th,\r\n      table td { padding: .625em; text-align: center;}\r\ntable th { font-size: .85em; letter-spacing: .1em; text-transform: uppercase;}\r\n@media screen and (max-width: 600px) {\r\n        table { border: 0; }\r\n        table caption { font-size: 1.3em; }\r\n        table thead { border: none; clip: rect(0 0 0 0); height: 1px; margin: -1px; overflow: hidden; padding: 0; position: absolute; width: 1px;}\r\n        table tr { border-bottom: 3px solid #ddd; display: block; margin-bottom: .625em; }\r\n        table td { border-bottom: 1px solid #ddd; display: block; font-size: .8em; text-align: right;}\r\n        table td::before { content: attr(data-label); float: left; font-weight: bold; text-transform: uppercase; }\r\n        table td:last-child { border-bottom: 0; }\r\n      }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBsYWludHMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxtRUFBbUU7QUFDbkU7RUFDRSxzQkFBc0I7QUFDeEI7QUFFQTtFQUNFLFlBQVk7QUFDZDtBQUVBO0VBQ0UsWUFBWTtFQUNaLDhCQUE4QjtFQUM5QixnQkFBZ0I7RUFDaEIseUJBQXlCO0FBQzNCO0FBRUEsdUJBQXVCO0FBQ3ZCOztFQUVFLHdCQUF3QjtFQUN4QixNQUFNO0VBQ04sZUFBZTtFQUNmLFlBQVk7RUFDWixpQ0FBaUM7RUFDakMsY0FBYztFQUNkLFVBQVU7QUFDWjtBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixZQUFZO0FBQ2Q7QUFFQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUVBOztFQUVFLHFCQUFxQjtFQUNyQixxQkFBcUI7QUFDdkI7QUFFQTs7O0VBR0UsY0FBYztFQUNkLGVBQWU7RUFDZixXQUFXO0VBQ1gsdUJBQXVCO0VBQ3ZCLHNCQUFzQjtFQUN0Qix5QkFBeUI7RUFDekIsYUFBYTtFQUNiLDJDQUEyQztFQUMzQyx5QkFBeUI7RUFDekIseUJBQXlCO0FBQzNCO0FBQ0E7OztFQUdFLHFCQUFxQjtBQUN2QjtBQUVBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGFBQWE7QUFDZjtBQUVBOztFQUVFLFlBQVk7QUFDZDtBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCO0FBRUE7O0VBRUUsa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixXQUFXO0VBQ1gsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQix1QkFBdUI7RUFDdkIseUJBQXlCO0VBQ3pCLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsd0JBQXdCO0VBQ3hCLDJDQUEyQztFQUMzQyx5QkFBeUI7RUFDekIseUJBQXlCO0FBQzNCO0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLHlCQUF5QjtFQUN6QixZQUFZO0FBQ2Q7QUFDQTtFQUNFLGNBQWM7RUFDZCxXQUFXO0VBQ1gsV0FBVztFQUNYLFdBQVc7RUFDWCw2QkFBNkI7RUFDN0IsMkJBQTJCO0VBQzNCLHNEQUFzRDtBQUN4RDtBQUVBO0VBQ0Usa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLHlCQUF5QjtBQUMzQjtBQUVBO0VBQ0UsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUNmLFlBQVk7RUFDWixZQUFZO0VBQ1osc0JBQXNCO0VBQ3RCLHlCQUF5QjtFQUN6QixhQUFhO0VBQ2Isc0RBQXNEO0VBQ3RELCtCQUErQjtFQUMvQix1REFBdUQ7RUFDdkQsNEJBQTRCO0VBQzVCOzJCQUN5QjtFQUN6Qix5QkFBeUI7RUFDekIsZ0RBQWdEO0VBQ2hELHlCQUF5QjtBQUMzQjtBQUNBO0VBQ0UsZUFBZTtFQUNmLHlCQUF5QjtFQUN6QixvQ0FBb0M7RUFDcEMsZ0NBQWdDO0FBQ2xDO0FBQ0E7RUFDRSx5QkFBeUI7RUFDekIsc0NBQXNDO0FBQ3hDO0FBR0E7SUFDSSxpQkFBaUI7SUFDakIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFHQSxRQUFRLHNCQUFzQixFQUFFLHlCQUF5QixFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixDQUFDO0FBQzdHLGdCQUFnQixnQkFBZ0IsRUFBRSxvQkFBb0IsQ0FBQztBQUN2RCxXQUFXLHNCQUFzQixFQUFFLGNBQWMsQ0FBQztBQUNsRDtpQkFDVyxlQUFlLEVBQUUsa0JBQWtCLENBQUM7QUFDL0MsV0FBVyxnQkFBZ0IsRUFBRSxvQkFBb0IsRUFBRSx5QkFBeUIsQ0FBQztBQUM3RTtRQUNFLFFBQVEsU0FBUyxFQUFFO1FBQ25CLGdCQUFnQixnQkFBZ0IsRUFBRTtRQUNsQyxjQUFjLFlBQVksRUFBRSxtQkFBbUIsRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUFFLGdCQUFnQixFQUFFLFVBQVUsRUFBRSxrQkFBa0IsRUFBRSxVQUFVLENBQUM7UUFDekksV0FBVyw2QkFBNkIsRUFBRSxjQUFjLEVBQUUscUJBQXFCLEVBQUU7UUFDakYsV0FBVyw2QkFBNkIsRUFBRSxjQUFjLEVBQUUsZUFBZSxFQUFFLGlCQUFpQixDQUFDO1FBQzdGLG1CQUFtQix5QkFBeUIsRUFBRSxXQUFXLEVBQUUsaUJBQWlCLEVBQUUseUJBQXlCLEVBQUU7UUFDekcsc0JBQXNCLGdCQUFnQixFQUFFO01BQzFDIiwiZmlsZSI6ImNvbXBsYWludHMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgdXJsKGh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1Sb2JvdG86NDAwLDMwMCk7XHJcbioge1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbn1cclxuXHJcbmh0bWwge1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG5cclxuYm9keSB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIGZvbnQtZmFtaWx5OiBSb2JvdG8sIHNhbi1zZXJpZjtcclxuICBmb250LXdlaWdodDogMzAwO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmNWY2Zjc7XHJcbn1cclxuXHJcbi8qIEZvcm0gZWxlbWVudCBzZXR1cCAqL1xyXG5mb3JtIHtcclxuICAgIFxyXG4gIC8qIHBvc2l0aW9uOiBhYnNvbHV0ZTsgKi9cclxuICB0b3A6IDA7XHJcbiAgLyogbGVmdDogNTAlOyAqL1xyXG4gIHdpZHRoOiAzMDBweDtcclxuICAvKiB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSk7ICovXHJcbiAgbWFyZ2luOiAycmVtIDA7XHJcbiAgei1pbmRleDogMTtcclxufVxyXG5cclxuZmllbGRzZXQge1xyXG4gIG1hcmdpbjogMCAwIDFyZW0gMDtcclxuICBwYWRkaW5nOiAwO1xyXG4gIGJvcmRlcjogbm9uZTtcclxufVxyXG5cclxubGVnZW5kIHtcclxuICBmb250LXdlaWdodDogNDAwO1xyXG59XHJcblxyXG5sZWdlbmQsXHJcbmxhYmVsIHtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPXRleHRdLFxyXG50ZXh0YXJlYSxcclxuc2VsZWN0IHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwYWRkaW5nOiAwLjVyZW07XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgYm9yZGVyLXJhZGl1czogMC4yNXJlbTtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZTVlNWU1O1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbiAgLyogTGlzdCBzb21lIHByb3BlcnRpZXMgdGhhdCBtaWdodCBjaGFuZ2UgKi9cclxuICB0cmFuc2l0aW9uLXByb3BlcnR5OiBub25lO1xyXG4gIHRyYW5zaXRpb24tZHVyYXRpb246IG5vbmU7XHJcbn1cclxuaW5wdXRbdHlwZT10ZXh0XTpmb2N1cyxcclxudGV4dGFyZWE6Zm9jdXMsXHJcbnNlbGVjdDpmb2N1cyB7XHJcbiAgYm9yZGVyLWNvbG9yOiAjZWY3ZWFkO1xyXG59XHJcblxyXG50ZXh0YXJlYSB7XHJcbiAgbWF4LXdpZHRoOiAzMDBweDtcclxuICBoZWlnaHQ6IDEwMHB4O1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPXRleHRdLFxyXG5zZWxlY3Qge1xyXG4gIGhlaWdodDogMzRweDtcclxufVxyXG5cclxuc2VsZWN0IHtcclxuICBmb250LXNpemU6IDAuODc1cmVtO1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPWNoZWNrYm94XSxcclxuaW5wdXRbdHlwZT1yYWRpb10ge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB0b3A6IDVweDtcclxuICB3aWR0aDogMjJweDtcclxuICBoZWlnaHQ6IDIycHg7XHJcbiAgbWFyZ2luOiAwIDAuNXJlbTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZTVlNWU1O1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbiAgLW1vei1hcHBlYXJhbmNlOiBub25lO1xyXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcclxuICAvKiBMaXN0IHNvbWUgcHJvcGVydGllcyB0aGF0IG1pZ2h0IGNoYW5nZSAqL1xyXG4gIHRyYW5zaXRpb24tcHJvcGVydHk6IG5vbmU7XHJcbiAgdHJhbnNpdGlvbi1kdXJhdGlvbjogbm9uZTtcclxufVxyXG5cclxuaW5wdXRbdHlwZT1jaGVja2JveF0ge1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxufVxyXG5pbnB1dFt0eXBlPWNoZWNrYm94XTpjaGVja2VkIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWY3ZWFkO1xyXG4gIGJvcmRlcjogbm9uZTtcclxufVxyXG5pbnB1dFt0eXBlPWNoZWNrYm94XTpjaGVja2VkOmFmdGVyIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBjb250ZW50OiBcIlwiO1xyXG4gIGhlaWdodDogNHB4O1xyXG4gIHdpZHRoOiAxMHB4O1xyXG4gIGJvcmRlci1ib3R0b206IDNweCBzb2xpZCAjZmZmO1xyXG4gIGJvcmRlci1sZWZ0OiAzcHggc29saWQgI2ZmZjtcclxuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSg1cHgsIDZweCkgcm90YXRlKC00NWRlZykgc2NhbGUoMSk7XHJcbn1cclxuXHJcbmlucHV0W3R5cGU9cmFkaW9dIHtcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbn1cclxuaW5wdXRbdHlwZT1yYWRpb106Y2hlY2tlZCB7XHJcbiAgYm9yZGVyLXdpZHRoOiA1cHg7XHJcbiAgYm9yZGVyLWNvbG9yOiB3aGl0ZTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWY3ZWFkO1xyXG59XHJcblxyXG5idXR0b24ge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbjogM2VtIGF1dG87XHJcbiAgcGFkZGluZzogMC41cmVtIDJyZW07XHJcbiAgZm9udC1zaXplOiAxMjUlO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgYm9yZGVyLXJhZGl1czogMC4yNXJlbTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWY3ZWFkO1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbiAgYm94LXNoYWRvdzogMCAwLjRyZW0gMC4xcmVtIC0wLjNyZW0gcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG4gIC8qIFdlJ2xsIHRhbGsgYWJvdXQgdGhpcyBuZXh0ICovXHJcbiAgdHJhbnNmb3JtOiBwZXJzcGVjdGl2ZSgzMDBweCkgc2NhbGUoMC45NSkgdHJhbnNsYXRlWigwKTtcclxuICB0cmFuc2Zvcm0tc3R5bGU6IHByZXNlcnZlLTNkO1xyXG4gIC8qIExpc3QgdGhlIHByb3BlcnRpZXMgdGhhdCB5b3UncmUgbG9va2luZyB0byB0cmFuc2l0aW9uLlxyXG4gICAgIFRyeSBub3QgdG8gdXNlICdhbGwnICovXHJcbiAgdHJhbnNpdGlvbi1wcm9wZXJ0eTogbm9uZTtcclxuICAvKiBUaGlzIGFwcGxpZXMgdG8gYWxsIG9mIHRoZSBhYm92ZSBwcm9wZXJ0aWVzICovXHJcbiAgdHJhbnNpdGlvbi1kdXJhdGlvbjogbm9uZTtcclxufVxyXG5idXR0b246aG92ZXIge1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmY5NmM4O1xyXG4gIGJveC1zaGFkb3c6IDAgMCAwIDAgcmdiYSgwLCAwLCAwLCAwKTtcclxuICB0cmFuc2Zvcm06IHNjYWxlKDEuMSkgcm90YXRlWCgwKTtcclxufVxyXG5idXR0b246YWN0aXZlIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWY3ZWFkO1xyXG4gIHRyYW5zZm9ybTogc2NhbGUoMS4wNSkgcm90YXRlWCgtMTBkZWcpO1xyXG59XHJcblxyXG5cclxuLnJpZ2h0Qm94IHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDMwdmg7XHJcbn1cclxuXHJcblxyXG50YWJsZSB7IGJvcmRlcjogMXB4IHNvbGlkICNjY2M7IGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7IG1hcmdpbjogMDsgcGFkZGluZzogMDsgd2lkdGg6IDEwMCU7IHRhYmxlLWxheW91dDogZml4ZWQ7fVxyXG4gICAgICB0YWJsZSBjYXB0aW9uIHsgZm9udC1zaXplOiAxLjVlbTsgbWFyZ2luOiAuNWVtIDAgLjc1ZW07fVxyXG4gICAgICB0YWJsZSB0ciB7IGJvcmRlcjogMXB4IHNvbGlkICNkZGQ7IHBhZGRpbmc6IC4zNWVtO31cclxuICAgICAgdGFibGUgdGgsXHJcbiAgICAgIHRhYmxlIHRkIHsgcGFkZGluZzogLjYyNWVtOyB0ZXh0LWFsaWduOiBjZW50ZXI7fVxyXG4gICAgICB0YWJsZSB0aCB7IGZvbnQtc2l6ZTogLjg1ZW07IGxldHRlci1zcGFjaW5nOiAuMWVtOyB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO31cclxuICAgICAgQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNjAwcHgpIHtcclxuICAgICAgICB0YWJsZSB7IGJvcmRlcjogMDsgfVxyXG4gICAgICAgIHRhYmxlIGNhcHRpb24geyBmb250LXNpemU6IDEuM2VtOyB9XHJcbiAgICAgICAgdGFibGUgdGhlYWQgeyBib3JkZXI6IG5vbmU7IGNsaXA6IHJlY3QoMCAwIDAgMCk7IGhlaWdodDogMXB4OyBtYXJnaW46IC0xcHg7IG92ZXJmbG93OiBoaWRkZW47IHBhZGRpbmc6IDA7IHBvc2l0aW9uOiBhYnNvbHV0ZTsgd2lkdGg6IDFweDt9XHJcbiAgICAgICAgdGFibGUgdHIgeyBib3JkZXItYm90dG9tOiAzcHggc29saWQgI2RkZDsgZGlzcGxheTogYmxvY2s7IG1hcmdpbi1ib3R0b206IC42MjVlbTsgfVxyXG4gICAgICAgIHRhYmxlIHRkIHsgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkZGQ7IGRpc3BsYXk6IGJsb2NrOyBmb250LXNpemU6IC44ZW07IHRleHQtYWxpZ246IHJpZ2h0O31cclxuICAgICAgICB0YWJsZSB0ZDo6YmVmb3JlIHsgY29udGVudDogYXR0cihkYXRhLWxhYmVsKTsgZmxvYXQ6IGxlZnQ7IGZvbnQtd2VpZ2h0OiBib2xkOyB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlOyB9XHJcbiAgICAgICAgdGFibGUgdGQ6bGFzdC1jaGlsZCB7IGJvcmRlci1ib3R0b206IDA7IH1cclxuICAgICAgfSJdfQ== */");

/***/ }),

/***/ "xwfu":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>profile works!</p>\n");

/***/ }),

/***/ "zRkE":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/navbar/navbar.component.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<nav class=\"navbar navbar-default\">\n    <div class=\"container-fluid\">\n        <div class=\"navbar-header\">\n            <!-- <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" (click)=\"sidebarToggle()\">\n                <span class=\"sr-only\">Toggle navigation</span>\n                <span class=\"icon-bar\"></span>\n                <span class=\"icon-bar\"></span>\n                <span class=\"icon-bar\"></span>\n            </button> -->\n            <!-- <a class=\"navbar-brand\" href=\"#\" >{{getTitle()}}</a> -->\n        </div>\n        <div class=\"collapse navbar-collapse\">\n           \n\n            <ul class=\"nav navbar-nav navbar-right\">\n                <!-- <li>\n                    <a href=\"#\">\n                        Account\n                    </a>\n                </li>\n                <li class=\"dropdown\">\n                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n\n                            Dropdown\n                            <b class=\"caret\"></b>\n                        \n                    </a>\n                    <ul class=\"dropdown-menu\">\n                        <li><a href=\"#\">Action</a></li>\n                        <li><a href=\"#\">Another action</a></li>\n                        <li><a href=\"#\">Something</a></li>\n                        <li><a href=\"#\">Another action</a></li>\n                        <li><a href=\"#\">Something</a></li>\n                        <li class=\"divider\"></li>\n                        <li><a href=\"#\">Separated link</a></li>\n                    </ul>\n                </li> -->\n                <li>\n                    <a routerLink=\"/login\">\n                        Log out\n                    </a>\n                </li>\n                <li class=\"separator hidden-lg hidden-md\"></li>\n            </ul>\n        </div>\n    </div>\n</nav>\n");

/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
/*!

 =========================================================
 * Light Bootstrap Dashboard Angular - v1.7.0
 =========================================================

 * Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-angular2
 * Copyright 2020 Creative Tim (http://www.creative-tim.com)
 * Licensed under MIT

 =========================================================

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 */
var core_1 = __webpack_require__(/*! @angular/core */ "fXoL");
var platform_browser_dynamic_1 = __webpack_require__(/*! @angular/platform-browser-dynamic */ "a3Wg");
var app_module_1 = __webpack_require__(/*! ./app/app.module */ "ZAI4");
var environment_1 = __webpack_require__(/*! ./environments/environment */ "AytR");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule);


/***/ }),

/***/ "zvoc":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/sidebar/sidebar.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<div class=\"sidebar-wrapper\">\n    <div class=\"logo\">\n        <a href=\"http://www.creative-tim.com\" class=\"simple-text\">\n            <div class=\"logo-img\">\n                <img src=\"/assets/img/angular2-logo-white.png\"/>\n            </div>\nCar parking\n        </a>\n    </div>\n    <ul class=\"nav responsive-nav\" *ngIf=\"isAdmin\">\n         <li routerLinkActive=\"active\" *ngFor=\"let menuItem of menuItems\" class=\"{{menuItem.class}}\" >\n            <a  [routerLink]=\"[menuItem.path]\">\n                <i class=\"{{menuItem.icon}}\"></i>\n                <p>{{menuItem.title}}</p>\n            </a>\n        </li>\n        \n        <li routerLinkActive=\"active\" >\n            <a (click)=\"clearAndLogout()\">\n                <i class=\"pe-7s-note2\"></i>\n                <p>Clear all data</p>\n            </a>\n        </li>\n    </ul>\n    \n    <ul class=\"nav responsive-nav\" *ngIf=\"!isAdmin\">\n        <li routerLinkActive=\"active\" *ngFor=\"let menuItem of menuItemsUser\" class=\"{{menuItem.class}}\" >\n           <a  [routerLink]=\"[menuItem.path]\">\n               <i class=\"{{menuItem.icon}}\"></i>\n               <p>{{menuItem.title}}</p>\n           </a>\n       </li>\n   </ul>\n</div>\n\n\n\n\n\n\n\n\n\n<!-- <li *ngIf=\"isMobileMenu()\">\n    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n        <i class=\"fa fa-dashboard\"></i>\n        <p class=\"hidden-lg hidden-md\">Dashboard</p>\n    </a>\n</li>\n<li class=\"dropdown\" *ngIf=\"isMobileMenu()\">\n      <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n            <i class=\"fa fa-globe\"></i>\n            <b class=\"caret hidden-sm hidden-xs\"></b>\n            <span class=\"notification hidden-sm hidden-xs\">5</span>\n            <p class=\"hidden-lg hidden-md\">\n                5 Notifications\n                <b class=\"caret\"></b>\n            </p>\n      </a>\n      <ul class=\"dropdown-menu\">\n        <li><a href=\"#\">Notification 1</a></li>\n        <li><a href=\"#\">Notification 2</a></li>\n        <li><a href=\"#\">Notification 3</a></li>\n        <li><a href=\"#\">Notification 4</a></li>\n        <li><a href=\"#\">Another notification</a></li>\n      </ul>\n</li>\n<li *ngIf=\"isMobileMenu()\">\n   <a>\n        <i class=\"fa fa-search\"></i>\n        <p class=\"hidden-lg hidden-md\">Search</p>\n    </a>\n</li>\n<li *ngIf=\"isMobileMenu()\">\n   <a href=\"\">\n       <p>Account</p>\n    </a>\n</li>\n<li class=\"dropdown\" *ngIf=\"isMobileMenu()\">\n      <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n            <p>\n                Dropdown\n                <b class=\"caret\"></b>\n            </p>\n\n      </a>\n      <ul class=\"dropdown-menu\">\n        <li><a href=\"#\">Action</a></li>\n        <li><a href=\"#\">Another action</a></li>\n        <li><a href=\"#\">Something</a></li>\n        <li><a href=\"#\">Another action</a></li>\n        <li><a href=\"#\">Something</a></li>\n        <li class=\"divider\"></li>\n        <li><a href=\"#\">Separated link</a></li>\n      </ul>\n</li>\n<li *ngIf=\"isMobileMenu()\">\n    <a>\n        <p>Log out</p>\n    </a>\n</li> -->");

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map